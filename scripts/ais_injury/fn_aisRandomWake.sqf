//[p1] call pr_fnc_aisRandomWake

if ((isDedicated) || !(hasInterface)) exitWith {};

pr_fnc_aisRandomWake = {
    _unit = _this select 0;
    _unit spawn {
        _unit = _this;
        _sleep = 0;

        _damage  = damage _unit;
        _dFace   = _unit getHit "face_hub";
        _dNeck   = _unit getHit "neck";
        _dHead   = _unit getHit "head";
        _dPelvis = _unit getHit "pelvis";
        _dspine1 = _unit getHit "spine1";
        _dspine2 = _unit getHit "spine2";
        _dspine3 = _unit getHit "spine3";
        _dBody   = _unit getHit "body";
        _dArms   = _unit getHit "arms";
        _dHand   = _unit getHit "hands";
        _dLeg    = _unit getHit "legs";

        _sFace   = (_dFace * 50); // 45
        _sNeck   = (_dNeck * 50); // 45
        _sHead   = (_dHead * 200); // 180  .7
        _sPelvis = (_dPelvis * 50); // 45
        _sSpine1 = (_dSpine1 * 50); // 45
        _sSpine2 = (_dSpine2 * 50); // 45
        _sSpine3 = (_dSpine3 * 50); // 45
        _sBody   = (_dBody * 100); // 90  .5
        _sArms   = (_dArms * 100); // 90
        _sHand   = (_dHand * 50); // 45  .15
        _sLeg    = (_dLeg * 75); // 67.5 .3



        if (_dHand > 0) then { _sleep = _sleep + (_sHand + (random _sHand)); };
        if (_dLeg > 0) then { _sleep = _sleep + (_sLeg + (random _sLeg)); };
        if (_dBody > 0) then { _sleep = _sleep + (_sBody + (random _sBody)); };
        if (_dHead > 0) then { _sleep = _sleep + (_sHead + (random _sHead)); };
        PR_sleep = _sleep;

        //while { (_unit getVariable "tcb_ais_agony") } do {
        _i = 0; 
        while { _i = _i + 1; ((_i < _sleep) && (_unit getVariable "tcb_ais_agony")) } do {
            _damage = damage _unit;
            _dHand = _unit getHit "hands";
            _dLeg = _unit getHit "legs";
            _dBody = _unit getHit "body";
            _dHead = _unit getHit "head";

            if (_dHand < 0.26) then { _i = _i + 1; };
            if (_dLeg < 0.26) then { _i = _i + 1; };
            if (_dBody < 0.26) then { _i = _i + 1; };
            if (_dHead < 0.26) then { _i = _i + 1; };
            if (_dHead <= 0) then { _sleep = _sleep / (random 3); };
            zz_i = _i;
            sleep 1;
        };
        _unit setVariable ["tcb_ais_agony", false, true];
        _unit setdamage _damage;
        sleep 0.5;
        _unit sethit ["hands", _dHand];
        _unit sethit ["legs", _dLeg];
        _unit sethit ["body", _dBody];
        _unit sethit ["head", _dHead];
        sleep 0.5;
        _unit allowDamage true;
    };
};

_unit = player; 
pr_fnc_aisPlayerState = {
    if ((isDedicated) || !(hasInterface)) exitWith {};
    _unit = _this select 0;

    while { true } do {
        while { !(_unit getVariable "tcb_ais_agony") } do { sleep 1; };
        [_unit] call pr_fnc_aisRandomWake;
        while { (_unit getVariable "tcb_ais_agony") } do { sleep 1; };
    }; 
}; 
[_unit] spawn pr_fnc_aisPlayerState; 