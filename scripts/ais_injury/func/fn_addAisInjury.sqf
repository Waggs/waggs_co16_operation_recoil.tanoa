// [pilot1] call tcb_fnc_addAisInjury

_unit = _this select 0;
_unit setDamage 0.5 + (random 0.35);
_unit setVariable ["unit_is_unconscious", false];
_unit setVariable ["tcb_ais_agony", true, true];
_unit playActionNow "agonyStart";

if (isNil {_unit getVariable ["drag_action", Nil]}) then {
    _drag_action = _unit addAction [
        format ["<t color='#FC9512'>Drag %1</t>", name _unit],{ _this spawn tcb_fnc_drag }, _unit, 99, false, true, "",
        "{ not isNull (_target getVariable _x) } count ['healer','dragger','helper'] == 0 && { alive _target } && { vehicle _target == _target }"
    ];
    _unit setVariable ["drag_action", _drag_action];
};

if !(_unit getVariable "unit_is_unconscious") then {
    //if (tcb_ais_bloodParticle) then { [_unit] call tcb_fnc_setBleeding };
    tcb_ais_in_agony = [_unit,true];
    publicVariable "tcb_ais_in_agony";
    _unit setVariable ["unit_is_unconscious", true];

    _unit setHit ["hands",0.9];
    _unit setHit ["legs",0.9];
    _unit setCaptive true;

    /*if (!isPlayer _unit) then {
        _unit stop true;
        _unit disableAI "MOVE";
        _unit disableAI "TARGET";
        _unit disableAI "AUTOTARGET";
        _unit disableAI "ANIM";
    };*/

    //_unit allowDamage true;

    //_unit addEventHandler ["fired",{(_this select 0) setCaptive false}];
    //if (sunOrMoon == 0) then {
    //    _unit action ["nvGogglesOff",_unit];
    //};
};

//disableUserInput false;