1. Place the DebugMenu folder in your mission folder.
2. Add the following to your description.ext:

#include "scripts\SKULL\DebugMenu\SKL_DebugMenu.hpp"

3. Add the following to your init.sqf:
execVM "scripts\SKULL\DebugMenu\SKL_Zeus.sqf";

also:
to enable the system:
true execVM "scripts\SKULL\DebugMenu\SKL_DebugMenu.sqf";

or:
to allow only the author:
false execVM "scripts\SKULL\DebugMenu\SKL_DebugMenu.sqf";

4. If you enabled the system, then you can edit the SKL_DebugMenu.sqf and add admins to SKL_DEBUG_ADMINS, or set '_allowed = true' to allow all players to use debug.  

5. You can also set SKL_DEBUG_ALLOWED to false to disable the menu for everyone (by a mission parameter for instance).

6. If you place the script in a directory other than scripts\SKULL\DebugMenu, then set the PATH in ids.h
