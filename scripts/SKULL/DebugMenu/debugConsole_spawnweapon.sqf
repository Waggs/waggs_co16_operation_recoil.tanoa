#include "ids.h"

FHQ_DebugConsole_SelectedWeapon = -1;
FHQ_DebugConsole_WeaponFilterLast = 0;

FHQ_DebugConsole_WeaponCamPos = getPos player;
FHQ_DebugConsole_WeaponCamDir = direction player;

FHQ_DebugConsole_ClearInventory = {
    _unit = player;
	removeAllWeapons _unit;
	removeAllItems _unit;
	removeBackpack _unit;
	removeAllAssignedItems _unit;
	removeUniform _unit;
	removeVest _unit;
	removeHeadgear _unit;
	removeGoggles _unit;
};

FHQ_DebugConsole_WeaponFilterListBuildCfgWeapon = {
    
	FHQ_DebugConsole_WeaponList = [];

	_filterType = _this select 0;
    _filterSubType = _this select 1;
    _addAs = _this select 2;
    
    _miscCat = false;
    if (_filterSubType == -2) then {
        _miscCat = true;
        _filterSubType = -1;
    };
    
	_cnt = 0;
	_cfgWeapons = configFile >> "cfgWeapons";
	_display = findDisplay WEAPONSPAWN_DIALOG;
	lbClear WEAPONSPAWN_DIALOG_LIST;

	for "_i" from 0 to (count _cfgWeapons) - 1 do {
		_current = _cfgWeapons select _i;
		if (isClass _current) then {
            _discard = false;
			_configName = configName(_current);
			_scope = getNumber(configFile >> "cfgWeapons" >> _configName >> "scope");
			_displayName =  getText(configFile >> "cfgWeapons" >> _configName >> "displayName");
			_picture =  getText(configFile >> "cfgWeapons" >> _configName >> "picture");
    		_text = getText(configFile >> "cfgWeapons" >> _configName >> "Library" >> "libTextDesc");
            _type = getNumber(configFile >> "cfgWeapons" >> _configName >> "Type");
            
			_info = -1;
            if (isClass (configFile >> "cfgWeapons" >> _configName >> "ItemInfo")) then {
                _info = getNumber(configFile >> "cfgWeapons" >> _configName >> "ItemInfo" >> "Type");
            };
            
            if (_miscCat) then {
                _simulation = tolower (getText(configFile >> "cfgWeapons" >> _configName >> "simulation"));
                if (_info in [101, 201, 301, 801, 605, 701]) then {
                    _discard = true;
                };
                if (_displayName == "") then {
                    _discard = true;
                };
            };
            
            if (_filterSubType != -1) then {
                _type = 0;
                _filterType = 0;
            };
            
           	if (_filterType != 0) then {
                _filterSubType = -1;
               	_info = -1;
            };


			if (_scope == 2 && _filterType == _type && _filterSubType == _info && !_discard ) then {                
    			FHQ_DebugConsole_WeaponList = FHQ_DebugConsole_WeaponList + [[_configName, _displayName, _addAs, _picture, _text]];
			
	            lbAdd [WEAPONSPAWN_DIALOG_LIST, _displayName]; 
                lbSetPicture[WEAPONSPAWN_DIALOG_LIST, _cnt, _picture]; 
				lbSetValue [WEAPONSPAWN_DIALOG_LIST, _cnt, _cnt];
				if (FHQ_DebugConsole_IsArma3) then {
                	lbSetTooltip [WEAPONSPAWN_DIALOG_LIST, _cnt, _displayName];
				};
            	_cnt = _cnt + 1;
			};
		};
	};

            
	lbSort (_display displayCtrl WEAPONSPAWN_DIALOG_LIST);
};

FHQ_DebugConsole_WeaponFilterListBuildCfgVehicle = {
    _type = _this select 0;
    _addAs = _this select 1;
    
	_cnt = 0;
	_cfgVehicles = configFile >> "cfgVehicles";
	_display = findDisplay WEAPONSPAWN_DIALOG;
	lbClear WEAPONSPAWN_DIALOG_LIST;
	
    FHQ_DebugConsole_WeaponList = [];

	for "_i" from 0 to (count _cfgVehicles) - 1 do {
		_current = _cfgVehicles select _i;
		if (isClass _current) then {
			_configName = configName(_current);
			_scope = getNumber(configFile >> "cfgVehicles" >> _configName >> "scope");
			_class =  getText(configFile >> "cfgVehicles" >> _configName >> "vehicleclass");
			_displayName =  getText(configFile >> "cfgVehicles" >> _configName >> "displayName");
            _picture =  getText(configFile >> "cfgVehicles" >> _configName >> "picture");
            _text = "";
                        
			if (_scope == 2 && _class == _type) then {
    			FHQ_DebugConsole_WeaponList = FHQ_DebugConsole_WeaponList + [[_configName, _displayName, _addAs, _picture, _text]];
                			
	            lbAdd [WEAPONSPAWN_DIALOG_LIST, _displayName]; 
				lbSetValue [WEAPONSPAWN_DIALOG_LIST, _cnt, _cnt];
				lbSetPicture[WEAPONSPAWN_DIALOG_LIST, _cnt, _picture]; 
           		if (FHQ_DebugConsole_IsArma3) then {
                	lbSetTooltip [WEAPONSPAWN_DIALOG_LIST, _cnt, _displayName];
				};
            	_cnt = _cnt + 1;
			};
		};
	};

            
	lbSort (_display displayCtrl WEAPONSPAWN_DIALOG_LIST);
};


FHQ_DebugConsole_WeaponFilterListBuildCfgGlasses = {
    _addAs = _this select 0;
    
	_cnt = 0;
	_cfgGlasses = configFile >> "cfgGlasses";
	_display = findDisplay WEAPONSPAWN_DIALOG;
	lbClear WEAPONSPAWN_DIALOG_LIST;
	
    FHQ_DebugConsole_WeaponList = [];

	for "_i" from 0 to (count _cfgGlasses) - 1 do {
		_current = _cfgGlasses select _i;
		if (isClass _current) then {
			_configName = configName(_current);
			_scope = getNumber(configFile >> "cfgGlasses" >> _configName >> "scope");
			_displayName =  getText(configFile >> "cfgGlasses" >> _configName >> "displayName");
            _picture =  getText(configFile >> "cfgGlasses" >> _configName >> "picture");
            _text = "";
            
			if (_scope == 2) then {
    			FHQ_DebugConsole_WeaponList = FHQ_DebugConsole_WeaponList + [[_configName, _displayName, _addAs, _picture, _text]];
                			
	            lbAdd [WEAPONSPAWN_DIALOG_LIST, _displayName]; 
				lbSetValue [WEAPONSPAWN_DIALOG_LIST, _cnt, _cnt];
				lbSetPicture[WEAPONSPAWN_DIALOG_LIST, _cnt, _picture]; 
           		if (FHQ_DebugConsole_IsArma3) then {
                	lbSetTooltip [WEAPONSPAWN_DIALOG_LIST, _cnt, _displayName];
				};
            	_cnt = _cnt + 1;
			};
		};
	};

            
	lbSort (_display displayCtrl WEAPONSPAWN_DIALOG_LIST);
};

FHQ_DebugConsole_WeaponFilterListBuildCfgFaces = {
    _addAs = _this select 0;
    
	_cnt = 0;
	_cfgFaces = configFile >> "cfgFaces";
	_display = findDisplay WEAPONSPAWN_DIALOG;
	lbClear WEAPONSPAWN_DIALOG_LIST;
	
    FHQ_DebugConsole_WeaponList = [];

	for "_i" from 0 to (count _cfgFaces) - 1 do {
		_current = _cfgFaces select _i;
		if (isClass _current) then {
            
            _configName = configName(_current);
            
            for "_j" from 0 to (count _current) - 1 do {
                _entry = _current select _j;
            	_subCurrent = configName(_entry);
				
				_displayName =  getText(configFile >> "cfgFaces" >> _configName >> _subCurrent >> "displayName");
            	_picture =  "";
            	_text = "";
            
            	//diag_log str [_configName, _subCurrent, _displayName];
                
                if (!(tolower(_displayName) in ["", "default", "default face", "custom face", "animal"])) then {
                	FHQ_DebugConsole_WeaponList = FHQ_DebugConsole_WeaponList + [[_subCurrent, _displayName, _addAs, _picture, _text]];
                			
	        		lbAdd [WEAPONSPAWN_DIALOG_LIST, _displayName]; 
					lbSetValue [WEAPONSPAWN_DIALOG_LIST, _cnt, _cnt];
					lbSetPicture[WEAPONSPAWN_DIALOG_LIST, _cnt, _picture]; 
           			if (FHQ_DebugConsole_IsArma3) then {
		               	lbSetTooltip [WEAPONSPAWN_DIALOG_LIST, _cnt, _displayName];
					};
            		_cnt = _cnt + 1;
                };
			};
     	};
	};

            
	lbSort (_display displayCtrl WEAPONSPAWN_DIALOG_LIST);
};

FHQ_DebugConsole_SelectWeapon = {
    _index = _this select 0;
	_display = findDisplay WEAPONSPAWN_DIALOG;
           
    FHQ_DebugConsole_SelectedWeapon = lbValue [WEAPONSPAWN_DIALOG_LIST, _index];
    
    _weapon = FHQ_DebugConsole_WeaponList select FHQ_DebugConsole_SelectedWeapon;
    _configName = _weapon select 0;
    _configFile = _weapon select 2;
  	_picture =  _weapon select 3;
    _text = _weapon select 4;

	(_display displayCtrl 1100) ctrlSetStructuredText (parseText format["<t size='0.85'>%1</t>", _text]);
	(_display displayCtrl WEAPONSPAWN_DIALOG_PICTURE) ctrlSetText _picture;
};


// AmovPercMstpSlowWrflDnon Rifle
// AmovPercMstpSlowWpstDnon Pistol
// AmovPercMstpSnonWnonDnon None
FHQ_DebugConsole_WeaponCam_IdleWeaponAnim = "AmovPercMstpSlowWrflDnon";

FHQ_DebugConsole_WeaponCam_Update_Anim = {
    if (primaryWeapon player == "") then {
        player playMove "AmovPercMstpSnonWnonDnon";
    } else {
        player playMove FHQ_DebugConsole_WeaponCam_IdleWeaponAnim;
	};
};

FHQ_DebugConsole_SpawnWeaponNow = {
    if (FHQ_DebugConsole_SelectedWeapon == -1) exitWith {/* Nothing selected */};
    
    _weapon = FHQ_DebugConsole_WeaponList select FHQ_DebugConsole_SelectedWeapon;  
    
    _configName = _weapon select 0;
    _addAs = _weapon select 2;

    switch (_addAs) do
    {
        //case 1: { player addWeapon _configName; };
        case 1: { [player, _configName, 4] call BIS_fnc_addWeapon; player selectWeapon _configName; };
        case 2: { player addItem _configName; };
        case 3: { player addUniform _configName; };
        case 4: { player addHeadgear _configName; };
        case 5: { player addVest _configName; };
        case 6: { player addItem _configName; player assignItem _configName;};
        case 7: { player addBackpack _configName;};
        case 8: { if (_configName == "None") then {removeGoggles player;} else {player addGoggles _configName; }; };
        case 9: { [player, _configName, 2] call BIS_fnc_addWeapon; };
        case 10: { player setFace _configName; };
    };
    
    call FHQ_DebugConsole_WeaponCam_Update_Anim;
};

FHQ_DebugConsole_SpawnWeaponNowAdd = {
     _weapon = FHQ_DebugConsole_WeaponList select FHQ_DebugConsole_SelectedWeapon;  
    
    _configName = _weapon select 0;
    _addAs = _weapon select 2;
    
    if (_addAs == 2) then {
        if (FHQ_DebugConsole_WeaponCam_IdleWeaponAnim == "AmovPercMstpSlowWrflDnon") then {
            player addPrimaryWeaponItem _configName;
        } else {
            player addHandgunItem _configName;
        };
    };
        
};

FHQ_DebugConsole_WeaponSpawn_CreateCam = {   
    _res = [] spawn {
		_camPos = FHQ_DebugConsole_WeaponCamPos;
    
    	_camera = "camera" camCreate _camPos;
        _camera camPreparePos (player modelToWorld [0, 1, 1]);
        _camera camCommand "inertia off";
    	_camera camPrepareTarget (player modelToWorld [0, 0, 1]);
    	_camera camCommitPrepared 0;
    	_camera cameraEffect ["EXTERNAL", "BACK"];
        showCinemaBorder false;
       
		FHQ_DebugConsole_WeaponCamera = _camera;
        
        waitUntil {
            
            if (FHQ_DebugConsole_WeaponCamNewPos) then {
                _camera camSetPos (player modelToWorld FHQ_DebugConsole_WeaponCamPos);
                _camera camSetTarget (player modelToWorld FHQ_DebugConsole_WeaponCamTarget);
                _camera camSetFOV FHQ_DebugConsole_WeaponCamZoom;
                _camera camCommit 0.5;
                FHQ_DebugConsole_WeaponCamNewPos = false;
            };
            false
        };
	};
    
    _res;
};

FHQ_DebugConsole_WeaponSpawn_DeleteCam = { 
    FHQ_DebugConsole_WeaponCamera cameraEffect ["Terminate", "BACK"];
    FHQ_DebugConsole_WeaponCamera = objNull;
    terminate _this;
};
        

FHQ_DebugConsole_WeaponCam_FullFront = {
	FHQ_DebugConsole_WeaponCamPos = [1, 2, 1];
	FHQ_DebugConsole_WeaponCamTarget = [1, 0, 1];
	FHQ_DebugConsole_WeaponCamZoom = 0.7;
	FHQ_DebugConsole_WeaponCamNewPos = true;
};

FHQ_DebugConsole_WeaponCam_WeaponFront = {
	FHQ_DebugConsole_WeaponCamPos = [0.8, 2, 1.3];
	FHQ_DebugConsole_WeaponCamTarget = [0.8, 0, 1.3];
	FHQ_DebugConsole_WeaponCamZoom = 0.5;
	FHQ_DebugConsole_WeaponCamNewPos = true;
};

FHQ_DebugConsole_WeaponCam_FullBack = {
	FHQ_DebugConsole_WeaponCamPos = [-1, -2, 1];
	FHQ_DebugConsole_WeaponCamTarget = [-1, 0, 1];
	FHQ_DebugConsole_WeaponCamZoom = 0.7;
	FHQ_DebugConsole_WeaponCamNewPos = true;
};

FHQ_DebugConsole_WeaponCam_HeadFront = {
	FHQ_DebugConsole_WeaponCamPos = [0.3, 1.5, 1.6];
	FHQ_DebugConsole_WeaponCamTarget = [0.3, 0, 1.6];
	FHQ_DebugConsole_WeaponCamZoom = 0.4;
	FHQ_DebugConsole_WeaponCamNewPos = true;
};


FHQ_DebugConsole_WeaponFilterList = {
	_filter = lbData [WEAPONSPAWN_DIALOG_FILTER, FHQ_DebugConsole_WeaponFilterLast];

    switch (_filter) do {
	    case "Rifles": {
        	[1, -1, 1] call FHQ_DebugConsole_WeaponFilterListBuildCfgWeapon;
            FHQ_DebugConsole_SelectedWeapon = -1;
            call FHQ_DebugConsole_WeaponCam_WeaponFront;
            FHQ_DebugConsole_WeaponCam_IdleWeaponAnim = "AmovPercMstpSlowWrflDnon";
            call FHQ_DebugConsole_WeaponCam_Update_Anim;
    	};
	    case "Machineguns": {
        	[5, -1, 1] call FHQ_DebugConsole_WeaponFilterListBuildCfgWeapon;
            FHQ_DebugConsole_SelectedWeapon = -1;
            call FHQ_DebugConsole_WeaponCam_WeaponFront;
            FHQ_DebugConsole_WeaponCam_IdleWeaponAnim = "AmovPercMstpSlowWrflDnon";
            call FHQ_DebugConsole_WeaponCam_Update_Anim;
    	};
	    case "Launchers": {
        	[4, -1, 9] call FHQ_DebugConsole_WeaponFilterListBuildCfgWeapon;
            FHQ_DebugConsole_SelectedWeapon = -1;
            call FHQ_DebugConsole_WeaponCam_FullBack;
            call FHQ_DebugConsole_WeaponCam_Update_Anim;
    	};
	    case "Handguns": {
        	[2, -1, 1] call FHQ_DebugConsole_WeaponFilterListBuildCfgWeapon;
            FHQ_DebugConsole_SelectedWeapon = -1;
            call FHQ_DebugConsole_WeaponCam_WeaponFront;
            FHQ_DebugConsole_WeaponCam_IdleWeaponAnim = "AmovPercMstpSlowWpstDnon";
            call FHQ_DebugConsole_WeaponCam_Update_Anim;
    	};  
		case "Optics": {
        	[131027, 201, 2] call FHQ_DebugConsole_WeaponFilterListBuildCfgWeapon;
            FHQ_DebugConsole_SelectedWeapon = -1;
            call FHQ_DebugConsole_WeaponCam_WeaponFront;
            call FHQ_DebugConsole_WeaponCam_Update_Anim;
    	};  
		case "Weapon Muzzles": {
        	[131027, 101, 2] call FHQ_DebugConsole_WeaponFilterListBuildCfgWeapon;
            FHQ_DebugConsole_SelectedWeapon = -1;
            call FHQ_DebugConsole_WeaponCam_WeaponFront;
            call FHQ_DebugConsole_WeaponCam_Update_Anim;
    	};  
		case "Weapon Accessories": {
        	[131027, 301, 2] call FHQ_DebugConsole_WeaponFilterListBuildCfgWeapon;
            FHQ_DebugConsole_SelectedWeapon = -1;
            call FHQ_DebugConsole_WeaponCam_WeaponFront;
            call FHQ_DebugConsole_WeaponCam_Update_Anim;
    	}; 
		case "Uniforms": {
        	[131027, 801, 3] call FHQ_DebugConsole_WeaponFilterListBuildCfgWeapon;
            FHQ_DebugConsole_SelectedWeapon = -1;
            call FHQ_DebugConsole_WeaponCam_FullFront;
            call FHQ_DebugConsole_WeaponCam_Update_Anim;
    	}; 
		case "Helmets/Hats": {
        	[131027, 605, 4] call FHQ_DebugConsole_WeaponFilterListBuildCfgWeapon;
            FHQ_DebugConsole_SelectedWeapon = -1;
            call FHQ_DebugConsole_WeaponCam_HeadFront;
            call FHQ_DebugConsole_WeaponCam_Update_Anim;
    	}; 
		case "Vests": {
        	[131027, 701, 5] call FHQ_DebugConsole_WeaponFilterListBuildCfgWeapon;
            FHQ_DebugConsole_SelectedWeapon = -1;
            call FHQ_DebugConsole_WeaponCam_FullFront;
            call FHQ_DebugConsole_WeaponCam_Update_Anim;
    	}; 
        case "Other Items": {
        	[131072, -2, 6] call FHQ_DebugConsole_WeaponFilterListBuildCfgWeapon;
            FHQ_DebugConsole_SelectedWeapon = -1;
            call FHQ_DebugConsole_WeaponCam_FullFront;
            call FHQ_DebugConsole_WeaponCam_Update_Anim;
    	};
        case "Binoculars/NVG": {
        	[4096, -1, 2] call FHQ_DebugConsole_WeaponFilterListBuildCfgWeapon;
            FHQ_DebugConsole_SelectedWeapon = -1;
            call FHQ_DebugConsole_WeaponCam_HeadFront;
            call FHQ_DebugConsole_WeaponCam_Update_Anim;
    	};
        case "Backpacks": {
            ["Backpacks", 7] call FHQ_DebugConsole_WeaponFilterListBuildCfgVehicle;
            FHQ_DebugConsole_SelectedWeapon = -1;
            call FHQ_DebugConsole_WeaponCam_FullBack;
            call FHQ_DebugConsole_WeaponCam_Update_Anim;
    	};
        case "Glasses": {
            [8] call FHQ_DebugConsole_WeaponFilterListBuildCfgGlasses;
            FHQ_DebugConsole_SelectedWeapon = -1;
            call FHQ_DebugConsole_WeaponCam_HeadFront;
            call FHQ_DebugConsole_WeaponCam_Update_Anim;
    	};
        case "Faces": {
            [10] call FHQ_DebugConsole_WeaponFilterListBuildCfgFaces;
            FHQ_DebugConsole_SelectedWeapon = -1;
            call FHQ_DebugConsole_WeaponCam_HeadFront;
            call FHQ_DebugConsole_WeaponCam_Update_Anim;
        };
    };
    
    _display = findDisplay WEAPONSPAWN_DIALOG;
    (_display displayCtrl 1100) ctrlSetStructuredText parseText "<t size='0.85'></t>";
	(_display displayCtrl WEAPONSPAWN_DIALOG_PICTURE) ctrlSetText "";
};



disableSerialization;

_ok = createDialog "FHQ_DebugDialog_WeaponSpawn";

FHQ_DebugConsole_WeaponCamPos = [1, 2, 1];
FHQ_DebugConsole_WeaponCamTarget = [1, 0, 1];
FHQ_DebugConsole_WeaponCamZoom = 0.7;
FHQ_DebugConsole_WeaponCamNewPos = true;

call FHQ_DebugConsole_WeaponCam_FullFront;


_script = call FHQ_DebugConsole_WeaponSpawn_CreateCam;

{
	private "_index";
        
	_index = lbAdd [WEAPONSPAWN_DIALOG_FILTER, _x];
	lbSetData [WEAPONSPAWN_DIALOG_FILTER, _index, _x];
} forEach ["Rifles", "Machineguns", "Handguns", "Launchers", "Uniforms", "Vests", "Backpacks",  
			"Helmets/Hats", "Optics", "Weapon Muzzles", "Weapon Accessories",
            "Binoculars/NVG", "Glasses", "Other Items", "Faces"];

lbSetCurSel [WEAPONSPAWN_DIALOG_FILTER, 0];

[1, -1, 1] call FHQ_DebugConsole_WeaponFilterListBuildCfgWeapon;


_script spawn {   
    //waitUntil {dialog};
    waitUntil {!dialog};
    
    _this call FHQ_DebugConsole_WeaponSpawn_DeleteCam;
};