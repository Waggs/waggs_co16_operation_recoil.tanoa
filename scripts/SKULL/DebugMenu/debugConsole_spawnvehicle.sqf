#include "ids.h"


if (isNil "z_AddObjects") then {
    0 execVM "scripts\SKULL\DebugMenu\SKL_ZeusObjects.sqf";
};

FHQ_DebugConsole_SelectedVehicle = -1;
FHQ_DebugConsole_VehicleFilterLast = 0;

FHQ_DebugConsole_VehicleFilterListBuild = {
	FHQ_DebugConsole_VehicleList = [];

	_type = _this select 0;
	_cnt = 0;
	_cfgVehicles = configFile >> "cfgVehicles";
	_display = findDisplay VEHICLESPAWN_DIALOG;
	lbClear VEHICLESPAWN_DIALOG_LIST;

	for "_i" from 0 to (count _cfgVehicles) - 1 do {
		_current = _cfgVehicles select _i;
		if (isClass _current) then {
			_configName = configName(_current);
			_scope = getNumber(configFile >> "cfgVehicles" >> _configName >> "scope");
			_class =  getText(configFile >> "cfgVehicles" >> _configName >> "vehicleclass");
			_displayName =  getText(configFile >> "cfgVehicles" >> _configName >> "displayName");
            _faction = getText(configFile >> "cfgVehicles" >> _configName >> "faction");
			_picture =  getText(configFile >> "cfgVehicles" >> _configName >> "picture");
            
			_class2 = toArray _class;
            if (count _class2 >= 3) then {
                if ((_class2 select 0) == 77 && (_class2 select 1) == 101 && (_class2 select 2) == 110) then {
                	_class = "Men";
                };
            }; 

			if (_class == "Men") then {
                _picture = "";
            };

			if (_scope == 2 && _class == _type) then {
                _origDisplayName = _displayName;
                switch (_faction) do {
                    case "IND_F": 	{_displayName = _displayName + " [AAC]";};
                    case "BLU_F": 	{_displayName = _displayName + " [NATO]";};
        			case "BLU_G_F": {_displayName = _displayName + " [FIA]";};
        	        case "CIV_F": 	{_displayName = _displayName + " [Civilian]";};
        			case "OPF_F": 	{_displayName = _displayName + " [CSAT]";};
                };
				FHQ_DebugConsole_VehicleList = FHQ_DebugConsole_VehicleList + [[_configName, _displayName]];
			
	            lbAdd [VEHICLESPAWN_DIALOG_LIST, _displayName]; 
                lbSetPicture[VEHICLESPAWN_DIALOG_LIST, _cnt, _picture]; 
				lbSetValue [VEHICLESPAWN_DIALOG_LIST, _cnt, _cnt];
				if (FHQ_DebugConsole_IsArma3) then {
                	lbSetTooltip [VEHICLESPAWN_DIALOG_LIST, _cnt, _origDisplayName];
				};  
            	_cnt = _cnt + 1;
			};
		};
	};

            
	lbSort (_display displayCtrl VEHICLESPAWN_DIALOG_LIST);
};

FHQ_DebugConsole_VehicleFilterList = {
	_filter = lbData [VEHICLESPAWN_DIALOG_FILTER, FHQ_DebugConsole_VehicleFilterLast];

    switch (_filter) do {
	    case "Air": {
        	["Air"] call FHQ_DebugConsole_VehicleFilterListBuild;
            FHQ_DebugConsole_SelectedVehicle = -1;
    	};
    	case "Armored": {
			["Armored"] call FHQ_DebugConsole_VehicleFilterListBuild;
            FHQ_DebugConsole_SelectedVehicle = -1;
	    };
		case "Autonomous": {
			["Autonomous"] call FHQ_DebugConsole_VehicleFilterListBuild;
            FHQ_DebugConsole_SelectedVehicle = -1;
	    };
		case "Cars": {
			["Car"] call FHQ_DebugConsole_VehicleFilterListBuild;
            FHQ_DebugConsole_SelectedVehicle = -1;
	    };
		case "Men": {
			["Men"] call FHQ_DebugConsole_VehicleFilterListBuild;
            FHQ_DebugConsole_SelectedVehicle = -1;
	    };
		case "Ships": {
			["Ship"] call FHQ_DebugConsole_VehicleFilterListBuild;
            FHQ_DebugConsole_SelectedVehicle = -1;
	    };
		case "Static": {
			["Static"] call FHQ_DebugConsole_VehicleFilterListBuild;
            FHQ_DebugConsole_SelectedVehicle = -1;
	    };
		case "Submarines": {
			["Submarine"] call FHQ_DebugConsole_VehicleFilterListBuild;
            FHQ_DebugConsole_SelectedVehicle = -1;
	    };
		case "Supports": {
			["Support"] call FHQ_DebugConsole_VehicleFilterListBuild;
            FHQ_DebugConsole_SelectedVehicle = -1;
	    };
	};
    
    _display = findDisplay VEHICLESPAWN_DIALOG;
    (_display displayCtrl 1100) ctrlSetStructuredText parseText "<t size='0.85'></t>";
	(_display displayCtrl VEHICLESPAWN_DIALOG_PICTURE) ctrlSetText "";
};

FHQ_DebugConsole_SelectVehicle = {
    _index = _this select 0;
	_display = findDisplay VEHICLESPAWN_DIALOG;
           
    FHQ_DebugConsole_SelectedVehicle = lbValue [VEHICLESPAWN_DIALOG_LIST, _index];
    
    _vehicle = FHQ_DebugConsole_VehicleList select FHQ_DebugConsole_SelectedVehicle;
    _configName = _vehicle select 0;
  
	_picture =  getText(configFile >> "cfgVehicles" >> _configName >> "picture");
    _text = getText(configFile >> "cfgVehicles" >> _configName >> "Library" >> "libTextDesc");

	(_display displayCtrl 1100) ctrlSetStructuredText (parseText format["<t size='0.85'>%1</t>", _text]);
	(_display displayCtrl VEHICLESPAWN_DIALOG_PICTURE) ctrlSetText _picture;
 
    
};

FHQ_DebugConsole_SpawnVehicleNow = {
    if (FHQ_DebugConsole_SelectedVehicle == -1) exitWith {/* Nothing selected */};
    
    _vehicle = FHQ_DebugConsole_VehicleList select FHQ_DebugConsole_SelectedVehicle;  
      
    _dir = getDir player;
    _pos = [player, random(5)+1, (_dir - 15) + random(30)] call BIS_fnc_relPos;
   	//_newVehicle = createVehicle [_vehicle select 0, _pos, [], 0, "NONE"];
    if (isNil "SKLVehNum") then {SKLVehNum = 0;};
    call compile format ['SKLV%1 = createVehicle [%2 select 0,%3, [], 0, "NONE"]; [SKLV%1] call z_AddObjects; hint "SKLV%1 created";',SKLVehNum,_vehicle,_pos];
    SKLVehNum = SKLVehNum + 1;
};

disableSerialization;

_ok = createDialog "FHQ_DebugDialog_VehicleSpawn";

{
	private "_index";
        
	_index = lbAdd [VEHICLESPAWN_DIALOG_FILTER, _x];
	lbSetData [VEHICLESPAWN_DIALOG_FILTER, _index, _x];
} forEach ["Air", "Armored", "Autonomous", "Cars", "Men", "Ships", "Static", "Submarines", "Supports"];
 lbSetCurSel [VEHICLESPAWN_DIALOG_FILTER, 0];
 
["Air"] call FHQ_DebugConsole_VehicleFilterListBuild;