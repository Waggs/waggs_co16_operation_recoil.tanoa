
class FHQ_DebugDialog_VehicleSpawn
{
	idd = 140876;
	movingEnable = true;
	enableSimulation = true;
	class Objects
	{

	};

	class ControlsBackground
	{
		class FHQ_DebugMore_Backdrop: FHQRscText
		{
			idc = 1001;
			x = 0.29374 * safezoneW + safezoneX;
			y = 0.252426 * safezoneH + safezoneY;
			w = 0.41252 * safezoneW;
			h = 0.522655 * safezoneH;
			colorBackground[] = {0,0,0,0.4};
		};
	};

	class Controls
	{
		////////////////////////////////////////////////////////
		// GUI EDITOR OUTPUT START (by Varanon, v1.063, #Qabaho)
		////////////////////////////////////////////////////////

		class FHQ_DebugVehicle_Header: FHQRscText
		{
			idc = 1000;
			text = "Spawn Vehicle"; //--- ToDo: Localize;
			x = 0.29374 * safezoneW + safezoneX;
			y = 0.224918 * safezoneH + safezoneY;
			w = 0.41252 * safezoneW;
			h = 0.0275082 * safezoneH;
			colorBackground[] = {0,0,0,1};
		};
		class FHQ_DebugVehicle_Close: FHQRscButton
		{
			action = "closeDialog 0";

			idc = 1600;
			text = "Close"; //--- ToDo: Localize;
			x = 0.29374 * safezoneW + safezoneX;
			y = 0.747574 * safezoneH + safezoneY;
			w = 0.0902388 * safezoneW;
			h = 0.0275082 * safezoneH;
		};
		class FHQ_DebugVehicle_Back: FHQRscButton
		{
			action = "closeDialog 0; _handle = CreateDialog 'SKL_DEBUG_DIALOG';";

			idc = 1601;
			text = "Back"; //--- ToDo: Localize;
			x = 0.390424 * safezoneW + safezoneX;
			y = 0.747574 * safezoneH + safezoneY;
			w = 0.0902388 * safezoneW;
			h = 0.0275082 * safezoneH;
		};
		class FHQ_DebugVehicle_Selection: FHQRscListbox
		{
			onLBSelChanged = "[_this select 1] call FHQ_DebugConsole_SelectVehicle;";

			idc = 1500;
			x = 0.300186 * safezoneW + safezoneX;
			y = 0.307443 * safezoneH + safezoneY;
			w = 0.180469 * safezoneW;
			h = 0.42625 * safezoneH;
		};
		class FHQ_DebugVehicle_SpawnNow: FHQRscButton
		{
			action = "[] call FHQ_DebugConsole_SpawnVehicleNow;";

			idc = 1602;
			text = "Spawn"; //--- ToDo: Localize;
			x = 0.60313 * safezoneW + safezoneX;
			y = 0.747574 * safezoneH + safezoneY;
			w = 0.10313 * safezoneW;
			h = 0.0275082 * safezoneH;
		};
		class FHQ_DebugVehicle_Filter: FHQRscCombo
		{
			onLBSelChanged	= "FHQ_DebugConsole_VehicleFilterLast = _this select 1; call FHQ_DebugConsole_VehicleFilterList;";

			idc = 2100;
			x = 0.300186 * safezoneW + safezoneX;
			y = 0.266181 * safezoneH + safezoneY;
			w = 0.399629 * safezoneW;
			h = 0.0275082 * safezoneH;
		};
		class FHQ_DebugVehicle_Image: FHQRscPicture
		{
			style = 48 + ST_KEEP_ASPECT_RATIO;

			idc = 1200;
			text = "#(argb,8,8,3)color(1,1,1,0)";
			x = 0.487109 * safezoneW + safezoneX;
			y = 0.3075 * safezoneH + safezoneY;
			w = 0.212695 * safezoneW;
			h = 0.23375 * safezoneH;
		};
		class FHQ_DebugVehicle_Text: FHQRscStructuredText
		{
			idc = 1100;
			x = 0.487109 * safezoneW + safezoneX;
			y = 0.54125 * safezoneH + safezoneY;
			w = 0.212695 * safezoneW;
			h = 0.1925 * safezoneH;
		};
		////////////////////////////////////////////////////////
		// GUI EDITOR OUTPUT END
		////////////////////////////////////////////////////////

	};
};
