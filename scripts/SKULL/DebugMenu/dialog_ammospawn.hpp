
///////////////////////////////////////////////////////////////////////////
/// Ammo Spawn dialog
///////////////////////////////////////////////////////////////////////////


class FHQ_DebugDialog_AmmoSpawn
{
	idd = 140872;
	movingEnable = true;
	enableSimulation = true;
	class Objects
	{

	};

	class ControlsBackground
	{
		class FHQ_DebugMore_Backdrop: FHQRscText
		{
			idc = 1001;
			x = 0.29374 * safezoneW + safezoneX;
			y = 0.252426 * safezoneH + safezoneY;
			w = 0.41252 * safezoneW;
			h = 0.522655 * safezoneH;
			colorBackground[] = {0,0,0,0.4};
		};
	};

	class Controls
	{
		class FHQ_DebugWeapons_Header: FHQRscText
		{
			idc = 1000;
			text = "Spawn Weapon Boxes";
			x = 0.29374 * safezoneW + safezoneX;
			y = 0.224918 * safezoneH + safezoneY;
			w = 0.41252 * safezoneW;
			h = 0.0275082 * safezoneH;
			colorBackground[] = {0,0,0,1};
		};
		class FHQ_DebugWeapons_Close: FHQRscButton
		{
			action = "closeDialog 0";

			idc = 1600;
			text = "Close";
			x = 0.29374 * safezoneW + safezoneX;
			y = 0.747574 * safezoneH + safezoneY;
			w = 0.0902388 * safezoneW;
			h = 0.0275082 * safezoneH;
		};
		class FHQ_DebugClose_Back: FHQRscButton
		{
			action = "closeDialog 0; _handle = CreateDialog 'SKL_DEBUG_DIALOG';";

			idc = 1601;
			text = "Back";
			x = 0.390424 * safezoneW + safezoneX;
			y = 0.747574 * safezoneH + safezoneY;
			w = 0.0902388 * safezoneW;
			h = 0.0275082 * safezoneH;
		};
		class FHQ_DebugCosnole_AmmoBoxTypeSel: FHQRscCombo
		{
			onLBSelChanged  = "FHQ_DebugConsole_AmmoBoxFilterLast = _this select 1; call FHQ_DebugConsole_AmmoBoxFilterList;";

			idc = 2100;
			x = 0.300186 * safezoneW + safezoneX;
			y = 0.266181 * safezoneH + safezoneY;
			w = 0.399629 * safezoneW;
			h = 0.0275082 * safezoneH;
		};
		class FHQ_DebugWeapon_Selection: FHQRscListBox
		{
			onLBSelChanged = "[_this select 1] call FHQ_DebugConsole_SelectAmmoBox;";

			idc = 1500;
			x = 0.300186 * safezoneW + safezoneX;
			y = 0.307443 * safezoneH + safezoneY;
			w = 0.399629 * safezoneW;
			h = 0.426377 * safezoneH;
		};
		class FHQ_DebugConsole_SpawnBoxNow: FHQRscButton
		{
			action = "[] call FHQ_DebugConsole_SpawnAmmoBox;";

			idc = 1602;
			text = "Spawn";
			x = 0.60313 * safezoneW + safezoneX;
			y = 0.747574 * safezoneH + safezoneY;
			w = 0.10313 * safezoneW;
			h = 0.0275082 * safezoneH;
		};


	};
};
