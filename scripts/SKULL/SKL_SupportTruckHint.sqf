// SKL_SupportTruckHint.sqf
//
// Will add a action item to support vehicles to show hint with amount of stock (gas, ammo, repair) available
//
// in truck's init:  
//   _nil = this execVM "scripts\SKULL\SKL_SupportTruckHint.sqf";
//
// Version 1.1  5-30-14
// 1.1: use case insensitive compare as BIS changed case on some trucks
// 1.0: initial release

private ["_type","_title","_ammoTrucks","_fuelTrucks","_repairTrucks","_script","_error"];

if (isDedicated) exitWith {};

_type = toUpper (typeOf _this);

_ammoTrucks =   ["B_TRUCK_01_AMMO_F"    , "O_TRUCK_02_AMMO_F"   , "I_TRUCK_02_AMMO_F"   ];  
_fuelTrucks =   ["B_TRUCK_01_FUEL_F"    , "O_TRUCK_02_FUEL_F"   , "I_TRUCK_02_FUEL_F"   , "C_VAN_01_FUEL_F", "B_G_VAN_01_FUEL_F"];    
_repairTrucks = ["B_TRUCK_01_REPAIR_F"  , "O_TRUCK_02_BOX_F"    , "I_TRUCK_02_BOX_F"    ];  
  
_fuelScript = "hint parseText format[""<t size='2' color='#9ECFF9'>Tanker Fuel:<br/>%1 percent</t>"",  round (  getFuelCargo (_this select 0)*100)];";
_repairScript = "hint parseText format[""<t size='2' color='#9ECFF9'>Repairs:<br/>%1 percent</t>"",    round (getRepairCargo (_this select 0)*100)];";
_ammoScript = "hint parseText format[""<t size='2' color='#9ECFF9'>Ammo Supplies:<br/>%1 percent</t>"",round (  getAmmoCargo (_this select 0)*100)];";

_error = false;
_title = "error";
_script = "hint error";
switch true do
{
        case (_type in _fuelTrucks)  : { _title = "Get Tanker Fuel Level";      _script = _fuelScript}; 
        case (_type in _repairTrucks): { _title = "Get Repairs Supplies Level"; _script = _repairScript}; 
        case (_type in _ammoTrucks)  : { _title = "Get Ammo Supplies Level";    _script = _ammoScript};
        default {hint format ["Error in SupportTruckHint.sqf for vehicle %1:%2",_this,_type]; _error = true;};
};
if (!_error) then
{
    _this addAction [_title, _script];
};
