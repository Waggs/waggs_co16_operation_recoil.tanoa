//  SKL_PilotCheck.sqf
//  This will kick players out of helicopter pilot or copilot seats when the engine is running if
//  a pilot is not in one of the seats
//
//  Add to init.sqf:   _nil = [] execVM 'scripts\SKULL\SKL_PilotCheck.sqf'
//
//  Note: to allow non-pilot classes to fly, set an unit variable "SKL_PILOT_CHECK_ALLOW" to true
//        or to allow all units to fly set the global variable SKL_PILOT_CHECK_ALLOW_ALL to true

if (isDedicated) exitWith {};
private ["_allowed_pilots","_crewType","_pc","_trainedAir","_veh"];
_pc = (["PilotCheck",0] call BIS_fnc_getParamValue);
if(_pc == 0) exitWith {};
	
_trainedAir = [
"B_Heli_Light_01_armed_F","B_Heli_Light_01_F","B_Heli_Attack_01_F","B_Heli_Transport_01_F","B_Heli_Transport_01_camo_F","B_Plane_CAS_01_F",
"I_Heli_Transport_02_F","I_Plane_Fighter_03_AA_F","I_Plane_Fighter_03_CAS_F","I_Heli_light_03_F","I_Heli_light_03_unarmed_F",
"O_Heli_Light_02_F","O_Heli_Light_02_unarmed_F","O_Heli_Attack_02_F","O_Heli_Attack_02_black_F","O_Plane_CAS_02_F"
];

_allowed_pilots = ["B_Helipilot_F","B_helicrew_F","B_Pilot_F","O_helipilot_F","O_helicrew_F","O_Pilot_F","I_helipilot_F","I_helicrew_F","I_Pilot_F","C_man_pilot_F"];

_pilot = (typeOf player) in _allowed_pilots;

if (isNil "SKL_PILOT_CHECK_ALLOW_ALL") then {SKL_PILOT_CHECK_ALLOW_ALL = false; publicVariable "SKL_PILOT_CHECK_ALLOW_ALL"};

while{true} do {	
    if(vehicle player != player) then {
        _veh = vehicle player;
        if (isEngineOn _veh && !SKL_PILOT_CHECK_ALLOW_ALL) then
        {
            if((typeOf _veh) in _trainedAir && !_pilot) then {
                _unTrained = [driver _veh] + [_veh turretUnit [0]]; 	//_unTrained = [driver _veh]/*pilot*/ + [_veh turretUnit [0]]/*copilot*/ + [gunner _veh]/*gunner*/;
                if({(typeOf _x) in _allowed_pilots }count _unTrained == 0) then {
                    if (!(player getVariable ["SKL_PILOT_CHECK_ALLOW",false])) then
                    {
                        hint "You have not been trained to fly this aircraft!";
                        if ((getPosATL _veh) select 2 < 2) then 
                        {
                            player action ["getOut", _veh];
                            if (isEngineOn _veh) then {_veh engineOn false};
                        };
                    };
                };
            };
        };
    };
    sleep 2;
};		

			
