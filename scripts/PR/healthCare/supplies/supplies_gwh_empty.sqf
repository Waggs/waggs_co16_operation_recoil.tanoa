/*
supplies_gwh_empty.sqf
ver 1.0 - 2015-04-17 complete ACE medical system rework in progress
*/ 
if (isNil "removeBloodBag") then { removeBloodBag = false; publicVariable "removeBloodBag" }; 

0 = [] spawn { 
waitUntil {!isNull findDisplay 46}; 
    findDisplay 46 displayAddEventHandler ["KeyDown", { 
        if (_this select 1 in actionKeys "Gear") then [{ 
            _tents = nearestObjects [player modelToWorld [0,2,0], ["Land_TentDome_F"], 2]; 
            if (count _tents > 0) then [{ 
                _tent = _tents select 0; 
                medHolder = mash_man_tent getVariable ["medHolder", objNull]; 
                if (isNull medHolder) then { 
                    medHolder = createVehicle ["GroundWeaponHolder", [0,0,0], [], 0, "NONE"]; 
                    medHolder attachTo [mash_man_tent, [0,0,0]]; 
                    medHolder setVectorUp vectorUp mash_man_tent; 
                    mash_man_tent setVariable ["medHolder", medHolder, true]; 
                    detach medHolder; 
                }; 
                player action ["Gear", medHolder]; 
                true 
            },{false}]; 
        },{false}]; 
    }]; 
}; 

fnc_medInv = { 
    while { !(isNull mash_man_tent) } do { 
        if ((removeBloodBag) && !(isNull medHolder)) then { 
            _bloodbags = 0; 
            _items = []; 
            _smokes = 0; 
            _mags = []; 

            _bloodbags = (({"ACE_bloodIV" == _x} count (itemCargo medHolder)) - 1) max 0; 
            _items = (itemCargo medHolder) - ["ACE_bloodIV"]; 
            medHolder addMagazineCargoGlobal ["SmokeShellGreen", 1]; 
            _smokes = (({"SmokeShellGreen" == _x} count (MagazineCargo medHolder)) - 1) max 0; 
            _mags = (magazineCargo medHolder) - ["SmokeShellGreen"]; 

            clearItemCargoGlobal medHolder; 
            for "_i" from 0 to (count _items) do { medHolder addItemCargoGlobal [(_items select _i), 1]; }; 
            medHolder addItemCargoGlobal ["ACE_bloodIV", _bloodbags]; 
            clearMagazineCargoGlobal medHolder; 
            for "_i" from 0 to (count _mags) do { medHolder addMagazineCargoGlobal [(_mags select _i), 1]; }; 
            medHolder addMagazineCargoGlobal ["SmokeShellGreen", _smokes]; 
            sleep 0.05; 
            removeBloodBag = false; publicVariable "removeBloodBag"; 
        }; 
        sleep 2; 
    }; 
}; 

medHolder spawn fnc_medInv; 

