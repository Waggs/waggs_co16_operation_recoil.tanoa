/*
  place_supplies.sqf
  ver 1.0 - 2015-04-17 complete ACE medical system rework in progress
*/
_unit = player; 
_name = name player; 
//_sleep = 600; 
_sleep = ["MedicalSuppliesWait", 600] call BIS_fnc_getParamValue; 

if (isNil "placeMed") then { placeMed = false }; 

if (placeMed) then { 
    timeNow = floor(serverTime); 
    placeTimeLeft = _sleep - (timeNow - placeTime); 
    _min = floor (placeTimeLeft / 60); 
    _sec = floor (placeTimeLeft) - (60 * _min); 
    _secHand = ""; 
    if ((floor (placeTimeLeft / 60)) < 1) then { 
        _secHand = 'sec'; 
    } else { 
        _secHand = 'min'; 
    }; 
    hint parseText format ["%1<br/><t color='#ff5d00'>No Meds Available<br/>Wait %2m %3s for more Meds</t>", _name, _min, _sec, _secHand]; 
    sleep 15; 
    hintSilent ""; 
}; 

if !(placeMed) then { 
    placeMed = true; publicVariable "placeMed"; 
    placeTime = floor (serverTime); 
    ["scripts\PR\HealthCare\supplies\supplies_gwh.sqf", "BIS_fnc_execVM", true, true ] call BIS_fnc_MP; 
    _unit playAction "ReloadMagazine"; 
    sleep 3; 
    _unit playAction "ReloadMagazine"; 
    sleep 4; 
    hint parseText format ["%1<br/><t color='#19e56e'>You have placed<br/>Medical Supplies in the Tent</t>", _name]; 
    sleep _sleep; 
    placeMed = false; 
}; 