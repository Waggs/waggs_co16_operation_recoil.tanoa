/* 
*  Author: PapaReap 
*  Function names: pr_fnc_localHealthCare 
*  Local healthcare before server healthcare 
*  ver 1.0 - 2015-04-17 complete ACE medical system rework in progress 
*  ver 1.1 - 2015-05-25 convert to function 
*  ver 1.2 - 2015-07-04 changed who runs PR_healthCare_misc_fnc_missionEnd 
*  ver 1.3 - 2016-01-03 major script system rework in progress 
*/ 
if !(pr_deBug == 0) then { diag_log format ["*PR* fn_localHealthCare start"]; }; 

private ["_mash_deploy","_mash_man","_unitsArray"]; 
_end_mission    = mash_Array select 0; 
_mash_deploy    = mash_Array select 3; 
_mash_type      = mash_Array select 4; 
_mash_man       = mash_Array select 5; 
_unitsArray     = playable_units; 
playable_units  = []; 

{ call compile format ["playable_units = playable_units + ['%1']", _x] } forEach _unitsArray; 

#include "\A3\editor_f\Data\Scripts\dikCodes.h" 

mash_man_deployAction   = nil; 
mash_man_stowAction     = nil; 
getMedsAction           = nil; 
getDropAction           = nil; 

//fieldMash_deployAction  = nil; 
//fieldMash_stowAction    = nil; 

if (isNil "mash_man_deployed") then { mash_man_deployed = false }; 
if (isNil "fieldMash_deployed") then { fieldMash_deployed = false }; 

/* ========== initialise mash deployment script if player is mash man ========== */ 
if ((player == _mash_man) && (_mash_deploy == 1) && (_mash_type == 1)) then { 
    waitUntil { if (pr_fnc_initRead) exitWith { true }; };
    [_mash_man] spawn pr_fnc_mashTentInit; 
    mash_man = player; publicVariable "mash_man"; 
} else { 
    if ((player == _mash_man) && (_mash_deploy == 1) && (_mash_type == 2)) then { 
        waitUntil { if (pr_fnc_initRead) exitWith { true }; }; 
        [_mash_man] spawn pr_fnc_mashTentInit; 
        [_mash_man] spawn pr_fnc_fmInit; 
        mash_man = player; publicVariable "mash_man"; 
    }; 
}; 
/* ============ finish mash deployment script if player is mash man ============ */ 

if (isServer) then { mission_Over = false; publicVariable "mission_Over"; if (_end_mission == 1) then { [] spawn fnc_missionEnd }; }; 

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_localHealthCare complete"]; }; 
