/* fn_fmBuild.sqf
*  Author: PapaReap
*  ver 1.1 - 2016-01-03 major script system rework in progress
*  ver 1.0 - 2015-04-29 complete ACE medical system rework in progress
*/
if !(aceOn) exitWith {};
private ["_deployer","_pos_crate","_dir_crate","_tent"];
_deployer = _this select 0;
_mash     = nearestObject [_deployer, "B_Slingload_01_Medevac_F"];
_mash     = _mash getVariable "mashID";
_time     = Mash_Array select 11;
_deployer setVariable ["fieldMashAbort", 0, false];  //need to figure this one out
[_deployer, _mash] spawn fnc_deployTent;
sleep (_time + 1);

if (_deployer getVariable "fieldMashAbort" == 1) then {
    _pos_crate = getPosATL _mash;
    _dir_crate = getDir _mash;
    _mash enableSimulation false;
    _fieldMash_tent = objNull;
    _tent = "CamoNet_BLUFOR_big_F";

    _fieldMash_tent = _tent createVehicle [0,0,0];
    _fieldMash_tent setDir (_dir_crate);
    _fieldMash_tent setPos [(_pos_crate select 0) + (sqrt (4) * cos (155 - _dir_crate)), (_pos_crate select 1) + (sqrt (4) * sin (155 - _dir_crate)) , 0];
    _fieldMash_tent enableSimulation false;
    _fieldMash_tent allowDamage false;
    _mash setVariable ["fieldMash", 2, false];
    waitUntil { !(isNull _fieldMash_tent) };

    sleep 0.5;

    _grass = "Land_ClutterCutter_large_F";
    _grass = _grass createVehicle [(_pos_crate select 0) + (sqrt (4) * cos (101 - _dir_crate)), (_pos_crate select 1) + (sqrt (4) * sin (101 - _dir_crate)) , 0];

    _offsetX = cos(_dir_crate);
    _offsetY = sin(_dir_crate);

    _table = "Land_CampingTable_Small_F" createVehicle [(_pos_crate select 0) - 2, (_pos_crate select 1) + 2, (_pos_crate select 2) - 4];
    _table setDir (_dir_crate);
    _table enableSimulation false;
    _xLoc = (_pos_crate select 0) - 3.5*_offsetX + 5.5*_offsetY;
    _yLoc = (_pos_crate select 1) + 3.5*_offsetY + 5.5*_offsetX;
    _table setPos [_xLoc, _yLoc, 0 ];
    _pos_table = getPos _table;

    _defib = "Land_Defibrillator_F" createVehicle getPos _table;
    _defib setDir (_dir_crate - 90);
    _defib enableSimulation false;
    _defib setPos [_xLoc + 0.1 * _offsetY, _yLoc + 0.0 * _offsetX, 0.81];

    _item2 = "Land_Antibiotic_F" createVehicle getPos _table;
    _item2 enableSimulation false;
    _item2 setPos [_xLoc - 0.3 * _offsetX, _yLoc + 0.3 * _offsetY, 0.81];

    _item3 = "Land_Bandage_F" createVehicle getPos _table;
    _item3 enableSimulation false;
    _item3 setPos [_xLoc + 0.3 * _offsetX, _yLoc - 0.3 * _offsetY, 0.81];

    _bed = "Land_Sun_chair_green_F" createVehicle [0,0,0];
    _bed setDir (_dir_crate + 15);
    _bed setPos [(_pos_crate select 0) - 1.5*_offsetX + 5*_offsetY, (_pos_crate select 1) + 1.5*_offsetY + 5*_offsetX, 0];

    _bed2 = "Land_Sun_chair_green_F" createVehicle [0,0,0];
    _bed2 setDir (_dir_crate + 13);
    _bed2 setPos [(_pos_crate select 0) + 0.5*_offsetX + 5*_offsetY, (_pos_crate select 1) - 0.5*_offsetY + 5*_offsetX, 0];

    _box = "Land_PaperBox_open_empty_F" createVehicle [0,0,0];
    _box setDir (_dir_crate + 5);
    _box setPos [(_pos_crate select 0) - 3*_offsetX + 7*_offsetY, (_pos_crate select 1) + 3*_offsetY + 7*_offsetX, 0];

    _box2 = "Land_PaperBox_closed_F" createVehicle [0,0,0];
    _box2 setDir (_dir_crate - 15);
    _box2 setPos [(_pos_crate select 0) - 5*_offsetX + 7*_offsetY, (_pos_crate select 1) + 5*_offsetY + 7*_offsetX, 0];

    _box3 = "Land_PaperBox_closed_F" createVehicle [0,0,0];
    _box3 setDir (_dir_crate - 115);
    _box3 setPos [(_pos_crate select 0) - 7.5*_offsetX - 3*_offsetY, (_pos_crate select 1) + 7.5*_offsetY - 3*_offsetX, 0];

    _gen = "Land_Portable_generator_F" createVehicle [0,0,0];
    _gen setDir (_dir_crate + 100);
    _gen setPos [(_pos_crate select 0) - 8.5*_offsetX + 1*_offsetY, (_pos_crate select 1) + 8.5*_offsetY + 1*_offsetX, 0.1];

    _ant = "Land_TTowerSmall_1_F" createVehicle [0,0,0];
    _ant setDir (_dir_crate + 100);
    _ant setPos [(_pos_crate select 0) - 1.2*_offsetX + 3.3*_offsetY, (_pos_crate select 1) + 1.2*_offsetY + 3.3*_offsetX, 0.1];
    [[_fieldMash_tent, [10, 30], 4, "Boost", false], "fnc_radioBoost"] call BIS_fnc_MP;

    _lite = "Land_PortableLight_single_F" createVehicle [0,0,0];
    _lite setDir (_dir_crate - 90);
    _lite setPos [(_pos_crate select 0) - 8.5*_offsetX + 4*_offsetY, (_pos_crate select 1) + 8.5*_offsetY + 4*_offsetX, 0];

    _lite2 = "Land_PortableLight_single_F" createVehicle [0,0,0];
    _lite2 setDir (_dir_crate - 135);
    _lite2 setPos [(_pos_crate select 0) - 5.5*_offsetX - 6*_offsetY, (_pos_crate select 1) + 5.5*_offsetY - 6*_offsetX, 0];

    _lite3 = "Land_PortableLight_single_F" createVehicle [0,0,0];
    _lite3 setDir (_dir_crate + 25);
    _lite3 setPos [(_pos_crate select 0) + 3.5*_offsetX + 7.5*_offsetY, (_pos_crate select 1) - 3.5*_offsetY + 7.5*_offsetX, 0];

    _chair = "Land_CampingChair_V2_F" createVehicle [0,0,0];
    _chair setDir (_dir_crate + 60);
    _chair setPos [(_pos_crate select 0) + 4.0*_offsetX + 5.5*_offsetY, (_pos_crate select 1) - 4.0*_offsetY + 5.5*_offsetX, 0];

    _chair2 = "Land_CampingChair_V2_F" createVehicle [0,0,0];
    _chair2 setDir (_dir_crate + 0);
    _chair2 setPos [(_pos_crate select 0) + 0.0*_offsetX + 7*_offsetY, (_pos_crate select 1) - 0.0*_offsetY + 7*_offsetX, 0];

    _sign = "Sign_1L_Firstaid" createVehicle [0,0,0];
    _sign setDir (_dir_crate - 0);
    _sign setPos [(_pos_crate select 0) - 5.0*_offsetX - 6.8*_offsetY, (_pos_crate select 1) + 5.0*_offsetY - 6.8*_offsetX, 0];

    sleep 2;
    _wire = "ACE_ConcertinaWireCoil" createVehicle [0,0,0];
    _wire setDir (_dir_crate - 135);
    _wire setPos [(_pos_crate select 0) - 5.0 * _offsetX + 4.5 * _offsetY, (_pos_crate select 1) + 5.0 * _offsetY + 4.5 * _offsetX, 0];
    [_wire, _box2] call ace_cargo_fnc_loadItem;
    _wire2 = "ACE_ConcertinaWireCoil" createVehicle [0,0,0];
    _wire2 setDir (_dir_crate - 135);
    _wire2 setPos [(_pos_crate select 0) - 5.0 * _offsetX + 5.5 * _offsetY, (_pos_crate select 1) + 5.0 * _offsetY + 4.5 * _offsetX, 0];
    [_wire2, _box2] call ace_cargo_fnc_loadItem;
    _wire3 = "ACE_ConcertinaWireCoil" createVehicle [0,0,0];
    _wire3 setDir (_dir_crate - 135);
    _wire3 setPos [(_pos_crate select 0) - 5.0 * _offsetX + 6.5 * _offsetY, (_pos_crate select 1) + 5.0 * _offsetY + 4.5 * _offsetX, 0];
    [_wire3, _box2] call ace_cargo_fnc_loadItem;
    _wire4 = "ACE_ConcertinaWireCoil" createVehicle [0,0,0];
    _wire4 setDir (_dir_crate - 135);
    _wire4 setPos [(_pos_crate select 0) - 5.0 * _offsetX + 6.5 * _offsetY, (_pos_crate select 1) + 5.0 * _offsetY + 4.5 * _offsetX, 0];
    [_wire4, _box2] call ace_cargo_fnc_loadItem;
    _wire5 = "ACE_ConcertinaWireCoil" createVehicle [0,0,0];
    _wire5 setDir (_dir_crate - 135);
    _wire5 setPos [(_pos_crate select 0) - 5.0 * _offsetX + 6.5 * _offsetY, (_pos_crate select 1) + 5.0 * _offsetY + 4.5 * _offsetX, 0];
    [_wire5, _box2] call ace_cargo_fnc_loadItem;

    _AmmoBox = "ACE_medicalSupplyCrate" createVehicle [0,0,0];
    _AmmoBox setDir (_dir_crate - 135);
    _AmmoBox setPos [(_pos_crate select 0) - 5.0 * _offsetX + 4.5 * _offsetY, (_pos_crate select 1) + 5.0 * _offsetY + 4.5 * _offsetX, 0];
    [[_AmmoBox], "fnc_aceMedBox"] call BIS_fnc_MP;
    [_AmmoBox, _box3] call ace_cargo_fnc_loadItem;

    _AmmoBox2 = "ACE_Box_Misc" createVehicle [0,0,0];
    _AmmoBox2 setDir (_dir_crate - 135);
    _AmmoBox2 setPos [(_pos_crate select 0) - 5.0 * _offsetX + 4.5 * _offsetY, (_pos_crate select 1) + 5.0 * _offsetY + 4.5 * _offsetX, 0];
    [[_AmmoBox2], "fnc_aceDefend"] call BIS_fnc_MP;
    [_AmmoBox2, _box2] call ace_cargo_fnc_loadItem;

    _mash setVariable ["ammoBox", "true", true];
    _mash enableSimulation true;

    _mash setVariable ["bloodCount", 0, true];
    _mash setVariable ["removeBlood", 0, true];
    [[_mash], "pr_fnc_bloodCargo"] call BIS_fnc_MP;
    [[_mash], "pr_fnc_crateBloodInv"] call BIS_fnc_MP;
    [[_fieldMash_tent, 8, 1, 1, _mash], "pr_fnc_mashTreatment"] call BIS_fnc_MP;
    [[_fieldMash_tent, 8, 1], "pr_fnc_mashTreatmentClient"] call BIS_fnc_MP;

    _mash setVariable ["fieldMash_deployed", "true", true];

    _deployAction = _mash getVariable "deployAction";
    _deployer removeAction _deployAction;
    _deployer setVariable ["fieldMashAbort", 0, false];

    _medArray = [_fieldMash_tent, _table, _defib, _bed, _bed2, _box, _box2, _box3, _gen, _ant, _lite, _lite2, _lite3, _chair, _chair2, _sign, _item2, _item3];
    _mash setVariable ["medArray", _medArray, true];
} else {
    if (_deployer getVariable "fieldMashAbort" == 2) then {
        _mash setVariable ["fieldMash_deployed", "false", true];
        _deployer setVariable ["fieldMashAbort", 0, false];
    };
};
