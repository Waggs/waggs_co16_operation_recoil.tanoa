/* 
*  Author: PapaReap 
*  Function names: fnc_deployTent, fnc_deployTentCall & fnc_deployTentAbort 
*  ver 1.1 - 2016-01-03 major script system rework in progress 
*  ver 1.0 - 2015-04-17 complete ACE medical system rework in progress PR_fnc_deployTentCallInit
*/ 

fnc_deployTent = { 
    private ["_deployer", "_time", "_name", "_string"]; 
    _deployer  = _this select 0; 
    _name = ""; 
    _type = ""; 
    if (count _this > 1) then { _name = "Field Mash"; _type = "mash" } else { _name = "Medical Tent"; _type = "tent" }; 
    _time  = mash_Array select 11; 

    if !(isNil "LeadMedic") then { 
        if (_deployer == LeadMedic) then { Medic_1 = _deployer; } else { if !(isNil "p1") then { if (_deployer == p1) then { Medic_2 = _deployer }; }; }; 
    } else { if !(isNil "p1") then { if (_deployer == p1) then { Medic_2 = _deployer }; }; }; 

    [_deployer, "AinvPknlMstpSnonWnonDr_medic5", 0] call ace_common_fnc_doAnimation; 
    _string = format ["Deploying %1", _name]; 
    mashT = _type; 

    if !(isNil "Medic_1") then { 
        if (Medic_1 == player) then { 
            [_time, [], { [Medic_1, mashT] spawn fnc_deployTentCall }, { [Medic_1, mashT] spawn fnc_deployTentAbort }, _string] call ace_common_fnc_progressBar; 
        } else { 
            if !(isNil "Medic_2") then { 
                if (Medic_2 == player) then { [_time, [], { [Medic_2, mashT] spawn fnc_deployTentCall }, { [Medic_2, mashT] spawn fnc_deployTentAbort }, _string] call ace_common_fnc_progressBar }; 
            }; 
        }; 
    } else { 
        if !(isNil "Medic_2") then { 
            if (Medic_2 == player) then { [_time, [], { [Medic_2, mashT] spawn fnc_deployTentCall }, { [Medic_2, mashT] spawn fnc_deployTentAbort }, _string] call ace_common_fnc_progressBar }; 
        }; 
    }; 
}; 

fnc_deployTentCall = { 
    private ["_deployer","_type","_name"]; 
    _deployer = _this select 0; 
    _type = _this select 1; 
    if (_type == "tent") then { 
        _name = "Medical Tent is Deployed"; _deployer setVariable ["tentAbort", 1, false]; 
    } else { 
        _name = "Field Mash is Deployed"; _deployer setVariable ["fieldMashAbort", 1, false]; 
    }; 
    [_deployer, "AmovPknlMstpSrasWrflDnon", 1] call ace_common_fnc_doAnimation; 
    [parseText _name] call ace_common_fnc_displayTextStructured; 
}; 

fnc_deployTentAbort = { 
    private ["_deployer"]; 
    _deployer = _this select 0; 
    _type = _this select 1; 
    if (_type == "tent") then { 
        if (!isNil "mash_man_deployAction") then { _deployer removeAction mash_man_deployAction; publicVariable "mash_man_deployAction" }; 
        if (!mash_man_deployed) then { _deployer setVariable ["mash", 0, false] } else { _deployer setVariable ["mash", 1, false] }; 
        _deployer setVariable ["tentAbort", 2, false]; 
    } else { 
        _mash = nearestObject [_deployer, "B_Slingload_01_Medevac_F"]; 
        _mash = _mash getVariable "mashID"; 
        _deployAction = _mash getVariable "deployAction"; 
        _deployer removeAction _deployAction; 
        if (_mash getVariable "fieldMash_deployed" == "false") then { 
            _mash setVariable ["fieldMash", 0, true]; 
        } else { 
            _mash setVariable ["fieldMash", 1, true]; 
        }; 
        _deployer setVariable ["fieldMashAbort", 2, false]; 
    }; 
    [_deployer, "AmovPknlMstpSrasWrflDnon", 1] call ace_common_fnc_doAnimation; 
}; 
