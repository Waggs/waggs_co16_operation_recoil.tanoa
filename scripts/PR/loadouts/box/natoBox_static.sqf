//--- natoBox_static.sqf
//--- Add the following to the init field of an ammo box in the editor
//--- _nil = this execVM "scripts\PR\loadouts\box\natoBox_static.sqf";
//--- When adding items, make sure they are in the correct sections (weapons/ammo/items..) 
//--- and make sure the last item in each group doesn't have a comma at the end of the line

[_this,
//--- <1> Add Nato Common weapons
[],
//--- <2> Add Nato Arma 3 Weapons
[], 
//--- <3> Add Nato Massi Weapons
[],
//--- <4> Add Nato Common Ammo
[],
//--- <5> Add Nato Arma 3 Ammo
[],
//--- <6> Add Nato Massi Ammo
[],
//--- <7> Add Nato Common Items
[
	["B_UavTerminal",						 5 ]
],
//--- <8> Add Nato Arma 3 Items
[],
//--- <9> Add Nato Massi Items
[],
//--- <10> Add Nato Common Backpacks 
[
	["B_UAV_01_backpack_F",			6 ],

	["B_HMG_01_A_weapon_F",			2 ],	//Dismantled Autonomous MG 
	["B_HMG_01_weapon_F",			2 ],	//Dismantled Mk30 HMG
	["B_GMG_01_A_weapon_F",			2 ],	//Dismantled Autonomous GMG 
	["B_GMG_01_weapon_F",			2 ],	//Dismantled Mk32 GMG
	["B_AA_01_weapon_F",			2 ],	//Static Titan Launcher [AA]
	["B_AT_01_weapon_F",			2 ],	//Static Titan Launcher [AT]
	["B_HMG_01_support_F",			6 ],	//Folded Tripod

	["B_HMG_01_high_weapon_F",		2 ],	//Dismantled Mk30 HMG [Raised]
	["B_GMG_01_high_weapon_F",		2 ],	//Dismantled Mk32 GMG [Raised]
	["B_HMG_01_support_high_F",		4 ],	//Folded Tripod Raised

	["B_Mortar_01_support_F",		2 ],	//Folded Mk6 Morter Bipod
	["B_Mortar_01_weapon_F",		2 ]		//Folded Mk6 Morter Tube
],
//--- <11> Add Nato Arma 3 Backpacks
[],
//--- <12> Add Nato Massi Backpacks
[],
//--- <13> How many backup short range radios?
0
]
execVM "scripts\PR\loadouts\box\box_fill.sqf";
