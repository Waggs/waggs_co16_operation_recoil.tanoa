//--- natoBox_launchers.sqf
//--- Add the following to the init field of an ammo box in the editor
//--- _nil = this execVM "scripts\PR\loadouts\box\natoBox_launchers.sqf";
//--- When adding items, make sure they are in the correct sections (weapons/ammo/items..) 
//--- and make sure the last item in each group doesn't have a comma at the end of the line

[_this,
//--- <1>Add Nato Common weapons
[
	["launch_B_Titan_F",					 5 ],
	["launch_B_Titan_short_F",				 5 ],
	["launch_NLAW_F",						 5 ]
],
//--- <2>Add Nato Arma 3 Weapons
[], 
//--- <3>Add Nato Massi Weapons
[
	["mas_launch_maaws_F",					 5 ],
	["launch_RPG32_F",				 		 5 ]
],
//--- <4>Add Nato Common Ammo
[
[TiAT, 10 ],
[TiAA, 10 ],
	["NLAW_F",                            	10 ],
	["Titan_AP",                          	10 ]
],
//--- <5>Add Nato Arma 3 Ammo
[],
//--- <6>Add Nato Massi Ammo
[
	["mas_MAAWS",							10 ],
	["RPG32_F",                         	10 ]
],
//--- <7>Add Nato Common Items
[],
//--- <8>Add Nato Arma 3 Items
[],
//--- <9>Add Nato Massi Items
[],
//--- <10>Add Nato Common Backpacks
[],
//--- <11>Add Nato Arma 3 Backpacks
[],
//--- <12>Add Nato Massi Backpacks
[],
//--- <13> How many backup short range radios?
0
]
execVM "scripts\PR\loadouts\box\box_fill.sqf";
