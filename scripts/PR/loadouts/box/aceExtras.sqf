/*
_nil = [this,0,0,0,0,0] execVM "scripts\PR\loadouts\box\aceExtras.sqf";
_nil = [this,Jerry Can,Razor Wire Crate,Sandbag Barrier Crate,Spare Track,Spare Wheel] execVM "scripts\PR\loadouts\box\aceExtras.sqf";
max 10 items each
*/
 private ["_vehicle","_jerry_can","_RazorWire","_Sandbag","_spare_track","_spare_wheel","_i"];
_vehicle		= _this select 0;
_jerry_can		= _this select 1;
_RazorWire		= _this select 2;
_Sandbag		= _this select 3;
_spare_track	= _this select 4;
_spare_wheel	= _this select 5;

waitUntil {!isNil "AGM_Logistics_loadedItemsDummy"};
if isServer then {
	for "_i" from 1 to ((_jerry_can) Min 10) do {
		_vehicle spawn {
			private ["_item"];
			_item = ['AGM_JerryCan', [-1000, -1000, 100]] call AGM_Logistics_fnc_spawnObject;
			[_this, _item] call AGM_Logistics_fnc_initLoadedObject;
		};
	};
	
	for "_i" from 1 to ((_RazorWire) Min 10) do {
		_vehicle spawn {
			private ["_item"];
			_item = ['AGM_RazorWire_Crate', [-1000, -1000, 100]] call AGM_Logistics_fnc_spawnObject;
			[_this, _item] call AGM_Logistics_fnc_initLoadedObject;
		};
	};

	for "_i" from 1 to ((_Sandbag) Min 10) do {
		_vehicle spawn {
			private ["_item"];
			_item = ['AGM_SandbagBarrier_Crate', [-1000, -1000, 100]] call AGM_Logistics_fnc_spawnObject;
			[_this, _item] call AGM_Logistics_fnc_initLoadedObject;
		};
	};

	for "_i" from 1 to ((_spare_track) Min 10) do {
		_vehicle spawn {
			private ["_item"];
			_item = ['AGM_SpareTrack', [-1000, -1000, 100]] call AGM_Logistics_fnc_spawnObject;
			[_this, _item] call AGM_Logistics_fnc_initLoadedObject;
		};
	};

	for "_i" from 1 to ((_spare_wheel) Min 10) do {
		_vehicle spawn {
			private ["_item"];
			_item = ['AGM_SpareWheel', [-1000, -1000, 100]] call AGM_Logistics_fnc_spawnObject;
			[_this, _item] call AGM_Logistics_fnc_initLoadedObject;
		};
	};
};