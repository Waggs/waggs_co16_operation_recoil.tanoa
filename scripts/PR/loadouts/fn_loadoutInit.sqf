/* fn_loadoutInit.sqf
*  Author: PapaReap
*  Ver 1.0 2015-11-26 restructure loadout calls
*  Ver 1.1 2015-12-24 optimize lines
*  Ver 1.2 2016-05-25 adding opfor units
*/
if !(pr_deBug == 0) then { diag_log format ["*PR* fn_loadoutInit start"]; };
/*
defaultWeapon = false; masWeapon  = false; masWeapon5 = false; masWeapon6 = false; masWeapon7 = false; masWeapon8 = false; rhsWeapon  = false;
if (
    (prWeapons <= 0)
    || (((prWeapons == 5) || (prWeapons == 6) || (prWeapons == 7) || (prWeapons == 8)) && !(masOn))
    || ((prWeapons == 10) && !(rhsOn))
    || (((prWeapons == 15) || (prWeapons == 16) || (prWeapons == 17) || (prWeapons == 18) || (prWeapons == 19)) && !(unsungOn))
) then {
    defaultWeapon = true;
} else {
    if (((prWeapons == 5) || (prWeapons == 6) || (prWeapons == 7) || (prWeapons == 8)) && (masOn)) then {
        masWeapon = true;
        if (prWeapons == 5) then {
            masWeapon5 = true;
        } else {
            if (prWeapons == 6) then {
                masWeapon6 = true;
            } else {
                if (prWeapons == 7) then {
                    masWeapon7 = true;
                } else {
                    if (prWeapons == 8) then {
                        masWeapon8 = true;
                    };
                };
            };
        };
    } else {
        if ((prWeapons == 10) && (rhsOn)) then {
            rhsWeapon = true;
        };
    };
};

// temp fix until loadouts finished, remove once weapons added
if !(defaultWeapon) then {
    if ((prWeapons == 15) || (prWeapons == 16) || (prWeapons == 17) || (prWeapons == 18) || (prWeapons == 19)) then {
        if ((unsungOn) && (rhsOn)) then { 
            rhsWeapon = true;
        } else {
            defaultWeapon = true;
        };
    };
};

playWest = false; playEast = false; playRes  = false;
if (side player == west) then { playWest = true; } else { if (side player == east) then { playEast = true; } else { if (side player == resistance) then { playRes = true; }; }; };
*/
waitUntil { !(isNil "objectVarsCompiled") };

//--- call scripts
[] call compile preprocessFileLineNumbers "scripts\PR\loadouts\units\fn_common.sqf";
fnc_gear = compile preprocessFileLineNumbers "scripts\PR\loadouts\units\fn_gear.sqf";
[] call compile preprocessFileLineNumbers "scripts\PR\loadouts\box\fn_customBox.sqf";

if !(aceOn) then { 0 = [] execVM "scripts\PR\loadouts\saveLoadout.sqf"; };

if !(pr_deBug == 0) then {
    diag_log format ["*PR* fn_loadoutInit complete"];
    diag_log format ["*PR* isNull Player %1, Local Player %2", (isNull Player), (local player)];
};
