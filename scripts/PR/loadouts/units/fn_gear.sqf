	/*
*  Author: PapaReap
*  File used by fn_Client.sqf
*  Change the loadout as required by your mission
*  Ver 1.0 2014-02-13 initial release
*  Ver 1.1 2015-01-15 allow for ace
*  Ver 1.2 2015-10-18 total loadout rework
*  Ver 1.3 2015-12-25 continued code optimization
*  Ver 1.4 2016-02-18 complete rework
*  Ver 1.5 2016-05-25 adding opfor units
*/

if (playWest) then {
    // Helmets
    hAh = "H_HelmetB";
    hAh2 = "H_HelmetB_light";
    hAh3 = "H_HelmetB_paint";
    hAcap = "H_Cap_tan_specops_US";
    hAcapHS = "H_Cap_oli_hs";
    hAhch = "H_CrewHelmetHeli_B";
    // Uniforms
    uAc = "U_B_CombatUniform_mcam";
    uAc2 = "U_B_CombatUniform_mcam_tshirt";
    uAc3 = "U_B_CombatUniform_mcam_vest";
    uAg = "U_B_GhillieSuit";
    uAga = "U_B_FullGhillie_ard";
    uActu = "U_B_CTRG_Soldier_urb_1_F";
    // Vests
    vAc = "V_Chestrig_rgr";
    vAp2 = "V_PlateCarrier2_rgr";
    vApgl = "V_PlateCarrierGL_rgr";
    vApcs = "V_PlateCarrierSpec_mtp";
    // Backpacks
    bAa = "B_AssaultPack_mcamo";
    bAc = "B_Carryall_mcamo";
    bAf = "B_FieldPack_blk";
    bAuav = "B_UAV_01_backpack_F";
} else {
    if (playEast) then {
        // Helmets
        hAh = "H_HelmetO_ocamo";
        hAh2 = "H_HelmetSpecO_ocamo";
        hAh3 = "H_HelmetLeaderO_ocamo";
        hAcap = "H_Cap_brn_SPECOPS";
        hAcapHS = "H_MilCap_ocamo";
        // Uniforms
        uAc = "U_O_CombatUniform_ocamo";
        uAc2 = "U_O_OfficerUniform_ocamo";
        uAc3 = "U_O_SpecopsUniform_ocamo";
        uAg = "U_O_GhillieSuit";
        uAga = "U_O_FullGhillie_ard";
        // Vests
        vAc = "V_Chestrig_khk";
        vAp2 = "V_HarnessO_brn";
        vApgl = "V_HarnessOGL_brn";
        // Backpacks
        bAa = "B_AssaultPack_ocamo";
        bAc = "B_Carryall_ocamo";
        bAf = "B_FieldPack_ocamo";
        bAuav = "O_UAV_01_backpack_F";
    };
};

// Universal
// Helmets
hAbb = "H_Beret_blk";

// Backpacks
bAab = "B_AssaultPack_blk";
bAak = "B_Kitbag_cbr";
bAam = "B_Kitbag_mcamo";
bAcc = "B_Carryall_cbr";
bAck = "B_Carryall_khk";
bAco = "B_Carryall_oli";
bAat = "B_AssaultPack_tna_F";

if (tfarOn) then {
    if (playWest) then {
        bTF = "tf_rt1523g_black";
        bTF2 = "tf_rt1523g_big_bwmod";
        bTF3 = "tf_anprc155_coyote";
        bTF4 = "tf_mr3000_bwmod";
    } else {
        if (playEast) then {
            bTF = "tf_rt1523g_sage";
            bTF2 = "tf_rt1523g_big_rhs";
            bTF3 = "tf_mr3000";
            bTF4 = "tf_mr3000_multicam";
        };
    };
} else {
    bTF = bAf;
    bTF2 = bAf;
    bTF3 = bAf;
    bTF4 = bAf;
};

// Backpacks - ACE3
if (aceOn) then { bACEl = "ACE_TacticalLadder_Pack" } else { bACEl = bAc };

if (nqdyOn) then {
    // Uniforms
    uNQlu = "nqdy_leader_uniform";
    uNQmu = "nqdy_medic_uniform";
    // Helmets
    hNQsl = "nqdy_squad_leader_helmet";
    hNQm = "nqdy_medic_helmet";
    hNQsc = "nqdy_units_sniper_cap";
    // Vests
    vNQsl = "nqdy_squad_leader_vest";
    vNQtl = "nqdy_team_leader_vest";
    vNQmv = "nqdy_medic_vest";
    //Backpacks
    bNQatc = "nqdy_at_carryall";
    bNQdc = "nqdy_demo_carryall";
    bNQmc = "nqdy_medic_carryall";
    bNQsc = "nqdy_sniper_carryall";
    bNQuc = "nqdy_unit_carryall";
} else {
    // Uniforms
    uNQlu = uAc;
    uNQmu = uAc;
    // Helmets
    hNQsl = hAh;
    hNQm = hAh2;
    hNQsc = hAh3;
    // Vests
    vNQsl = vAp2;
    vNQtl = vAp2;
    vNQmv = vAp2;
    //Backpacks
    bNQatc = bAc;
    bNQdc = bAc;
    bNQmc = bAc;
    bNQsc = bAc;
    bNQuc = bAc;
};

fnc_defaultGear = {
    // Helmets
    slHelmet = hNQsl;
    tlHelmet = hAbb;
    mglHelmet = hAh;
    mghHelmet = hAh;
    gllHelmet = hAh3;
    glhHelmet = hAh3;
    atHelmet = hAh2;
    atsHelmet = hAh2;
    aalHelmet = hAh;
    aahHelmet = hAh;
    engHelmet = hAh3;
    medHelmet = hAh2;
    jtacHelmet = hAh3;
    abmHelmet = hAh3;
    mkHelmet = hAcapHS;
    snipHelmet = hAcapHS;
    ladHelmet = hAh3;
    pilHelmet = hAhch;
    // Uniforms
    slUniform = uNQlu;
    tlUniform = uNQlu;
    mglUniform = uAc;
    mghUniform = uAc;
    gllUniform = uAc3;
    glhUniform = uAc3;
    atUniform = uAc;
    atsUniform = uAc;
    aalUniform = uAc2;
    aahUniform = uAc2;
    engUniform = uAc3;
    medUniform = uAc2;
    jtacUniform = uAc3;
    abmUniform = uAc3;
    mkUniform = uAc2;
    snipUniform = uAc2;
    ladUniform = uAc3;
    pilUniform = uActu;
    // Vests
    slVest = vNQsl;
    tlVest = vNQtl;
    mglVest = vApgl;
    mghVest = vApgl;
    gllVest = vApgl;
    glhVest = vApgl;
    atVest = vAp2;
    atsVest = vAp2;
    aalVest = vAp2;
    aahVest = vAp2;
    engVest = vAc;
    medVest = vAc;
    jtacVest = vAc;
    abmVest = vApgl;
    mkVest = vAp2;
    snipVest = vAp2;
    ladVest = vApgl;
    pilVest = vApgl;
    // Backpacks
    slPack = bTF2;
    tlPack = bTF2;
    mglPack = bAc;
    mghPack = bAa;
    gllPack = bAa;
    glhPack = bAc;
    atPack = bAc;
    atsPack = bAc;
    aalPack = bAc;
    aahPack = bAc;
    engPack = bAc;
    medPack = bAc;
    jtacPack = bAuav;
    abmPack = bAc;
    mkPack = bAam;
    snipPack = bAa;
    ladPack = bACEl;
    pilPack = bTF;
};

if (defaultWeapon) then {
    [] call fnc_defaultGear;
} else {
    if (tropicWeapon) then {
        // Helmets
        hAth = "H_HelmetB_Enh_tna_F";
        hAth2 = "H_HelmetB_tna_F";
        hAth3 = "H_Booniehat_tna_F";
        slHelmet = hAth;
        tlHelmet = hAth;
        mglHelmet = hAth2;
        mghHelmet = hAth2;
        gllHelmet = hAth2;
        glhHelmet = hAth2;
        atHelmet = hAth2;
        atsHelmet = hAth2;
        aalHelmet = hAth2;
        aahHelmet = hAth2;
        engHelmet = hAth;
        medHelmet = hAth2;
        jtacHelmet = hAth2;
        abmHelmet = hAth2;
        mkHelmet = hAth3;
        snipHelmet = hAth3;
        ladHelmet = hAth2;
        pilHelmet = hAhch;
        // Uniforms
        uAtu = "U_B_T_Soldier_SL_F";
        uAtu2 = "U_B_T_Soldier_AR_F";
        uAtu3 = "U_B_T_Soldier_F";
        slUniform = uAtu3;
        tlUniform = uAtu3;
        mglUniform = uAtu2;
        mghUniform = uAtu2;
        gllUniform = uAtu2;
        glhUniform = uAtu2;
        atUniform = uAtu2;
        atsUniform = uAtu2;
        aalUniform = uAtu;
        aahUniform = uAtu;
        engUniform = uAtu2;
        medUniform = uAtu;
        jtacUniform = uAtu3;
        abmUniform = uAtu2;
        mkUniform = uAtu3;
        snipUniform = uAtu3;
        ladUniform = uAtu3;
        pilUniform = uActu;
        // Vests
        vAtv = "V_PlateCarrierGL_tna_F";
        vAtv2 = "V_PlateCarrier1_tna_F";
        vAtv3 = "V_PlateCarrierSpec_tna_F";
        vAtv4 = "V_PlateCarrier2_tna_F";
        slVest = vAtv2;
        tlVest = vAtv2;
        mglVest = vAtv4;
        mghVest = vAtv4;
        gllVest = vAtv;
        glhVest = vAtv;
        atVest = vAtv4;
        atsVest = vAtv4;
        aalVest = vAtv4;
        aahVest = vAtv4;
        engVest = vAtv3;
        medVest = vAtv2;
        jtacVest = vAtv2;
        abmVest = vAtv4;
        mkVest = vAtv2;
        snipVest = vAtv2;
        ladVest = vAtv4;
        pilVest = vAtv4;
        // Backpacks
        slPack = bTF2;
        tlPack = bTF2;
        mglPack = bAco;
        mghPack = bAat;
        gllPack = bAat;
        glhPack = bAco;
        atPack = bAco;
        atsPack = bAco;
        aalPack = bAco;
        aahPack = bAco;
        engPack = bAco;
        medPack = bAco;
        jtacPack = bAuav;
        abmPack = bAco;
        mkPack = bAat;
        snipPack = bAat;
        ladPack = bACEl;
        pilPack = bTF;
    } else {
        if (masWeapon5) then { // Massi - UK
            // Helmets
            hMUhmsg = "H_mas_uk_helmet_mich_sf_g";
            hMUhmshg = "H_mas_uk_helmet_mich_sf_h_g";
            hMUhmsgg = "H_mas_uk_helmet_mich_sf_gog_g";
            hMUbhr = "H_mas_uk_Booniehat_rgr";
            hMUmcmv = "H_mas_uk_MilCap_mcamo_v";
            hMUb = "H_mas_uk_Bandana";
            hMUrchb = "H_mas_uk_revcapheadset_b";
            slHelmet = hNQsl;
            tlHelmet = hAbb;
            mglHelmet = hMUmcmv;
            mghHelmet = hMUhmsg;
            gllHelmet = hMUbhr;
            glhHelmet = hMUhmsgg;
            atHelmet = hMUb;
            atsHelmet = hMUhmsg;
            aalHelmet = hMUrchb;
            aahHelmet = hMUhmshg;
            engHelmet = hMUhmsg;
            medHelmet = hNQm;
            jtacHelmet = hMUhmsg;
            abmHelmet = hMUbhr;
            mkHelmet = hMUrchb;
            snipHelmet = hMUrchb;
            ladHelmet = hMUbhr;
            pilHelmet = hAhch;          //Needs Mas Equiv
            // Uniforms
            uMUbi1v = "U_mas_uk_B_IndUniform1_v";
            uMUbi2v = "U_mas_uk_B_IndUniform2_v";
            uMUbcvt = "U_mas_uk_B_CombatUniform_veg_tshirt";
            uMUbcvv = "U_mas_uk_B_CombatUniform_veg1_vest";
            uMUbi = "U_mas_uk_B_IndUniformx2";
            slUniform = uNQlu;
            tlUniform = uNQlu;
            mglUniform = uMUbi2v;
            mghUniform = uMUbi1v;
            gllUniform = uMUbcvt;
            glhUniform = uMUbi2v;
            atUniform = uMUbcvv;
            atsUniform = uMUbi2v;
            aalUniform = uMUbi;
            aahUniform = uMUbi2v;
            engUniform = uMUbcvv;
            medUniform = uNQmu;
            jtacUniform = uMUbcvv;
            abmUniform = uMUbcvt;
            mkUniform = uMUbcvt;
            snipUniform = uMUbcvt;
            ladUniform = uMUbcvt;
            pilUniform = uActu;        //Needs Mas Equiv
            // Vests
            vMUprt = "V_mas_uk_PlateCarrier2_rgr_t";
            vMUprv = "V_mas_uk_PlateCarrier2_rgr_v";
            vMUpglrv = "V_mas_uk_PlateCarrierGL_rgr_v";
            slVest = vNQsl;
            tlVest = vNQtl;
            mglVest = vMUprt;
            mghVest = vMUprv;
            gllVest = vMUprv;
            glhVest = vMUprv;
            atVest = vMUprv;
            atsVest = vMUprv;
            aalVest = vMUprv;
            aahVest = vMUprv;
            engVest = vMUpglrv;
            medVest = vNQmv;
            jtacVest = vMUpglrv;
            abmVest = vMUpglrv;
            mkVest = vMUprv;
            snipVest = vMUprv;
            ladVest = vMUprv;
            pilVest = vMUprv;
            // Backpacks
            slPack = bTF2;
            tlPack = bTF2;
            mglPack = bNQuc;
            mghPack = bNQuc;
            gllPack = bAa;
            glhPack = bNQuc;
            atPack = bNQatc;
            atsPack = bNQatc;
            aalPack = bNQatc;
            aahPack = bNQatc;
            engPack = bNQdc;
            medPack = bNQmc;
            jtacPack = bAuav;
            abmPack = bNQuc;
            mkPack = bAam;
            snipPack = bAam;
            ladPack = bACEl;
            pilPack = bTF;
        } else {
            if (masWeapon6) then { // Massi - UK Winter
                // Helmets
                hMUhmsw = "H_mas_uk_helmet_mich_sf_w";
                hMUhmshw = "H_mas_uk_helmet_mich_sf_h_w";
                hMUhmsgw = "H_mas_uk_helmet_mich_sf_gog_w";
                hMUwhw = "H_mas_uk_woolhat_ht_w";
                slHelmet = hNQsl;
                tlHelmet = hMUhmsw;
                mglHelmet = hMUhmsw;
                mghHelmet = hMUhmsw;
                gllHelmet = hMUhmsgw;
                glhHelmet = hMUhmsw;
                atHelmet = hMUhmsw;
                atsHelmet = hMUhmsgw;
                aalHelmet = hMUhmsw;
                aahHelmet = hMUhmshw;
                engHelmet = hMUhmsgw;
                medHelmet = hNQm;
                jtacHelmet = hMUhmshw;
                abmHelmet = hMUhmsgw;
                mkHelmet = hMUwhw;
                snipHelmet = hMUwhw;
                ladHelmet = hMUhmsgw;
                pilHelmet = hAhch;          //Needs Mas Equiv
                // Uniforms
                uMUbw = "U_mas_uk_B_wint";   // hood up
                uMUbw2 = "U_mas_uk_B_wint2"; // hood down
                slUniform = uMUbw2;
                tlUniform = uMUbw2;
                mglUniform = uMUbw;
                mghUniform = uMUbw2;
                gllUniform = uMUbw;
                glhUniform = uMUbw2;
                atUniform = uMUbw2;
                atsUniform = uMUbw2;
                aalUniform = uMUbw;
                aahUniform = uMUbw2;
                engUniform = uMUbw2;
                medUniform = uMUbw2;
                jtacUniform = uMUbw;
                abmUniform = uMUbw;
                mkUniform = uMUbw2;
                snipUniform = uMUbw2;
                ladUniform = uMUbw2;
                pilUniform = uActu;        //Needs Mas Equiv
                // Vests
                vMUp1rw = "V_mas_uk_PlateCarrier1_rgr_w";
                vMUcrw = "V_mas_uk_ChestrigB_rgr_w";
                slVest = vNQsl;
                tlVest = vNQtl;
                mglVest = vMUp1rw;
                mghVest = vMUp1rw;
                gllVest = vMUp1rw;
                glhVest = vMUp1rw;
                atVest = vMUp1rw;
                atsVest = vMUcrw;
                aalVest = vMUp1rw;
                aahVest = vMUp1rw;
                engVest = vMUp1rw;
                medVest = vMUp1rw;
                jtacVest = vMUp1rw;
                abmVest = vMUp1rw;
                mkVest = vMUp1rw;
                snipVest = vMUp1rw;
                ladVest = vMUp1rw;
                pilVest = vMUp1rw;
                // Backpacks
                bMWk = "B_mas_Kitbag_wint";       // fast pack
                bMWa = "B_mas_AssaultPack_wint";  // assault pack
                bMWb = "B_mas_Bergen_wint";       // bergen artic
                bMWbp = "B_mas_m_Bergen_us_w";    // patrol
                bMWbhp = "B_mas_m_Bergen_acr_w";  // bergen heavy patrol
                slPack = bTF;
                tlPack = bTF;
                mglPack = bMWk;
                mghPack = bMWb;
                gllPack = bMWa;
                glhPack = bMWb;
                atPack = bMWb;
                atsPack = bMWbp;
                aalPack = bMWb;
                aahPack = bMWb;
                engPack = bMWb;
                medPack = bMWk;
                jtacPack = bAuav;
                abmPack = bMWb;
                mkPack = bMWa;
                snipPack = bMWk;
                ladPack = bACEl;
                pilPack = bTF;
            } else {
                    if (masWeapon7) then { // Massi - US Marines
                    // Helmets
                    hMMhu = "H_mas_mar_helmetv_us";
                    hMMhos = "H_mas_mar_helmet_ops_sf";
                    hMMhosh = "H_mas_mar_helmet_ops_st_h";
                    hMUrb = "H_mas_usn_revcapheadset_b";
                    slHelmet = hNQsl;
                    tlHelmet = hAbb;
                    mglHelmet = hMMhu;
                    mghHelmet = hMMhosh;
                    gllHelmet = hMMhosh;
                    glhHelmet = hMMhosh;
                    atHelmet = hMMhos;
                    atsHelmet = hMMhos;
                    aalHelmet = hMMhosh;
                    aahHelmet = hMMhosh;
                    engHelmet = hMMhosh;
                    medHelmet = hNQm;
                    jtacHelmet = hMMhosh;
                    abmHelmet = hMMhu;
                    mkHelmet = hMUrb;
                    snipHelmet = hMUrb;
                    ladHelmet = hMMhu;
                    pilHelmet = hAhch;          //Needs Mas Equiv
                    // Uniforms
                    uMMwv = "U_mas_mar_B_CombatUniform_wood_vest3";
                    slUniform = uNQlu;
                    tlUniform = uNQlu;
                    mglUniform = uMMwv;
                    mghUniform = uMMwv;
                    gllUniform = uMMwv;
                    glhUniform = uMMwv;
                    atUniform = uMMwv;
                    atsUniform = uMMwv;
                    aalUniform = uMMwv;
                    aahUniform = uMMwv;
                    engUniform = uMMwv;
                    medUniform = uNQmu;
                    jtacUniform = uMMwv;
                    abmUniform = uMMwv;
                    mkUniform = uMMwv;
                    snipUniform = uMMwv;
                    ladUniform = uMMwv;
                    pilUniform = uActu;        //Needs Mas Equiv
                    // Vests
                    vMMpr = "V_mas_mar_PlateCarrier1_rgr_g";
                    slVest = vNQsl;
                    tlVest = vNQtl;
                    mglVest = vMMpr;
                    mghVest = vMMpr;
                    gllVest = vMMpr;
                    glhVest = vMMpr;
                    atVest = vMMpr;
                    atsVest = vMMpr;
                    aalVest = vMMpr;
                    aahVest = vMMpr;
                    engVest = vMMpr;
                    medVest = vNQmv;
                    jtacVest = vMMpr;
                    abmVest = vMMpr;
                    mkVest = vMMpr;
                    snipVest = vMMpr;
                    ladVest = vMMpr;
                    pilVest = vMMpr;
                    // Backpacks
                    slPack = bTF2;
                    tlPack = bTF2;
                    mglPack = bAc;
                    mghPack = bNQuc;
                    gllPack = bAa;
                    glhPack = bNQuc;
                    atPack = bNQatc;
                    atsPack = bNQatc;
                    aalPack = bNQatc;
                    aahPack = bNQatc;
                    engPack = bNQdc;
                    medPack = bNQmc;
                    jtacPack = bAuav;
                    abmPack = bAc;
                    mkPack = bAam;
                    snipPack = bAam;
                    ladPack = bACEl;
                    pilPack = bTF;
                } else {
                        if (masWeapon8) then {  // Massi - US Marines Winter
                        // Helmets
                        hMMW = "H_mas_mar_helmetw_us";      // no googles
                        hMMWg = "H_mas_mar_helmetwgog_us";  // googles
                        hMMwhw = "H_mas_mar_woolhat_ht_w";
                        slHelmet = hNQsl;
                        tlHelmet = hMMW;
                        mglHelmet = hMMW;
                        mghHelmet = hMMWg;
                        gllHelmet = hMMWg;
                        glhHelmet = hMMW;
                        atHelmet = hMMWg;
                        atsHelmet = hMMWg;
                        aalHelmet = hMMWg;
                        aahHelmet = hMMW;
                        engHelmet = hMMW;
                        medHelmet = hNQm;
                        jtacHelmet = hMMWg;
                        abmHelmet = hMMW;
                        mkHelmet = hMMwhw;
                        snipHelmet = hMMwhw;
                        ladHelmet = hMMW;
                        pilHelmet = hAhch;          //Needs Mas Equiv
                        // Uniforms
                        uMMW = "U_mas_mar_B_wint";    //hood up
                        uMMW2 = "U_mas_mar_B_wint2";  // hood down
                        slUniform = uMMW2;
                        tlUniform = uMMW2;
                        mglUniform = uMMW;
                        mghUniform = uMMW2;
                        gllUniform = uMMW;
                        glhUniform = uMMW2;
                        atUniform = uMMW2;
                        atsUniform = uMMW2;
                        aalUniform = uMMW;
                        aahUniform = uMMW2;
                        engUniform = uMMW2;
                        medUniform = uMMW2;
                        jtacUniform = uMMW;
                        abmUniform = uMMW;
                        mkUniform = uMMW2;
                        snipUniform = uMMW2;
                        ladUniform = uMMW2;
                        pilUniform = uActu;        //Needs Mas Equiv
                        // Vests
                        vMMWp1 = "V_mas_mar_PlateCarrier1_rgr_w";
                        vMMWp2 = "V_mas_mar_PlateCarrier2_rgr_w";
                        vMMWc = "V_mas_mar_ChestrigB_rgr_w";
                        slVest = vNQsl;
                        tlVest = vNQtl;
                        mglVest = vMMWp1;
                        mghVest = vMMWp2;
                        gllVest = vMMWp1;
                        glhVest = vMMWp2;
                        atVest = vMMWp1;
                        atsVest = vMMWp1;
                        aalVest = vMMWp1;
                        aahVest = vMMWp1;
                        engVest = vMMWp1;
                        medVest = vNQmv;
                        jtacVest = vMMWp1;
                        abmVest = vMMWp2;
                        mkVest = vMMWc;
                        snipVest = vMMWc;
                        ladVest = vMMWp1;
                        pilVest = vMMWp1;
                        // Backpacks
                        bMWk = "B_mas_Kitbag_wint";
                        bMWa = "B_mas_AssaultPack_wint";
                        bMWb = "B_mas_Bergen_wint";
                        bMWbp = "B_mas_m_Bergen_us_w";
                        bMWbhp = "B_mas_m_Bergen_acr_w";
                        slPack = bTF;
                        tlPack = bTF;
                        mglPack = bMWk;
                        mghPack = bMWb;
                        gllPack = bMWa;
                        glhPack = bMWb;
                        atPack = bMWb;
                        atsPack = bMWbp;
                        aalPack = bMWb;
                        aahPack = bMWb;
                        engPack = bMWb;
                        medPack = bMWk;
                        jtacPack = bAuav;
                        abmPack = bMWb;
                        mkPack = bMWa;
                        snipPack = bMWk;
                        ladPack = bACEl;
                        pilPack = bTF;
                    } else {
                        if (rhsWeapon) then {
                            if (playWest) then {  // RHS - US Marines Desert
                                // Helmets
                                hRhmh = "rhsusf_lwh_helmet_marpatd_headset";
                                hRhme = "rhsusf_lwh_helmet_marpatd_ess";
                                hRhm = "rhsusf_lwh_helmet_marpatd";
                                slHelmet = hRhmh;
                                tlHelmet = hRhmh;
                                mglHelmet = hRhme;
                                mghHelmet = hRhme;
                                gllHelmet = hRhme;
                                glhHelmet = hRhme;
                                atHelmet = hRhm;
                                atsHelmet = hRhm;
                                aalHelmet = hRhm;
                                aahHelmet = hRhm;
                                engHelmet = hRhme;
                                medHelmet = hRhmh;
                                jtacHelmet = hRhm;
                                abmHelmet = hRhm;
                                mkHelmet = hRhmh;
                                snipHelmet = hRhmh;
                                ladHelmet = hRhm;
                                pilHelmet = hAhch;          //Needs RHS Equiv
                                // Uniforms
                                uRd = "rhs_uniform_FROG01_d";
                                slUniform = uRd;
                                tlUniform = uRd;
                                mglUniform = uRd;
                                mghUniform = uRd;
                                gllUniform = uRd;
                                glhUniform = uRd;
                                atUniform = uRd;
                                atsUniform = uRd;
                                aalUniform = uRd;
                                aahUniform = uRd;
                                engUniform = uRd;
                                medUniform = uRd;
                                jtacUniform = uRd;
                                abmUniform = uRd;
                                mkUniform = uRd;
                                snipUniform = uRd;
                                ladUniform = uRd;
                                pilUniform = uActu;        //Needs RHS Equiv
                                // Vests
                                vRssl = "rhsusf_spc_squadleader";
                                vRstl = "rhsusf_spc_teamleader";
                                vRsmg = "rhsusf_spc_mg";
                                vRsl = "rhsusf_spc_light";
                                vRiom = "rhsusf_iotv_ocp_Medic";
                                slVest = vRssl;
                                tlVest = vRstl;
                                mglVest = vRsmg;
                                mghVest = vRsmg;
                                gllVest = vRstl;
                                glhVest = vRstl;
                                atVest = vRsl;
                                atsVest = vRsl;
                                aalVest = vRsl;
                                aahVest = vRsl;
                                engVest = vRsmg;
                                medVest = vRiom;
                                jtacVest = vRsmg;
                                abmVest = vRsl;
                                mkVest = vRsmg;
                                snipVest = vRsmg;
                                ladVest = vRsl;
                                pilVest = vRsl;
                                // Backpacks
                                bRaec = "rhsusf_assault_eagleaiii_coy";
                                slPack = bTF3;
                                tlPack = bTF3;
                                mglPack = bRaec;
                                mghPack = bRaec;
                                gllPack = bRaec;
                                glhPack = bAak;
                                atPack = bAak;
                                atsPack = bAcc;
                                aalPack = bRaec;
                                aahPack = bAcc;
                                engPack = bAcc;
                                medPack = bAak;
                                jtacPack = bAuav;
                                abmPack = bAcc;
                                mkPack = bRaec;
                                snipPack = bRaec;
                                ladPack = bACEl;
                                pilPack = bTF;
                            } else {
                                if (playEast) then {  // RHS - Russian Airborne Troops [VDV] - Desert
                                    // Helmets
                                    hR6g = "rhs_6b27m_green";
                                    hR6ge = "rhs_6b27m_green_ess";
                                    slHelmet = hR6ge;
                                    tlHelmet = hR6ge;
                                    mglHelmet = hR6g;
                                    mghHelmet = hR6g;
                                    gllHelmet = hR6g;
                                    glhHelmet = hR6g;
                                    atHelmet = hR6g;
                                    atsHelmet = hR6g;
                                    aalHelmet = hR6g;
                                    aahHelmet = hR6g;
                                    engHelmet = hR6g;
                                    medHelmet = hR6ge;
                                    jtacHelmet = hR6g;
                                    abmHelmet = hR6g;
                                    mkHelmet = hR6g;
                                    snipHelmet = hR6g;
                                    ladHelmet = hR6g;
                                    pilHelmet = hAhch;          //Needs RHS Equiv
                                    // Uniforms
                                    uRvdvMf = "rhs_uniform_vdv_mflora";
                                    slUniform = uRvdvMf;
                                    tlUniform = uRvdvMf;
                                    mglUniform = uRvdvMf;
                                    mghUniform = uRvdvMf;
                                    gllUniform = uRvdvMf;
                                    glhUniform = uRvdvMf;
                                    atUniform = uRvdvMf;
                                    atsUniform = uRvdvMf;
                                    aalUniform = uRvdvMf;
                                    aahUniform = uRvdvMf;
                                    engUniform = uRvdvMf;
                                    medUniform = uRvdvMf;
                                    jtacUniform = uRvdvMf;
                                    abmUniform = uRvdvMf;
                                    mkUniform = uRvdvMf;
                                    snipUniform = uRvdvMf;
                                    ladUniform = uRvdvMf;
                                    pilUniform = uActu;        //Needs RHS Equiv
                                    // Vests
                                    vR6dvh = "rhs_6sh92_digi_vog_headset";
                                    vR6mr = "rhs_6b23_ML_rifleman";
                                    vR6mm = "rhs_6b23_ML_medic";
                                    vR6d = "rhs_6sh92_digi";
                                    vR6dv = "rhs_6sh92_digi_vog";
                                    vR6 = "rhs_6sh92";
                                    slVest = vR6dvh;
                                    tlVest = vR6dvh;
                                    mglVest = vR6d;
                                    mghVest = vR6mr;
                                    gllVest = vR6dv;
                                    glhVest = vR6dvh;
                                    atVest = vR6;
                                    atsVest = vR6;
                                    aalVest = vR6;
                                    aahVest = vR6;
                                    engVest = vR6mr;
                                    medVest = vR6mm;
                                    jtacVest = vR6mr;
                                    abmVest = vR6;
                                    mkVest = vR6;
                                    snipVest = vR6;
                                    ladVest = vR6;
                                    pilVest = vR6;
                                    // Backpacks
                                    bRaum = "rhs_assault_umbts_medic"; // varify backpacks
                                    bRsMG = "rhs_sidorMG";
                                    slPack = bTF4;
                                    tlPack = bTF4;
                                    mglPack = bAam;
                                    mghPack = bRsMG;
                                    gllPack = bAam;
                                    glhPack = bAam;
                                    atPack = bAam;
                                    atsPack = bAck;
                                    aalPack = bRsMG;
                                    aahPack = bAck;
                                    engPack = bAck;
                                    medPack = bAam;
                                    jtacPack = bAuav; // varify backpacks
                                    abmPack = bAck;
                                    mkPack = bAam;
                                    snipPack = bAam;
                                    ladPack = bACEl;
                                    pilPack = bTF;
                                };
                            };
                        } else {
                           [] call fnc_defaultGear;
                        };
                    };
                };
            };
        };
    };
};

Squad_leader = {
    _unit = _this select 0;
    [_unit] call fn_remove;
    [_unit] call fn_addItems;
    _unit addHeadgear slHelmet;
    _unit addUniform slUniform;
    _unit addVest slVest;
    _unit addBackpack slPack;
    _vest = vestContainer _unit;
    _uniform = uniformContainer _unit;
    _backpack = unitBackPack _unit;
    [_unit] call fn_rifleGL;
    [_unit] call fn_45Handgun;
    [_unit] call fn_vestItems;
    [_unit] call fn_uniformItems;
    if (aceOn) then {
        if (side player == west) then {
            _uniform addItemCargoGlobal ["ACE_key_west", 1];
        } else {
            if (side player == east) then {
                _uniform addItemCargoGlobal ["ACE_key_east", 1];
            };
        };
    };
    reload _unit;
    [_unit] call fn_backpackItems;
    _backpack addMagazineCargoGlobal ["Laserbatteries", 1];
    _backpack addWeaponCargoGlobal ["Laserdesignator", 1];
};

Team_leader = {
    _unit = _this select 0;
    [_unit] call fn_remove;
    [_unit] call fn_addItems;
    _unit addHeadgear tlHelmet;
    _unit addUniform tlUniform;
    _unit addVest tlVest;
    _unit addBackpack tlPack;
    _vest = vestContainer _unit;
    _uniform = uniformContainer _unit;
    _backpack = unitBackPack _unit;
    [_unit] call fn_rifleGL;
    [_unit] call fn_45Handgun;
    [_unit] call fn_vestItems;
    [_unit] call fn_uniformItems;
    reload _unit;
    [_unit] call fn_backpackItems;
    _backpack addMagazineCargoGlobal ["Laserbatteries", 1];
    _backpack addWeaponCargoGlobal ["Laserdesignator", 1];
}; 

MG_Light = {
    _unit = _this select 0;
    [_unit] call fn_remove;
    [_unit] call fn_addItems;
    _unit addHeadgear mglHelmet;
    _unit addUniform mglUniform;
    _unit addVest mglVest;
    _unit addBackpack mglPack;
    _vest = vestContainer _unit;
    _uniform = uniformContainer _unit;
    _backpack = unitBackPack _unit;
    [_unit] call fn_rifleMGlt;
    [_unit] call fn_45Handgun;
    [_unit] call fn_vestItems;
    [_unit] call fn_uniformItems;
    reload _unit;
    [_unit] call fn_backpackItems;
};

MG_Heavy = {
    _unit = _this select 0;
    [_unit] call fn_remove;
    [_unit] call fn_addItems;
    _unit addHeadgear mghHelmet;
    _unit addUniform mghUniform;
    _unit addVest mghVest;
    _unit addBackpack mghPack;
    _vest = vestContainer _unit;
    _uniform = uniformContainer _unit;
    _backpack = unitBackPack _unit;
    [_unit] call fn_rifleMGhvy;
    [_unit] call fn_45Handgun;
    [_unit] call fn_vestItems;
    [_unit] call fn_uniformItems;
    reload _unit;
    [_unit] call fn_backpackItems;
    if (rhsWeapon) then { if (irPoint in (items player + assignedItems player)) then { _unit unassignItem irPoint; _unit removeItem irPoint; }; };
};

Grenadier_light = {
    _unit = _this select 0;
    [_unit] call fn_remove;
    [_unit] call fn_addItems;
    _unit addHeadgear gllHelmet;
    _unit addUniform gllUniform;
    _unit addVest gllVest;
    _unit addBackpack gllPack;
    _vest = vestContainer _unit;
    _uniform = uniformContainer _unit;
    _backpack = unitBackPack _unit;
    [_unit] call fn_rifleGL;
    [_unit] call fn_GLlt;
    [_unit] call fn_45Handgun;
    [_unit] call fn_vestItems;
    [_unit] call fn_uniformItems;
    reload _unit;
    [_unit] call fn_backpackItems;
 };

Grenadier_Heavy = {
    _unit = _this select 0;
    [_unit] call fn_remove;
    [_unit] call fn_addItems;
    _unit addHeadgear glhHelmet;
    _unit addUniform glhUniform;
    _unit addVest glhVest;
    _unit addBackpack glhPack;
    if (rhsWeapon) then { if (side player == west) then { _unit setVariable ["rhsGLhvy", "true", true]; }; };
    _vest = vestContainer _unit;
    _uniform = uniformContainer _unit;
    _backpack = unitBackPack _unit;
    [_unit] call fn_rifleGL;
    [_unit] call fn_GLlt;
    [_unit] call fn_GLhvy;
    [_unit] call fn_45Handgun;
    [_unit] call fn_vestItems;
    [_unit] call fn_uniformItems;
    reload _unit;
    [_unit] call fn_backpackItems;
};
 
AT = {
    _unit = _this select 0;
    [_unit] call fn_remove;
    [_unit] call fn_addItems;
    _unit addHeadgear atHelmet;
    _unit addUniform atUniform;
    if (closeQuarters) then {
        if (defaultWeapon) then {
            _unit addvest vApcs;
        } else {
            if (tropicWeapon) then {
                _unit addvest vAtv3;
            };
        };
    } else {
        _unit addVest atVest;
    };
    _unit addBackpack atPack;
    _vest = vestContainer _unit;
    _uniform = uniformContainer _unit;
    _backpack = unitBackPack _unit;
    [_unit] call fn_rifle;
    [_unit] call fn_AT;
    [_unit] call fn_45Handgun;
    [_unit] call fn_vestItems;
    [_unit] call fn_uniformItems;
    reload _unit;
    [_unit] call fn_backpackItems;
 };

AT_specialist = {
    _unit = _this select 0;
    [_unit] call fn_remove;
    [_unit] call fn_addItems;
    _unit addHeadgear atsHelmet;
    _unit addUniform atsUniform;
    if (closeQuarters) then {
        if (defaultWeapon) then {
            _unit addvest vApcs;
        } else {
            if (tropicWeapon) then {
                _unit addvest vAtv3;
            };
        };
    } else {
        _unit addVest atsVest;
    };
    _unit addBackpack atsPack;
    _vest = vestContainer _unit;
    _uniform = uniformContainer _unit;
    _backpack = unitBackPack _unit;
    [_unit] call fn_rifle;
    [_unit] call fn_ATS;
    [_unit] call fn_45Handgun;
    [_unit] call fn_vestItems;
    [_unit] call fn_uniformItems;
    reload _unit;
    [_unit] call fn_backpackItems;
    if (rhsWeapon) then { _unit removeMagazines "rhs_mag_30Rnd_556x45_M855A1_Stanag"; _unit removeMagazines grenade; };
 };
 
AA_light = {
    _unit = _this select 0;
    [_unit] call fn_remove;
    [_unit] call fn_addItems;
    _unit addHeadgear aalHelmet;
    _unit addUniform aalUniform;
    if (closeQuarters) then {
        if (defaultWeapon) then {
            _unit addvest vApcs;
        } else {
            if (tropicWeapon) then {
                _unit addvest vAtv3;
            };
        };
    } else {
        _unit addVest aalVest;
    };
    _unit addBackpack aalPack;
    _vest = vestContainer _unit;
    _uniform = uniformContainer _unit;
    _backpack = unitBackPack _unit;
    [_unit] call fn_rifle;
    [_unit] call fn_AAlt;
    [_unit] call fn_45Handgun;
    [_unit] call fn_vestItems;
    [_unit] call fn_uniformItems;
    reload _unit;
    [_unit] call fn_backpackItems;
};

AA_heavy = {
    _unit = _this select 0;
    [_unit] call fn_remove;
    [_unit] call fn_addItems;
    _unit addHeadgear aahHelmet;
    _unit addUniform aahUniform;
    if (closeQuarters) then {
        if (defaultWeapon) then {
            _unit addvest vApcs;
        } else {
            if (tropicWeapon) then {
                _unit addvest vAtv3;
            };
        };
    } else {
        _unit addVest aahVest;
    };
    _unit addBackpack aahPack;
    _vest = vestContainer _unit;
    _uniform = uniformContainer _unit;
    _backpack = unitBackPack _unit;
    [_unit] call fn_rifle;
    [_unit] call fn_AAhvy;
    [_unit] call fn_45Handgun;
    [_unit] call fn_vestItems;
    [_unit] call fn_uniformItems;
    reload _unit;
    [_unit] call fn_backpackItems;
};

Engineer = {
    _unit = _this select 0;
    [_unit] call fn_remove;
    [_unit] call fn_addItems;
    _unit addHeadgear engHelmet;
    _unit addUniform engUniform;
    if (closeQuarters) then {
        if (defaultWeapon) then {
            _unit addvest vApcs;
        } else {
            if (tropicWeapon) then {
                _unit addvest vAtv3;
            };
        };
    } else {
        _unit addVest engVest;
    };
    _unit addBackpack engPack;
    _vest = vestContainer _unit;
    _uniform = uniformContainer _unit;
    _backpack = unitBackPack _unit;
    [_unit] call fn_rifle;
    [_unit] call fn_repairDemo;
    [_unit] call fn_45Handgun;
    [_unit] call fn_vestItems;
    [_unit] call fn_uniformItems;
    reload _unit;
    [_unit] call fn_backpackItems;
};

Medic = {
    _unit = _this select 0;
    [_unit] call fn_remove;
    [_unit] call fn_addItems;
    _unit addHeadgear medHelmet;
    _unit addUniform medUniform;
    if (closeQuarters) then {
        if (defaultWeapon) then {
            _unit addvest vApcs;
        } else {
            if (tropicWeapon) then {
                _unit addvest vAtv3;
            };
        };
    } else {
        _unit addVest medVest;
    };
    _unit addBackpack medPack;
    _vest = vestContainer _unit;
    _uniform = uniformContainer _unit;
    _backpack = unitBackPack _unit;
    [_unit] call fn_rifle;
    [_unit] call fn_45Handgun;
    [_unit] call fn_vestItems;
    [_unit] call fn_uniformItems;
    reload _unit;
    [_unit] call fn_backpackItems;
    [_unit] call fn_med;
};
 
Jtac = {
    _unit = _this select 0;
    [_unit] call fn_remove;
    [_unit] call fn_addItems;
    if (side player == west) then { 
        _unit addWeapon "B_UavTerminal";
    } else {
        if (side player == east) then {
            _unit addWeapon "O_UavTerminal";
        } else {
            if (side player == resistance) then {
                _unit addWeapon "I_UavTerminal";
            };
        };
    };
    _unit addHeadgear jtacHelmet;
    _unit addUniform jtacUniform;
    if (closeQuarters) then {
        if (defaultWeapon) then {
            _unit addvest vApcs;
        } else {
            if (tropicWeapon) then {
                _unit addvest vAtv3;
            };
        };
    } else {
        _unit addVest jtacVest;
    };
    _unit addBackpack jtacPack;
    _vest = vestContainer _unit;
    _uniform = uniformContainer _unit;
    _backpack = unitBackPack _unit;
    [_unit] call fn_rifle;
    [_unit] call fn_45Handgun;
    [_unit] call fn_vestItems;
    [_unit] call fn_uniformItems;
    if (aceOn) then { _vest addItemCargoGlobal ["ACE_UAVBattery", 1] };
    reload _unit;
    if (side player == west) then {
        if (defaultWeapon) then {
            _unit removeMagazines "30Rnd_65x39_caseless_mag";
            _vest addMagazineCargoGlobal [mx65_30trace, 2];
        } else {
            if (masWeapon) then {
                _unit removeMagazines "30Rnd_mas_556x45_Stanag";
                _vest addMagazineCargoGlobal ["30Rnd_mas_556x45_Stanag", 2];
            } else {
                if (rhsWeapon) then {
                    _unit removeMagazines "rhs_mag_30Rnd_556x45_M855A1_Stanag";
                };
            };
        };
    };
 };

AmmoBearerMag = {
    _unit = _this select 0;
    [_unit] call fn_remove;
    [_unit] call fn_addItems;
    _unit addHeadgear abmHelmet;
    _unit addUniform abmUniform;
    if (closeQuarters) then {
        if (defaultWeapon) then {
            _unit addvest vApcs;
        } else {
            if (tropicWeapon) then {
                _unit addvest vAtv3;
            };
        };
    } else {
        _unit addVest abmVest;
    };
    _unit addBackpack abmPack;
    _vest = vestContainer _unit;
    _uniform = uniformContainer _unit;
    _backpack = unitBackPack _unit;
    [_unit] call fn_rifle;
    [_unit] call fn_ammoMag;
    [_unit] call fn_45Handgun;
    [_unit] call fn_vestItems;
    [_unit] call fn_uniformItems;
    reload _unit;
    [_unit] call fn_backpackItems;
};

Marksman = {
    _unit = _this select 0;
    [_unit] call fn_remove;
    [_unit] call fn_addItems;
    _unit addHeadgear mkHelmet;
    _unit addUniform mkUniform;
    _unit addVest mkVest;
    _unit addBackpack mkPack;
    _vest = vestContainer _unit;
    _uniform = uniformContainer _unit;
    _backpack = unitBackPack _unit;
    [_unit] call fn_rifleMark;
    [_unit] call fn_45Handgun;
    [_unit] call fn_vestItems;
    [_unit] call fn_uniformItems;
    reload _unit;
    [_unit] call fn_backpackItems;
};

Sniper = {
    _unit = _this select 0;
    [_unit] call fn_remove;
    [_unit] call fn_addItems;
    _unit addHeadgear snipHelmet;
    _unit addUniform snipUniform;
    _unit addVest snipVest;
    _unit addBackpack snipPack;
    _vest = vestContainer _unit;
    _uniform = uniformContainer _unit;
    _backpack = unitBackPack _unit;
    [_unit] call fn_rifleSnip;
    [_unit] call fn_45Handgun;
    [_unit] call fn_vestItems;
    [_unit] call fn_uniformItems;
    reload _unit;
    [_unit] call fn_backpackItems;
};

LadderCarrier = {
    _unit = _this select 0;
    [_unit] call fn_remove;
    [_unit] call fn_addItems;
    _unit addHeadgear ladHelmet;
    _unit addUniform ladUniform;
    if (closeQuarters) then {
        if (defaultWeapon) then {
            _unit addvest vApcs;
        } else {
            if (tropicWeapon) then {
                _unit addvest vAtv3;
            };
        };
    } else {
        _unit addVest ladVest;
    };
    _unit addBackpack ladPack;
    _vest = vestContainer _unit;
    _uniform = uniformContainer _unit;
    _backpack = unitBackPack _unit;
    [_unit] call fn_rifle;
    [_unit] call fn_45Handgun;
    [_unit] call fn_vestItems;
    [_unit] call fn_uniformItems;
    reload _unit;
};

LeadPilot = {
    _unit = _this select 0;
    [_unit] call fn_remove;
    [_unit] call fn_addItems;
    _unit addHeadgear pilHelmet;
    _unit addUniform pilUniform;
    _unit addVest pilVest;
    _unit addBackpack pilPack;
    _vest = vestContainer _unit;
    _uniform = uniformContainer _unit;
    _backpack = unitBackPack _unit;
    [_unit] call fn_Pilot;
    [_unit] call fn_45Handgun;
    [_unit] call fn_vestItems;
    [_unit] call fn_uniformItems;
    _backpack addItemCargoGlobal ["ToolKit", 1];
    _backpack addItemCargoGlobal ["FirstAidKit", 2];
    reload _unit;
};

CoPilot = {
    _unit = _this select 0;
    [_unit] call fn_remove;
    [_unit] call fn_addItems;
    _unit addHeadgear pilHelmet;
    _unit addUniform pilUniform;
    _unit addVest pilVest;
    _unit addBackpack pilPack;
    _vest = vestContainer _unit;
    _uniform = uniformContainer _unit;
    _backpack = unitBackPack _unit;
    [_unit] call fn_Pilot;
    [_unit] call fn_45Handgun;
    [_unit] call fn_vestItems;
    [_unit] call fn_uniformItems;
    _backpack addItemCargoGlobal ["MediKit", 1];
    _backpack addItemCargoGlobal ["FirstAidKit", 2];
    reload _unit;
};

fn_diver = {
    _unit = _this select 0;
    _diver = "";
    [_unit] call fn_remove;
    [_unit] call fn_addItems;
    if (playWest) then {
        _unit addVest "V_RebreatherB";
        _unit addUniform "U_B_Wetsuit";
        _unit addGoggles "G_B_Diving";
    } else {
        if (playEast) then {
            _unit addVest "V_RebreatherIR";
            _unit addUniform "U_O_Wetsuit";
            _unit addGoggles "G_O_Diving";
        };
    };
    _uniform = uniformContainer _unit;
    if (count _this < 2) then {
        _unit addBackpack bAab;
        _backpack = unitBackPack _unit;
        _unit addMagazine sdar20;
        _unit addWeapon "arifle_SDAR_F";
        _uniform addMagazineCargoGlobal [grenade, 2];
        _backpack addMagazineCargoGlobal [grenade, 2];
        _backpack addMagazineCargoGlobal [smoke, 2];
        _backpack addMagazineCargoGlobal [sdar20, 4];
        if (playWest) then {
            _backpack addMagazineCargoGlobal [m556_30, 10];
        } else {
            if (playEast) then {
                _backpack addMagazineCargoGlobal ["30Rnd_556x45_Stanag_green", 10];
            };
        };
    } else {
        if (count _this > 1) then {
            _diver = _this select 1;
            _unit addBackpack "B_TacticalPack_blk";
            if (_diver == "SL") then {
                [_unit] call fn_rifleGL;
                if (aceOn) then {
                    if (side player == west) then {
                        _uniform addItemCargoGlobal ["ACE_key_west", 1];
                    } else {
                        if (side player == east) then {
                            _uniform addItemCargoGlobal ["ACE_key_east", 1];
                        };
                    };
                };
            } else {
                if (_diver == "TL") then {
                    [_unit] call fn_rifleGL;
                } else {
                    if (_diver == "MGL") then {
                        [_unit] call fn_rifleMGlt;
                    } else {
                        if (_diver == "MGH") then {
                            [_unit] call fn_rifleMGhvy;
                        } else {
                            if (_diver == "GLL") then {
                                [_unit] call fn_rifleGL;
                                [_unit] call fn_GLlt;
                            } else {
                                if (_diver == "GLH") then {
                                    [_unit] call fn_rifleGL;
                                    [_unit] call fn_GLlt;
                                    [_unit] call fn_GLhvy;
                                } else {
                                    if (_diver == "AT") then {
                                        pr_test = true;
                                        [_unit] call fn_rifle;
                                        [_unit] call fn_AT;
                                    } else {
                                        if (_diver == "ATS") then {
                                            [_unit] call fn_rifle;
                                            [_unit] call fn_ATS;
                                        } else {
                                            if (_diver == "AAL") then {
                                                [_unit] call fn_rifle;
                                                [_unit] call fn_AAlt;
                                            } else {
                                                if (_diver == "AAH") then {
                                                    [_unit] call fn_rifle;
                                                    [_unit] call fn_AAhvy;
                                                } else {
                                                    if (_diver == "ENG") then {
                                                        [_unit] call fn_rifle;
                                                        [_unit] call fn_repairDemo;
                                                    } else {
                                                        if (_diver == "MED") then {
                                                            [_unit] call fn_rifle;
                                                        } else {
                                                            if (_diver == "JTAC") then {
                                                                [_unit] call fn_rifle;
                                                            } else {
                                                                if (_diver == "ABM") then {
                                                                    [_unit] call fn_rifle;
                                                                    [_unit] call fn_ammoMag;
                                                                } else {
                                                                    if (_diver == "MRK") then {
                                                                        [_unit] call fn_rifleMark;
                                                                    } else {
                                                                        if (_diver == "SNIP") then {
                                                                            [_unit] call fn_rifleSnip;
                                                                        } else {
                                                                            if (_diver == "LAD") then {
                                                                                [_unit] call fn_rifle;
                                                                            };
                                                                        };
                                                                    };
                                                                };
                                                            };
                                                        };
                                                    };
                                                };
                                            };
                                        };
                                    };
                                };
                            };
                        };
                    };
                };
            };
        };
    };
    [_unit] call fn_45Handgun;
    [_unit] call fn_uniformItems;
    reload _unit;
    [_unit] call fn_backpackItems;
    if (_diver == "MED") then { [_unit] call fn_med; };
    [_unit] call fn_vest2Pack;
};
