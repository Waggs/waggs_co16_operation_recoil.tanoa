//Parameters 
private ["_code", "_inputCode"]; 
_code      = [_this, 0, [], [[]]] call BIS_fnc_param; 
_inputCode = [_this, 1, [], [[]]] call BIS_fnc_param; 

//compare codes 
private "_compare"; 
_compare = [_code, _inputCode] call BIS_fnc_areEqual; 

if (_compare) then { 
    ["<t size='1' color='#57e241'>" + "BOMB DEFUSED" + "</t>" ,0,0.8,10,1] spawn BIS_fnc_dynamicText; 
    DEFUSED = true; publicVariable "DEFUSED"; 
    playSound "button_close"; 
} else { 
    //["<t size='1' color='#ff5b5b'>" + "BOMB ARMED" + "</t>" ,0,0.8,10,1] spawn BIS_fnc_dynamicText; 
    ARMED = true; publicVariable "ARMED"; 
    playSound "button_wrong"; 
}; 

CODEINPUT = []; 

//Return Value 
_code 
