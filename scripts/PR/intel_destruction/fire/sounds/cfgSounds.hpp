
class flames { 
    name = "flames"; 
    sound[] = {"scripts\PR\scripts\misc\intel_destruction\fire\sounds\flames.ogg", "db -1", 1}; 
    titles[] = {1, ""}; 
}; 

class burned { 
    name = "burned"; 
    sound[] = {"scripts\PR\scripts\misc\intel_destruction\fire\sounds\burned.ogg", "db -5", 1.0}; 
    titles[] = {1, ""}; 
}; 

class tipat { 
    name = "tipat"; 
    sound[] = {"scripts\PR\scripts\misc\intel_destruction\fire\sounds\tipat.ogg", 0.8, 1}; 
    titles[] = {1, ""}; 
}; 
