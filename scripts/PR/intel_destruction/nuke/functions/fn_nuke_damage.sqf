//[[_bomb,_yield,_radius,_cone,_smoke],"pr_fnc_nuke_damage",false,true] call bis_fnc_MP; 

if !(isServer) exitWith {}; 

_bomb = _this select 0; 

_bArr = _bomb getVariable "nukeArray"; 
//_yield     = _bArr select 0; 
_radius    = _bArr select 1; 
_doDamage  = _bArr select 2; 
_fire      = _bArr select 3; 
_fireScale = _bArr select 4; 
_blowSpeed = _bArr select 7; 
_blacklist = _bArr select 10; 

_pos = getPosWorld _bomb; 
_posx = _pos select 0; 
_posy = _pos select 1; 
_posz = _pos select 2; 

_destroy = (_radius/1.9) max 450; 

_vehicles    = _this select 1; 
_airs        = _this select 2; 
_land        = _this select 3; 
_house       = _this select 4; 
_lights      = _this select 5; 
_objectsSkip = _this select 6; 
_objects     = _this select 7; 

//_fire = false; //true - every killed object will burn (useful for movies, etc. , but could cause lag)
//BIS_Effects_Burn = compile preprocessFile "\ca\Data\ParticleEffects\SCRIPTS\destruction\burn.sqf"; 
//diag_log format ["_pos %1 _yield %2 _radius %3",_pos,_yield,_radius]; 

if !(_doDamage) exitWith {}; 

{ 
    _distance = [_posx, _posy, 0] distance _x; 
    _dir = asin (((getPosWorld _x select 1) - _posy) / _distance); 
    if (getPosWorld _x select 0 < _posx) then {
        _dir = 180 - _dir; 
    }; 
    _vel = velocity _x; 
    _damage = 1 - _distance / _radius; 
    _damage = _damage * _damage; 
    _speed = 4 * (_damage + random (_damage / 4)); 
    _speed = _speed * _blowSpeed; 
    _x setVelocity [(_vel select 0) + (_speed * cos _dir), (_vel select 1) + (_speed * sin _dir), _speed / 3]; 
    _x setDamage ((damage _x) + _damage); 
} forEach _airs; 

{ 
    _damage = (1.8 + random (1) - 2 * (_pos distance _x) / _radius) max 0; 

    switch true do { 
        case (_x isKindOf "Man"): {_damage = _damage * 2;}; 
        case ((_x isKindOf "WheeledAPC") || (_x isKindOf "TrackedAPC")): {_damage = _damage * 0.7;}; 
        case (_x isKindOf "Car"): { _damage = _damage * 1.2; }; 
        case (_x isKindOf "Tank"): { _damage = _damage * 0.4; }; 
        case (_x isKindOf "Ship"): { _damage = _damage * 0.9; }; 
        //case (_x isKindOf "Air"): { _damage = _damage * 1.3; }; 
        default {}; 
    }; 

    if ( _x isKindOf "Man" || _x isKindOf "Car" || _x isKindOf "Tank" || _x isKindOf "Ship" ) then { 
        _distance = [_posx, _posy, 0] distance _x; 
        _dir = asin (((getPosWorld _x select 1) - _posy) / _distance); 
        if (getPosWorld _x select 0 < _posx) then {
            _dir = 180 - _dir; 
        }; 
        _vel = velocity _x; 
        _speed = _damage + random (_damage / 4); 

        if (!([_x, _bomb] call pr_fnc_houseCover) && !([_x, _bomb] call pr_fnc_landCover)) then { 
            if (_x isKindOf "Man") then {
                _speed = _speed * 2; 
            } else { 
                [_x, _speed * 6] spawn pr_fnc_nuke_rotate; 
            };
 
            _speed = _speed * _blowSpeed; 
            _x setVelocity [(_vel select 0) + (_speed * cos _dir), (_vel select 1) + (_speed * sin _dir), _speed / 3]; 
        };
    }; 

    if ((_damage > 0) && (alive _x)) then { 
        _new_damage = ((damage _x) + _damage); 
        _x setDamage _new_damage; 
    }; 
} forEach _land; 

//sleep 2; 

{ 
    if (alive _x) then { 
        //[[_x], "pr_fnc_nuke_electroPulse", true, false] call bis_fnc_MP; 
        [_x, 0] spawn pr_fnc_nuke_electroPulseInit; 
    }; 
} forEach _vehicles; 
sleep 2; 

{ 
    /*
    _damage = (1.8 + random (1) - 2 * (_pos distance _x) / _radius) max 0; 
    if ((_damage > 0) && (alive _x)) then { 
        _new_damage = ((damage _x) + _damage); 
        _x setDamage _new_damage; 
		if ((_new_damage >= 1) && ((_x isKindOf "Building") || (_fire)) && (random (4) > 3)) then { 
			//[_x,1+random(9),time,false,true] spawn BIS_Effects_Burn; 
			//[_x, true, 300, 0.25, 0.01, true, false] spawn pr_fnc_fire; 
			[_x, 0] spawn pr_fnc_fireRandomizer; 
		}; 
    }; 
	*/
    //if (((_x isKindOf "Building") || (_fire)) && (random (4) > 3.5)) then { 
    if (_fire) then { 
        //if ((_x isKindOf "House") && (random (4) > 3.5)) then { 
        if ((_x isKindOf "House") && (random (4) > (4 - _fireScale))) then { 
            [_x] spawn { 
                _obj = _this select 0; 
                sleep 60 + (random 60) max 10; 
                [_obj, 0] spawn pr_fnc_fireRandomizer; 
            }; 
        }; 
    }; 
} forEach _house; 

sleep 2; 
{ 
    if (alive _x) then { 
        [_x] spawn { 
            _light = _this select 0; 
            sleep random 2; 
            _light setDamage 0.95; 
            sleep 300 + random 300; 
            _light setDamage 0; 
        }; 
    }; 
} forEach _lights; 

sleep 20; 

{ 
    _damage = (2 + random (1) - 2 * (_pos distance _x) / _radius) max 0; 
    //_damage = 1; 

    //if ((random (2) > 1) && (alive _x)) then { 
    if ((_damage > 0) && (alive _x)) then { 
        //_new_damage = ((damage _x) + _damage); 
        _x setDamage _damage; 
		//if ((_new_damage > 1) && ((_x isKindOf "Building") || (_fire)) && (random (2) > 1)) then {
			//[_x,1+random(9),time,false,true] spawn BIS_Effects_Burn;
		//}; 
    }; 
} forEach _objects; 

//sleep 60 + random 60; 
[[_bomb, time], "pr_fnc_nuke_geiger", true, false] call bis_fnc_MP; 
[_bomb, time] spawn pr_fnc_nuke_radiation; 

