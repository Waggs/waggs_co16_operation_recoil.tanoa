// [_unit, _target] call pr_fnc_houseCover; [player, main_nuclear] call pr_fnc_houseCover

pr_fnc_houseCover = { 
    private ["_unit","_target","_house"]; 
    _unit = _this select 0; 
    _target = _this select 1; 
    _house = objNull;
    lineIntersectsSurfaces [
        [getPosWorld _unit select 0, getPosWorld _unit select 1, (getPosWorld _unit select 2) + 3],
        [getPosWorld _target select 0, getPosWorld _target select 1,(getPosWorld _target select 2) + 12],
        _unit, 
        _target, true, 1, "GEOM", "NONE"
    ] select 0 params ["","","","_house"]; 
    if ((_house isKindOf "House") && (_unit distance _house < 25)) exitWith { true }; 
    false
}; 

//onEachFrame {hintSilent str ([_unit, _target] call pr_fnc_houseCover)};
pr_fnc_landCover = { 
    private ["_unit","_target"]; 
    _unit = _this select 0; 
    _target = _this select 1; 

    terrainIntersect [
        [getPosATL _unit select 0, getPosATL _unit select 1, (getPosATL _unit select 2) + 2], 
        [getPosATL _target select 0, getPosATL _target select 1, (getPosATL _target select 2) + 5]
    ]; 
}; 

pr_fnc_inHouse = { 
    private ["_unit","_house"]; 
    _unit = _this; 
    _house = objNull; 
    lineIntersectsSurfaces [
        getPosWorld _unit, 
        getPosWorld _unit vectorAdd [0, 0, 50], 
        _unit, 
        _house, true, 1, "GEOM", "NONE"
    ] select 0 params ["","","","_house"];
    if (_house isKindOf "House") exitWith { true };
    false
};

//["buildingCheck","onEachFrame",{ bInStructure = player call pr_fnc_inHouse; }] call BIS_fnc_addStackedEventHandler;


//onEachFrame { hintSilent str (player call pr_fnc_inHouse) };
// use BIS func (recommended)