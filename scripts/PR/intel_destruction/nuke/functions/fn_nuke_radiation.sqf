private ["_posx","_posy","_blast_time","_all_radius","_radius","_current_radius","_v","_distance","_radiation","_damage","_dmg"]; 

_bomb = _this select 0; 
_bArr = _bomb getVariable "nukeArray"; 
_radius    = _bArr select 1; 
_irradTime = _bArr select 5; 
_reqGPS    = _bArr select 6; 
_radDamage = _bArr select 8; 
_carArmor  = _bArr select 9; 
_radFree   = _bArr select 11; 

_pos = getpos _bomb; 
_posx = _pos select 0; 
_posy = _pos select 1; 
_posz = _pos select 2; 

_blast_time = _this select 1; 

_all_radius  = _radius * 1.3; 
_radius2 = _all_radius; 
_v = _radius2 / (2 * _irradTime); 

while { _radius2 > 1 } do { 
    _current_radius = _radius2 - (time - _blast_time) * _v; 
    _safeZone = objNull; 
    _disToSafe = 0; 

    { 
        if ((_x isKindOf "Man") || count (crew _x) > 0) then { 
            _distance = [_posx, _posy] distance _x; 
            if ( _distance < _current_radius ) then { 
                _radiation = (1 - _distance / _current_radius) * _current_radius / _all_radius; 
                if (_x isKindOf "Man") then { 
                    _man = _x; 
                    { 
                        _safeZone = _x; 
                        if ((_safeZone isKindOf "House") || (_safeZone isKindOf "Building")) then { 
                            _disToSafe = _man distance _safeZone; 
                            if ((_disToSafe < 10) && (_man call pr_fnc_inHouse) && !(_safeZone animationPhase "Door_1_rot" > 0) && !(_safeZone animationPhase "Door_2_rot" > 0)) then { 
                                while { ((_disToSafe < 10) && (_man call pr_fnc_inHouse) && !(_safeZone animationPhase "Door_1_rot" > 0) && !(_safeZone animationPhase "Door_2_rot" > 0)) } do { sleep 1; }; 
                            }; 
                        }; 
                    } forEach _radFree; 


                    _damage = _radDamage * _radiation; 
                    _damage = _damage + random _damage; 
                    _man setDamage ((getDammage _man) + _damage); 
                    if (_man == player && _damage > 0.01) then { 
                        titleText ["", "BLACK IN"]; 
                    }; 
                } else { 
                    {
                        _safeZone = _x; 
                        _crew = crew _safeZone; 
                        {
                            if (vehicle _x == _safeZone) then { 
                                while { ((vehicle _x == _safeZone) && !((_safeZone animationPhase (getText([_safeZone, [1]] call pr_fnc_getTurret >> "animationSourceHatch"))) == 1)) } do { 
                                    sleep 1; 
                                }; 
                            }; 
                        } forEach _crew; 
                    } forEach _radFree; 

                    _radiation = _carArmor * _radiation; 
                    _damage = _radDamage * _radiation; 
                    {
                        _dmg = _damage + random _damage; 
                        _x setDamage ((getDammage _x) + _dmg); 
                        if (_x == player && _dmg > 0.01) then { 
                            titleText ["", "BLACK IN"]; 
                        }; 
                    } forEach (crew _x); 
                }; 
            }; 
        }; 
    } forEach ([_posx, _posy, 0] nearObjects ["All", _current_radius]); 

    sleep (1 + random 1); 
    if ((time - _blast_time) > _irradTime) then { 
        _radius2 = _radius2 / 2; 
        _blast_time = _blast_time + _irradTime; 
        _v = _radius2 / (2 * _irradTime); 
    }; 
}; 
