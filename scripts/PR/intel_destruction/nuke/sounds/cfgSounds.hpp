
class nuclear_boom { 
    name = "nuclear_boom"; 
    sound[] = {"scripts\PR\intel_destruction\nuke\sounds\nuclear_boom.ogg", db+0, 1.0}; 
    titles[] = {}; 
}; 

class nuclear_geiger { 
    name = "nuclear_geiger"; 
    sound[] = { "scripts\PR\intel_destruction\nuke\sounds\geiger.ogg", "db -10", 1.0 };
    titles[] = {};
}; 

class geiger_1 { 
    name = "geiger_1"; 
    sound[] = { "scripts\PR\intel_destruction\nuke\sounds\geiger_1.ogg", "db+1", 1.0 }; 
    titles[] = {}; 
}; 

class geiger_2 { 
    name = "geiger_2"; 
    sound[] = { "scripts\PR\intel_destruction\nuke\sounds\geiger_2.ogg", "db+2", 1.0 }; 
    titles[] = {}; 
}; 

class geiger_3 { 
    name = "geiger_3"; 
    sound[] = { "scripts\PR\intel_destruction\nuke\sounds\geiger_3.ogg", "db+3", 1.0 }; 
    titles[] = {}; 
}; 