/* fn_callSupplyDrop.sqf 
*  Author: PapaReap 
* 
*  ver 1.1 - 2016-01-07 code cleanup and made into function 
*  ver 1.0 - 2015-03-11 
*/ 

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_callSupplyDrop start"]; }; 

if (isDedicated) exitWith {}; 

if !((p1) == (player)) exitWith {}; 

if (isNil "startTimer") then { startTimer = false; }; 
if (isNil "heliSupplyDrop") then { heliSupplyDrop = false; publicVariable "heliSupplyDrop"; }; 

if (heliSupplyDrop) then { 
    _sleep = ["CDWait",600] call BIS_fnc_getParamValue; 
    _name = name player; 
    timeNow = floor(serverTime); 
    placeTimeLeft = _sleep - (timeNow - placeTime); 
    _min = floor (placeTimeLeft / 60); 
    _sec = floor (placeTimeLeft) - (60 * _min); 
    _secHand = ""; 
    if ((floor (placeTimeLeft / 60)) < 1) then { _secHand = 'sec' } else { _secHand = 'min' }; 
    hint parseText format ["%1<br/><t color='#ff5d00'>No Supply Drops Available<br/>Wait %2m %3s for Heli to RTB</t>", _name, _min, _sec, _secHand]; 
    onMapSingleClick ""; 
    sleep 15; 
    hintSilent ""; 
    if (heliSupplyDrop) exitWith {}; 
}; 

if !(heliSupplyDrop) then { 
    _typeDrop = ""; 
    _typeDrop = toUpper (_this select 1); 
    _dropMarker = ""; 
    _typeCrate = ""; 

    if (_typeDrop == "AMMO") then { 
        missionNameSpace setVariable ["supplyDropMarker", "Ammo Drop"]; 
        if (((prWeapons == 5) || (prWeapons == 6) || (prWeapons == 7) || (prWeapons == 8)) && (masOn)) then { // Massi 
            _typeCrate = "Box_mas_us_rifle_Wps_F"; 
        } else { 
            if ((prWeapons == 10) && (rhsOn)) then {  // RHS 
                _typeCrate = "rhsusf_mags_crate"; 
            } else { 
                _typeCrate = "Box_NATO_Ammo_F"; 
            }; 
        }; 
        missionNameSpace setVariable ["supplyDropCrate", _typeCrate]; 
        missionNameSpace setVariable ["supplyDropHint", "Ammo Supply"]; 
        missionNameSpace setVariable ["typeSupplyDrop", "Ammo"]; 
    } else { 
        if (_typeDrop == "MED") then { 
            missionNameSpace setVariable ["supplyDropMarker", "Med Drop"]; 
            if (aceOn) then { missionNameSpace setVariable ["supplyDropCrate", "ACE_medicalSupplyCrate"]; } else { missionNameSpace setVariable ["supplyDropCrate", "Box_NATO_Support_F"]; }; 
            missionNameSpace setVariable ["supplyDropHint", "Medical Supply"]; 
            missionNameSpace setVariable ["typeSupplyDrop", "Med"]; 
        } else { 
            if (_typeDrop == "MASH") then { 
                missionNameSpace setVariable ["supplyDropMarker", "Mash Drop"]; 
                missionNameSpace setVariable ["supplyDropCrate", "B_Slingload_01_Medevac_F"]; 
                missionNameSpace setVariable ["supplyDropHint", "Field Mash"]; 
                missionNameSpace setVariable ["typeSupplyDrop", "Mash"]; 
            } else { 
                if (_typeDrop == "MH9") then { 
                    missionNameSpace setVariable ["supplyDropMarker", "MH-9 Drop"]; 
                    missionNameSpace setVariable ["supplyDropCrate", "B_Heli_Light_01_F"]; 
                    missionNameSpace setVariable ["supplyDropHint", "MH-9 Little Bird"]; 
                    missionNameSpace setVariable ["typeSupplyDrop", "MH9"]; 
                }; 
            }; 
        }; 
    }; 

    supplyClick = false; 
    openMap [true, false]; 
    //["<t size='0.8' color='#00E0FD'>" + "Place drop location on map<br/>(click location)<br/><br/>" + "</t>" + "<t size='0.8' color='#ff9900'>exit map to cancel" + "</t>",0,0.8,8,1] spawn BIS_fnc_dynamicText; 
    ["<t size='0.8' color='#00E0FD'>" + "Place drop location on map<br/>(click location)<br/><br/>" + "</t>" + "<t size='0.8' color='#ff9900'>exit map to cancel" + "</t>",0,0.8,8,1] remoteExec ["BIS_fnc_dynamicText",p1,false]; 
    if (isNil "supplyID") then { supplyID = 1; }; 
    if (isNil "supplyID_new") then { supplyID_new = 0; }; 
    supplyID = supplyID + supplyID_new; 

    ["supplyID","onMapSingleClick", { 
        _dropMarker = missionNameSpace getVariable "supplyDropMarker"; 
        _typeCrate = missionNameSpace getVariable "supplyDropCrate"; 
        _supplyHint = missionNameSpace getVariable "supplyDropHint"; 
        _typeDrop = missionNameSpace getVariable "typeSupplyDrop"; 
        heliSupplyDrop = true; publicVariable "heliSupplyDrop"; 
        click = _pos; 
        supplyClick = true; 
        _supplyDrop = ""; 
        if (getMarkerColor (missionNameSpace getVariable "supplyDropMarker") == "") then { 
            _supplyDrop = createMarker [_dropMarker, click]; 
            _supplyDrop setMarkerType "hd_end"; 
            _supplyDrop setMarkerColor "ColorBlue"; 
            _supplyDrop setMarkerText _dropMarker; 
            missionNameSpace setVariable ["supplyDropMarker", _supplyDrop]; 
        } else { 
           _supplyDrop setMarkerPos click; 
        }; 

        _name = name player; 
        _gridPos = mapGridPosition (getMarkerPos (missionNameSpace getVariable "supplyDropMarker")); 

        onMapSingleClick ""; 
        ["",0,0,0,0] spawn BIS_fnc_dynamicText; 
        hint parseText format ["%1<br/><t color='#19e56e'>%2 Drop Called</t>", _name, _supplyHint]; 
        p1 setVariable ["dropArray", ["supplyHeliSpawn", _supplyDrop, 1, 1, _typeCrate, 1, _gridPos, _typeDrop, _supplyHint], true]; 
        [[p1], "pr_fnc_supplyDrop"] call BIS_fnc_MP; 
        startTimer = true; 
    }] call BIS_fnc_addStackedEventHandler; 

    sleep .01; 
    waitUntil { (!visibleMap) || (supplyClick) }; 
    if (!supplyClick) exitWith { 
        titleText ["","PLAIN",0.5]; 
        click = nil; 
        onMapSingleClick ""; 
        //["<t size='0.8' color='#ff9900'>" + "Supply drop request cancelled" + "</t>",0,0.8,4,1] spawn BIS_fnc_dynamicText; 
        ["<t size='0.8' color='#ff9900'>" + "Supply drop request cancelled" + "</t>",0,0.8,4,1] remoteExec ["BIS_fnc_dynamicText",p1,false]; 
    }; 

    if (startTimer) then { 
        _sleep = ["CDWait",600] call BIS_fnc_getParamValue; 
        sleep .01; 
        placeTime = floor (serverTime); 
        countDown = placeTime + _sleep - 15; 
        supplyID_new = (supplyID + supplyID_new) - 1; 
        ["supplyID", "onMapSingleClick"] call BIS_fnc_removeStackedEventHandler; 
        _gridPos = mapGridPosition (getMarkerPos (missionNameSpace getVariable "supplyDropMarker")); 
        _supplyHint = missionNameSpace getVariable "supplyDropHint"; 
        _requestDropHint = format ["Base Firefly, Requesting %1 Drop on Grid Location %2", _supplyHint, _gridPos]; 
        logic_Leader sideChat _requestDropHint; 
        logic_Leader_SideChat = _requestDropHint; publicVariable "logic_Leader_SideChat"; 
        sleep 15; 
        hintSilent ""; 

        while { ((heliSupplyDrop) || (countDown > placeTime)) } do { 
            countDown = countDown - 5; 
            sleep 5; 
        }; 

        countDown = 0; 
        startTimer = false; 
        heliSupplyDrop = false; publicVariable "heliSupplyDrop"; 
        if (missionNameSpace getVariable "supplyDropMarker" != "") then { 
            _supplyDrop = missionNameSpace getVariable "supplyDropMarker"; 
            if (getMarkerColor _supplyDrop == "ColorBlue") then { deleteMarker _supplyDrop}; 
            missionNameSpace setVariable ["supplyDropMarker", ""]; 
        }; 
    }; 
}; 

if !(pr_deBug == 0) then { diag_log format ["*PR* fn_callSupplyDrop complete"]; }; 
