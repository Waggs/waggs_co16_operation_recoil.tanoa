// Maintains the text box on the right, below "SELECTED TYPE".
// This script first adds values to the combobox to the left, then maintains updated text values to the right.
// Also sets missionNamespace variable "ShellUsed" for future references.

waitUntil {!isNull (findDisplay 1111) && {dialog}}; 
waitUntil {!isNil "ArtilleryUsed"}; 
_display = 3002; 

_text = ""; 
_selectdisplay = 1012; 
_startingArray = []; 

// This adds standard values to the list box, these values will always be there regardless of artillery.
{
    lbAdd [_display, _x]; 
} forEach _startingArray; 

while {dialog} do { 
    waitUntil {lbCurSel _display != -1}; 
    switch (lbCurSel _display) do { 
        case 0: 
        { 
            _text0 = lbText [_display, 0]; 
            _shellUsed0 = _text0 call fnc_g_findClassname; 
            ctrlSetText [_selectdisplay, _text0]; 
            if (_text0 == "Guided") then { 
                missionNameSpace setVariable ["typeShellUsed", ["2Rnd_155mm_Mo_guided"]]; //call fnc_g_findClassname 
            } else { 
                if (_text0 == "Laser Guided") then { 
                    missionNameSpace setVariable ["typeShellUsed", ["2Rnd_155mm_Mo_LG"]]; 
                } else { 
                    missionNameSpace setVariable ["typeShellUsed", [_shellUsed0]]; //call fnc_g_findClassname 
                }; 
            }; 
            missionNamespace setVariable ["typeShellSelected", 1]; 
        }; 
        case 1: 
        { 
            _text1 = lbText [_display, 1]; 
            _shellUsed1 = _text1 call fnc_g_findClassname; 
            ctrlSetText [_selectdisplay, _text1]; 
            if (_text1 == "Guided") then { 
                missionNameSpace setVariable ["typeShellUsed", ["2Rnd_155mm_Mo_guided"]]; //call fnc_g_findClassname 
            } else { 
                if (_text1 == "Laser Guided") then { 
                    missionNameSpace setVariable ["typeShellUsed", ["2Rnd_155mm_Mo_LG"]]; 
                } else { 
                    missionNameSpace setVariable ["typeShellUsed", [_shellUsed1]]; //call fnc_g_findClassname 
                }; 
            }; 
            missionNamespace setVariable ["typeShellSelected", 1]; 
        }; 
        case 2: 
        { 
            _text2 = lbText [_display, 2]; 
            _shellUsed2 = _text2 call fnc_g_findClassname; 
            ctrlSetText [_selectdisplay, _text2]; 
            if (_text2 == "Guided") then { 
                missionNameSpace setVariable ["typeShellUsed", ["2Rnd_155mm_Mo_guided"]]; 
            } else { 
                if (_text2 == "Laser Guided") then { 
                    missionNameSpace setVariable ["typeShellUsed", ["2Rnd_155mm_Mo_LG"]]; 
                } else { 
                    missionNameSpace setVariable ["typeShellUsed", [_shellUsed2]]; 
                }; 
            }; 
            missionNamespace setVariable ["typeShellSelected", 1]; 
        };
        case 3: 
        { 
            _text3 = lbText [_display, 3]; 
            _shellUsed3 = _text3 call fnc_g_findClassname; 
            ctrlSetText [_selectdisplay, _text3]; 
            if (_text3 == "Guided") then { 
                missionNameSpace setVariable ["typeShellUsed", ["2Rnd_155mm_Mo_guided"]]; 
            } else { 
                if (_text3 == "Laser Guided") then { 
                    missionNameSpace setVariable ["typeShellUsed", ["2Rnd_155mm_Mo_LG"]]; 
                } else { 
                    missionNameSpace setVariable ["typeShellUsed", [_shellUsed3]]; 
                }; 
            }; 
            missionNamespace setVariable ["typeShellSelected", 1]; 
        }; 
        case 4: 
        { 
            _text4 = lbText [_display, 4]; 
            _shellUsed4 = _text4 call fnc_g_findClassname; 
            ctrlSetText [_selectdisplay, _text4]; 
            if (_text4 == "Guided") then { 
                missionNameSpace setVariable ["typeShellUsed", ["2Rnd_155mm_Mo_guided"]]; 
            } else { 
                if (_text4 == "Laser Guided") then { 
                    missionNameSpace setVariable ["typeShellUsed", ["2Rnd_155mm_Mo_LG"]]; 
                } else { 
                    missionNameSpace setVariable ["typeShellUsed", [_shellUsed4]]; 
                }; 
            }; 
            missionNamespace setVariable ["typeShellSelected", 1]; 
        }; 
        case 5: 
        { 
            _text5 = lbText [_display, 5]; 
            _shellUsed5 = _text5 call fnc_g_findClassname; 
            ctrlSetText [_selectdisplay, _text5]; 
            if (_text5 == "Guided") then { 
                missionNameSpace setVariable ["typeShellUsed", ["2Rnd_155mm_Mo_guided"]]; 
            } else { 
                if (_text5 == "Laser Guided") then { 
                    missionNameSpace setVariable ["typeShellUsed", ["2Rnd_155mm_Mo_LG"]]; 
                } else { 
                    missionNameSpace setVariable ["typeShellUsed", [_shellUsed5]]; 
                }; 
            }; 
            missionNamespace setVariable ["typeShellSelected", 1]; 
        }; 
        case 6: 
        { 
            _text6 = lbText [_display, 6]; 
            _shellUsed6 = _text6 call fnc_g_findClassname; 
            ctrlSetText [_selectdisplay, _text6]; 
            if (_text6 == "Guided") then { 
                missionNameSpace setVariable ["typeShellUsed", ["2Rnd_155mm_Mo_guided"]]; 
            } else { 
                if (_text6 == "Laser Guided") then { 
                    missionNameSpace setVariable ["typeShellUsed", ["2Rnd_155mm_Mo_LG"]]; 
                } else { 
                    missionNameSpace setVariable ["typeShellUsed", [_shellUsed6]]; 
                }; 
            }; 
            missionNamespace setVariable ["typeShellSelected", 1]; 
        }; 
        case 7: 
        { 
            _text7 = lbText [_display, 7]; 
            _shellUsed7 = _text7 call fnc_g_findClassname; 
            ctrlSetText [_selectdisplay, _text7]; 
            if (_text7 == "Guided") then { 
                missionNameSpace setVariable ["typeShellUsed", ["2Rnd_155mm_Mo_guided"]]; 
            } else { 
                if (_text7 == "Laser Guided") then { 
                    missionNameSpace setVariable ["typeShellUsed", ["2Rnd_155mm_Mo_LG"]]; 
                } else { 
                    missionNameSpace setVariable ["typeShellUsed", [_shellUsed7]]; 
                }; 
            }; 
            missionNamespace setVariable ["typeShellSelected", 1]; 
        }; 
        case 8: 
        { 
            _text8 = lbText [_display, 8]; 
            _shellUsed8 = _text8 call fnc_g_findClassname; 
            ctrlSetText [_selectdisplay, _text8]; 
            if (_text8 == "Guided") then { 
                missionNameSpace setVariable ["typeShellUsed", ["2Rnd_155mm_Mo_guided"]]; 
            } else { 
                if (_text8 == "Laser Guided") then { 
                    missionNameSpace setVariable ["typeShellUsed", ["2Rnd_155mm_Mo_LG"]]; 
                } else { 
                    missionNameSpace setVariable ["typeShellUsed", [_shellUsed8]]; 
                }; 
            }; 
            missionNamespace setVariable ["typeShellSelected", 1]; 
        }; 
        case 9: 
        { 
            _text9 = lbText [_display, 9]; 
            _shellUsed9 = _text9 call fnc_g_findClassname; 
            ctrlSetText [_selectdisplay, _text9]; 
            if (_text9 == "Guided") then { 
                missionNameSpace setVariable ["typeShellUsed", ["2Rnd_155mm_Mo_guided"]]; 
            } else { 
                if (_text9 == "Laser Guided") then { 
                    missionNameSpace setVariable ["typeShellUsed", ["2Rnd_155mm_Mo_LG"]]; 
                } else { 
                    missionNameSpace setVariable ["typeShellUsed", [_shellUsed9]]; 
                }; 
            }; 
            missionNamespace setVariable ["typeShellSelected", 1]; 
        }; 
    }; 
}; 

missionNamespace setVariable ["typeShellSelected", 0]; 
