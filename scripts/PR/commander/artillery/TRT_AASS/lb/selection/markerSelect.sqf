// This script runs as soon as the player presses the "FIRE" Button on the GUI.
// It is a check before the actual fire mission to see if a pre-placed map marker is used or if the player has to be prompted to place one.
// After it is done it will execute functions\fn_fireMission.sqf.
//_unit = player; 

_cfg = configFile >> "CfgMarkers"; // Path to "CfgMarkers".
_allowedtype = "hd_destroy"; // This is the type of marker that is accepted as fire mission marker. 
//_allowedtype2 = "mil_destroy"; 
//_allowedtype3 = "Destroy"; 
_allowedcolor = "ColorRed"; // This colour is the only one that is accepted as fire mission marker.
_allowedText = "FIRE MISSION"; // This text has to be included in the placed marker to be accepted as fire mission marker.
_allowedText2 = "FIRE ARTY"; 
_marker = ""; // Empty string for script reference. The reason why it is defined outside is so it will be usable outside the forEach loop.
_count = 0; // Variable to limit accepted markers to one.

{
    // This loop checks ALL markers placed on map for the below condition. If all of the conditions are fulfilled, it will add the marker to _marker.
    // Only one marker can be added this way. It will be passed onto functions\fn_fireMission.sqf.
    // The marker type has to be equal to "hd_destroy" (black cross), has to be coloured red, and needs to have "FIRE MISSION" as text.
    // The _count variable needs to be 0 as well.

    if (getMarkerType _x == _allowedtype && {getMarkerColor _x == _allowedColor} && {((markerText _x == _allowedText) || (markerText _x == _allowedText2))} && {_count < 1}) then { 
        _marker = createMarker [str _x + "_marker", getMarkerPos _x]; 
        missionNamespace setVariable ["MarkerPositionUsed", (getMarkerPos _marker)]; 
        missionNamespace setVariable ["MarkerUsed", _marker]; 
        _count = _count + 1; // Add 1 to _count to prevent more markers from being added.
    }; 
} forEach allMapMarkers; // Checks all markers on map.

// If no suitable marker was added beforehand, this if statement executes lb\setMarker.sqf, 
// which allows the player to set his own marker via onMapSingleClick.
if (_marker == "") then { 
    _nul = [] execVM "scripts\PR\commander\artillery\TRT_AASS\lb\setMarker.sqf"; 
    breakOut "noMarker"; // Due to the "noMarker" scope not being defined through scopeName in this script, the script exits instead. Alternative to the usually unreliable exitWith.
}; 

// Calls main functions\fn_fireMission.sqf.
hint "Pre-set marker found."; 
call TRT_fnc_fireMission; 
