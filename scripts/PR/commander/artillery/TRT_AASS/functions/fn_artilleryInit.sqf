/*
By tryteyker
V1.0

Initializes Artillery Module GUI.

Parameters none and returns nothing.
*/

//_units = _this select 0; 
_unit = player; 

//{ 
//    _x addAction ["Access Artillery Computer","[_this select 0, _this select 1, _this select 2] spawn TRT_fnc_artilleryComputer"]; 
//} forEach [_units]; 

//missionNamespace setVariable ["UnitsWithArtComp", _units]; 
_unit setVariable ["UnitsWithArtComp", _unit, true]; 