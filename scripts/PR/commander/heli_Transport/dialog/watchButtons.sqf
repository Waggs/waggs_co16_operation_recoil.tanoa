
// Will unlock buttons automatically once all values are accepted.

waitUntil { !isNull (findDisplay 1801) && { dialog } };

// send transport button.
_buttonTransport = 1813;
ctrlEnable [1813,false];
_buttonTransport spawn {
    _buttonTransport = _this;
    while { dialog && { true } } do {
        waitUntil { ((missionNamespace getVariable "teamSelected" == 1) && (missionNamespace getVariable "transportSelected" == 1) && (missionNamespace getVariable "actionSelected" == 1)) };
        ctrlEnable [_buttonTransport, true]; // Enable send transport button.
        false;
    };
};

_buttonTeamToTransport = 1810; ctrlEnable [1810,true]; // transport team button
_buttonTransportType = 1811; ctrlEnable [1811,true]; // transport type button
_buttonTransportWP = 1812; ctrlEnable [1812,true]; // transport waypoint type button
[_buttonTeamToTransport, _buttonTransportType, _buttonTransportWP] spawn {
    _buttonTeamToTransport = _this select 0;
    _buttonTransportType = _this select 1;
    _buttonTransportWP = _this select 2;
    while { dialog } do {
        waitUntil { !(isNil "pr_heliTrans") };
        waitUntil { ((pr_transportAlive) && !(heliTransportEnd)) };
        ctrlEnable [_buttonTeamToTransport, false];
        ctrlEnable [_buttonTransportType, false];
        ctrlEnable [_buttonTransportWP, false];
        waitUntil { (!(pr_transportAlive) && ((heliTransportEnd) || !(startHeliTimer))) };
        ctrlEnable [_buttonTeamToTransport, true];
        ctrlEnable [_buttonTransportType, true];
        ctrlEnable [_buttonTransportWP, true];
    };
};

// liftoff transport button
_buttonLiftoff = 1815;
ctrlEnable [1815,false];
_buttonLiftoff spawn {
    _buttonLiftoff = _this;
    while { dialog } do {
        waitUntil { !(isNil "pr_heliTrans") };
        waitUntil { ((pr_transportAlive) && (pr_liftoff_order)) };
        ctrlEnable [_buttonLiftoff, true];
        waitUntil { (!(pr_transportAlive) || !(pr_liftoff_order)) };
        ctrlEnable [_buttonLiftoff, false];
    }; 
};

// change route buttons
_buttonSetRoutePosition = 1821; ctrlEnable [1821,true];
_buttonChangeRouteType = 1822; ctrlEnable [1822,true];
_buttonFlyHeight = 1823; ctrlEnable [1823,true];
_buttonTransSpeed = 1824; ctrlEnable [1824,true];
[_buttonSetRoutePosition, _buttonChangeRouteType, _buttonFlyHeight, _buttonTransSpeed] spawn {
    ctrlEnable [1821,false];
    ctrlEnable [1822,false];
    ctrlEnable [1823,false];
    ctrlEnable [1824,false];
    _buttonSetRoutePosition = _this select 0;
    _buttonChangeRouteType = _this select 1;
    _buttonFlyHeight = _this select 2;
    _buttonTransSpeed = _this select 3;
    while { dialog } do {
        waitUntil { !(isNil "pr_heliTrans") };
        waitUntil { ((pr_transportAlive) && !(heliTransportEnd)) };
        ctrlEnable [_buttonSetRoutePosition, true];
        ctrlEnable [_buttonChangeRouteType, true];
        ctrlEnable [_buttonFlyHeight, true];
        ctrlEnable [_buttonTransSpeed, true];
        waitUntil { (!(pr_transportAlive) || (heliTransportEnd)) };
        ctrlEnable [_buttonChangeRouteType, false];
        ctrlEnable [_buttonSetRoutePosition, false];
        ctrlEnable [_buttonFlyHeight, false];
        ctrlEnable [_buttonTransSpeed, false];
    };
};

// send transport button.
_buttonUseRouteChanges = 1825;
ctrlEnable [1825,false];
_buttonUseRouteChanges spawn {
    _buttonUseRouteChanges = _this;
    while { dialog && { true } } do {
        waitUntil { ((missionNamespace getVariable "positionTypeSelected" == 1) && (missionNamespace getVariable "changeRouteTypeSelected" == 1) && (missionNamespace getVariable "flyHeightSelected" == 1) && (missionNamespace getVariable "transSpeedSelected" == 1)) }; 
        ctrlEnable [_buttonUseRouteChanges, true];
        false; 
    }; 
};


// Use route button
_buttonQuickMove = 1832;
ctrlEnable [1832,false];
_buttonQuickMove spawn {
    _buttonQuickMove = _this;
    while { dialog } do {
        waitUntil { !(isNil "pr_heliTrans") };
        waitUntil { ((pr_transportAlive) && !(heliTransportEnd)) };
        ctrlEnable [_buttonQuickMove, true];
        waitUntil { (!(pr_transportAlive) || (heliTransportEnd)) };
        ctrlEnable [_buttonQuickMove, false];
    }; 
};


// Enable Fuel button
_buttonEnableFuel = 1833;
ctrlEnable [1833,false];
_buttonEnableFuel spawn {
    _buttonEnableFuel = _this;
    while { dialog } do {
        waitUntil { !(isNil "pr_heliTrans") };
        waitUntil { (!(isNull pr_heliTrans) && (pr_disAbleFuel)) }; //pr_disAbleFuel
        ctrlEnable [_buttonEnableFuel, true];
        waitUntil { ((isNull pr_heliTrans) || !(pr_disAbleFuel)) };
        ctrlEnable [_buttonEnableFuel, false];
    }; 
};

// Disable Fuel button
_buttonDisableFuel = 1834;
ctrlEnable [1834,false];
_buttonDisableFuel spawn {
    _buttonDisableFuel = _this;
    while { dialog } do {
        waitUntil { !(isNil "pr_heliTrans") };
        waitUntil { (!(isNull pr_heliTrans) && !(pr_disAbleFuel)) }; //pr_disAbleFuel
        ctrlEnable [_buttonDisableFuel, true];
        waitUntil { ((isNull pr_heliTrans) || (pr_disAbleFuel)) };
        ctrlEnable [_buttonDisableFuel, false];
    }; 
};

// rtb transport button
_buttonRTB = 1835;
ctrlEnable [1835,false];
_buttonRTB spawn {
    _buttonRTB = _this;
    while { dialog } do {
        waitUntil { !(isNil "pr_heliTrans") };
        waitUntil { ((pr_transportAlive) && !(heliTransportEnd)) };
        ctrlEnable [_buttonRTB, true]; // Enable rtb transport button.
        waitUntil { (!(pr_transportAlive) || (heliTransportEnd)) };
        ctrlEnable [_buttonRTB, false];
    };
};
