//--- fn_preciseSpawn.sqf
//--- ver 1.0 2015-05-28
//--- usage:
//--- [spawnObject, [unit, height, direction, (optional positions; "down","up","middle","auto"), (optional unit movement; 0 = no movement; 1 = movement allowed), (optional special init) ],[optional upsmon parameters; ""]] call pr_fnc_preciseSpawn;
//--- [ssSpawn, ["O_Sharpshooter_F", 19.979, 170]] call pr_fnc_preciseSpawn;
//--- [w1Spawn, ["O_soldier_M_F", 4.6, 150, "up", 0]] call pr_fnc_preciseSpawn;
//--- [ss_1, ["O_Sharpshooter_F", 1, 0,"auto", 0, ],["CARELESS"]] call pr_fnc_preciseSpawn;
//--- [randomManSpawn, ["b_soldier_f", 10.125, 45, "up", 0, "west"], ["combat"]] call pr_fnc_preciseSpawn;
//--- [randomManSpawn, [[r_men], 10.125, 45, "up", 0], ["combat"]] call pr_fnc_preciseSpawn;

//--- [mortSpawn_1, ["B_Mortar_01_F", 19.979, 10]] call pr_fnc_preciseSpawn;
//--- [st_6, ["O_soldier_M_F", 4.6, 193, "auto", 0]] call pr_fnc_preciseSpawn;

//--- [[ss_1, "west"], [[r_menDiver], 1, 0, "up", 0, "nul = [this,3] execVM 'scripts\PR\scripts\vehicle\functions\fn_unlimitedAmmo.sqf'; this allowDamage false"],["combat"]] call pr_fnc_preciseSpawn;

//--- [ss_1_1, ["O_Soldier_F", 0, 180, "up", 0, "nul = [this,3] execVM 'scripts\PR\scripts\vehicle\functions\fn_unlimitedAmmo.sqf'; this allowDamage false"],["combat"]] call pr_fnc_preciseSpawn;
//--- [[mortSpawn_1,"west"], [[b_mortar], 23, 10]] call pr_fnc_preciseSpawn;

//--- [[mortSpawn_1, "east"], [[r_staticHmgHigh], 0, 10]] call pr_fnc_preciseSpawn;
//--- [[mortSpawn_1, "west"], ["B_Soldier_F", 0, 10]] call pr_fnc_preciseSpawn;
//--- [[mortSpawn_1, "west"], ["B_Soldier_F", 0, 10, "auto", 1, "fred_1 = this;"]] call pr_fnc_preciseSpawn; fred_1 moveInGunner mort1;

//--- [thisTrigger, ["O_Sharpshooter_F", 0, 270], [[100,100],["combat"]]] call pr_fnc_preciseSpawn;

/*
r_carRandom = r_car call BIS_fnc_selectRandom;
r_supportVehRandom = r_supportVeh call BIS_fnc_selectRandom;
r_ifvRandom = r_ifv call BIS_fnc_selectRandom;
r_armorRandom = r_armor call BIS_fnc_selectRandom;
r_mbtRandom = r_mbt call BIS_fnc_selectRandom;
r_artilleryRandom = r_artillery call BIS_fnc_selectRandom;
*/

if (!isServer && hasInterface) exitWith {};
if ((isNull aiSpawnOwner) && !(isServer)) exitWith {};
if (!(isNull aiSpawnOwner) && !(aiSpawnOwner == player)) exitWith {};

_this spawn {
    [[], "pr_fnc_aiSpawner"] call BIS_fnc_MP;
    _spawn = _this select 0; pr_spawn=_spawn;
    _spawnPos = [0,0,0];
    _unit = (_this select 1) select 0;

    _side = EAST;
    if (prEnemySide == 1) then { _side = WEST; };
    if (prEnemySide == 2) then { _side = EAST; };
    if (prEnemySide == 3) then { _side = RESISTANCE; };

    if (typeName _spawn == "ARRAY") then {
        if (typeName (_spawn select 0) == "STRING") then {
            _spawnPos = getMarkerPos (_spawn select 0);
        } else {
            if (typeName (_spawn select 0) == "ARRAY") then {
                _spawnPos = _spawn select 0;
            } else {
                _spawnPos = getPos (_spawn select 0);
            };
        };

        _side = (_this select 0) select 1;

        if (typeName _side == "STRING") then {
            _side = toUpper _side;
            switch (_side) do {
                case "EAST":       {_side = EAST};
                case "WEST":       {_side = WEST};
                case "RESISTANCE": {_side = RESISTANCE};
                case "CIVILIAN":   {_side = CIVILIAN};
                default            {_side = EAST};
            };
        };
    } else {
        if (typeName _spawn == "STRING") then {
            _spawnPos = getMarkerPos _spawn;
        } else {
            _spawnPos = getPos _spawn;
        };
    };

    if (typeName _unit == "ARRAY") then {
        _randomManArray = (_this select 1) select 0;
        _unit = _randomManArray select 0;
        _random_man = _randomManArray call BIS_fnc_selectRandom;
        _unit = _random_man;
    };

    _height = (_this select 1) select 1;

    _spawnLoc = _spawnPos;
    _posX = _spawnLoc select 0;
    _posY = _spawnLoc select 1;
    _posZ = (_spawnLoc select 2) + _height;

    if ((count (_this select 1)) > 5) then {
        specialInit = (_this select 1) select 5;
    } else {
        specialInit = "";
    };
    _grp = createGroup _side;
    _unit createUnit [_spawnPos, _grp, specialInit];
    _man = leader _grp;
    _man setPosATL [_posX,_posY,_posZ];

    if (name aiSpawnOwner == "hc") then {
        { hcUnits = hcUnits + [_x] } forEach units _grp; publicVariable "hcUnits";
    } else {
        if (name aiSpawnOwner == "hc2") then {
            { hc2Units = hc2Units + [_x] } forEach units _grp; publicVariable "hc2Units";
        } else {
            { serverUnits = serverUnits + [_x] } forEach units _grp; publicVariable "serverUnits";
        };
    };

    if (prEnemyUnits == 4) then {
        if (_side == EAST) then {
            { [_x] spawn pr_fnc_r_TalibanClothing } forEach units _grp;
        };
    };

    //if (pr_addDragToAll) then { { _nil = [_x] spawn pr_fnc_dragAddDrag; } forEach units _grp; };
    if (pr_addDragToAll) then { { [_x] remoteExec ["pr_fnc_dragAddDrag", 0, true]; } forEach units _grp; };

    _dir = (_this select 1) select 2;
    (leader _grp) setFormDir _dir;

    if ((count (_this select 1)) > 3) then {
        _position = "";
        _position = (_this select 1) select 3;
        _position = toUpper _position;
        _man setUnitPos _position;
    };
    if ((count (_this select 1)) > 4) then {
        if ((_this select 1) select 4 == 0) then {
            doStop _man;
        };
    };

    //upsmon stuff *****
    _upsmon = [_this, 2, [], [[]]] call BIS_fnc_param; pr_upsmon=_upsmon;
    _isUpsmon = false;
    _area = "";
    //if (_upsmon select 0 == 0) then {
    if (typeName (pr_upsmon select 0) == "SCALAR") then {
    } else {
        if (count _upsmon > 0) then {
            _isUpsmon = true;
            _area = _upsmon select 0;
            if (count _upsmon > 1) then {
                _upsmonA = _upsmon select 1; [_grp, _area, _upsmonA] call PR_fnc_upsSpawner;
            } else {
                [_grp, _area] call PR_fnc_upsSpawner;
            };
        };
    };
};
