// usage (in a trigger or in the units init line): 
// _nil = [unit, amount of flak] execVM "scripts\PR\scripts\misc\flak.sqf"; 
// _nil = [plane1, 200] execVM "scripts\PR\scripts\misc\flak.sqf"; 
// Optional: _nil = [unit, amount of flak, tempo/extra sleep] execVM "scripts\PR\scripts\misc\flak.sqf"; 
// tempo: 1 = no change, 2 = double sleep, 3 = triple sleep. 
// _nil = [plane1, 200, 1.5] execVM "scripts\PR\scripts\misc\flak.sqf"; 
// where plane1 is the name of the unit and 200 is the amount of flak rounds
// works better in a trigger 

private ["_unit","_amountflak","_tempo"]; 
//_unit       = _this select 0; 
_unit = [_this, 0, objNull] call BIS_fnc_param; 
//_amountflak = _this select 1; 
_amountflak = [_this, 1, 50] call BIS_fnc_param; 
//if ((count _this) > 2) then { 
//    _tempo      = _this select 2; 
//} else { 
//    _tempo = 0; 
//}; 
_tempo = [_this, 2, 0] call BIS_fnc_param; 
_amountflak = _amountflak; 
_range = 1; 

_flaktype   = "SmallSecondary"; 
_i = 0; 

while {_i = _i + 1; ((_i < _amountflak) && (alive _unit))} do { 
    _flakLoc  = getPos _unit; 
    _flakLocX = _flakLoc select 0; 
    _flakLocY = _flakLoc select 1; 
    _flakLocZ = _flakLoc select 2; 
   
    if ((random 2) > 1.87) then { 
        _range = 10; 
    }; 
    _flakLocX = _flakLocX + ((random 150)-75)/_range; 
    _flakLocY = _flakLocY + ((random 50)-25)/_range; 
    _flakLocZ = _flakLocZ + ((random 40)-20)/_range; 
   
    _flak1   = _flaktype createVehicle [_flakLocX, _flakLocY, _flakLocZ]; 
    _hammer = "weaponHolder" createVehicle [_flakLocX, _flakLocY, _flakLocZ]; 
    _hammer  setPos [_flakLocX, _flakLocY, _flakLocZ]; 
//    sleep (random 0.2); 
//    sleep (((random 0.5) Max 0.1) Min 1.0); 
    sleep (random 0.5) * _tempo ; 
    //sleep (random _tempo); 
    _range = 1; 
}; 