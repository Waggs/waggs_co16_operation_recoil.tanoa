/* fn_haloToPos.sqf
*  Author: PapaReap
*  function name: pr_fnc_haloToPos

*  HALO Drop To Position
*  Ver 1.0  2016-06-11 initial release
*  Usage - in init field: _nil = [this, "jumpKavala", 2000,"Paradrop over Kavala"] execVM "scripts\PR\scripts\misc\fn_haloToPos.sqf";

*  Arguments: 
*  0: <JUMP ACTION OBJECT> 
*  1: <OBJECT/MARKER/POSITION TO JUMP TO>
*  2: <HEIGHT OF HALO JUMP
*  3: <TEXT FOR ACTION>
*/

private ["_object","_target","_height","_actionText","_text","_targetPos"];
_object = _this select 0;
_target = _this select 1;
_height = _this select 2;
_actionText = _this select 3;
_text = format ["<t color='#00aeff'>%1</t>", _actionText];
_targetPos = [0,0,0];

if (typeName _target == "STRING") then {
    _targetPos = getMarkerPos _target;
} else {
    if (typeName _target == "ARRAY") then {
        _targetPos = _target;
    } else {
        _targetPos = getPos _target;
    };
};

_targetPos set [2, _height];
_object setVariable ["targetPos", _targetPos, true];

_object addAction [
    _text,
    {
        private ["_done","_player"];
        _object = _this select 0;
        _player = _this select 1;
        _targetPos = _object getVariable "targetPos";

        _player setPos _targetPos;
        _player setCaptive true;
        waitUntil {((getPosATL _player) select 2) < 200};
        [_player] call BIS_fnc_halo;
        waitUntil { isTouchingGround _player };
        sleep 3;
        _player setCaptive false;
    }
];
