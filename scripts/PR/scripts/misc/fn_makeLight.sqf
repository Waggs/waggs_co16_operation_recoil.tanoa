/* fn_makeLight.sqf
*  Author: PapaReap
*  function name: pr_fnc_makeLight
*  ver 1.0 2016-08-24

*  Arguments:
*  0: <LIGHT TYPE>       e.g. - (REQUIRED) "LargeFlood", "", "", "", "" - (string)
*  0: <OPT LIGHT ARRAY>
*     0: <LIGHT TYPE>    e.g. - (REQUIRED) see: above
*     1: <LIGHT COUNT>   e.g. - total amount of lights to be used, will place additional lights equal spaced around 360 degrees - (scalar)
*
*  1: <POSITION>         e.g. - (REQUIRED) "marker1", trigger, object or [position]
*
*  2: <OPT DIRECTION>    e.g. - direction to face light. If -1 light direction will be direction of object, marker or trigger. (scalar) - default 0
*
*  3: <LIGHT MODE>       e.g. - mode can be: "ON", "OFF" or "AUTO" (Lamp is on only during nighttime) - default "AUTO"
*
*  EXAMPLES:
*  ["LargeFlood", "flood_1", 214, "auto"] remoteExec ["pr_fnc_makeLight", 0, false];
*  ["LargeFlood", flood_1, 214, "auto"] remoteExec ["pr_fnc_makeLight", 0, false];
*  ["LargeFlood", thisTrigger, 214, "auto"] remoteExec ["pr_fnc_makeLight", 0, false];
*  ["LargeFlood", thisTrigger, -1, "on"] remoteExec ["pr_fnc_makeLight", 0, false];
*  [["LargeFlood",4], thisTrigger, -1, "auto"] remoteExec ["pr_fnc_makeLight", 0, false];
*/

if !(isServer) exitWith {};

_lightType = "";
_pos = [0,0,0];
_dir = 0;
_mode = "AUTO";
_area = [];
_lightCt = 1;

_light = _this select 0;
if (typeName _light == "ARRAY") then {
    _light = (_this select 0) select 0;
    _lightCt = (_this select 0) select 1;
};

_spawn = _this select 1;

if (count _this > 2) then { _dir = _this select 2; };
if (count _this > 3) then { _mode = _this select 3; };

if (_dir < 0) then {
    switch (typeName _spawn) do {
        case "OBJECT": { _area = triggerarea _spawn; if (count _area > 0) then { _dir = _area select 2; } else { _dir = getDir _spawn; }; };
        case "STRING": { _dir = markerDir _spawn; };
    };
}; 

if (_light == "LargeFlood") then {
    _lightType = "Land_LampHalogen_F";
    _dir = _dir + 90;
}; 
//"Land_PowerPoleWooden_L_F"
//"Land_LampStreet_small_F"

if (typeName _spawn == "STRING") then {
    _pos = getMarkerPos _spawn;
} else {
    if (typeName _spawn == "ARRAY") then {
        _pos = _spawn;
    } else {
        _pos = getPos _spawn;
    };
};

_sections = 360 / _lightCt;

for [{ _loop = 0 }, { _loop < _lightCt }, { _loop = _loop + 1 }] do { 
    _newLight = createVehicle [_lightType, _pos,[], 0, "CAN_COLLIDE"];
    _newLight setdir _dir;
    _newLight switchLight _mode;
    _dir = _dir + _sections;
}; 
