//0 = [thisTrigger, west, allUnits, 800, true, false] execVM "scripts\PR\scripts\misc\unitCaptivity.sqf"; 
//0 = [thisTrigger, west, allUnits, 600, false, true, [30,3000]] execVM "scripts\PR\scripts\misc\unitCaptivity.sqf"; 

_obj = _this select 0; 
_side = _this select 1; 
_type = _this select 2; 
_distance = _this select 3; 
_captive = _this select 4; 
_persist = _this select 5; 
_heightLower = 0; 
_heightUpper = 10000; 

if (count _this > 6) then { 
    _heightArr = _this select 6; 
    _heightLower = _heightArr select 0; 
    _heightUpper = _heightArr select 1; 
}; 
pr_heightLower = _heightLower; 
pr_heightUpper = _heightUpper; 

{ 
    //if ((((side _x) == _side) || ((side _x) == civilian)) && (_x distance _obj < _distance)) then { 
    if ((((side _x) == _side) || ((side _x) == civilian)) && (_x distance _obj < _distance)) then { 
        if (_persist) then { 
            while { ((((getPosATL _x) select 2) >= _heightLower) && (((getPosATL _x) select 2) <= _heightUpper)) do { sleep 1; }; }; 
			hint "captivity changed while persistant"; 
        } else { hint "captivity changed, not persistant"; }; 
        _x setCaptive _captive; 
		
    }; 
} count _type; 
//} count allUnits; 

//hint "damage is on"; 

//this && ({ ((getPosATL _x) select 2) < 5 } count thisList > 0) && ({_x distance thisTrigger < 300} count playableUnits > 0)
