/* fn_artyBarrage.sqf
*  Author: PapaReap
*  function name: pr_fnc_artyBarrage
*  ver 1.0 2016-08-13
*
*  Arguments:
*  0: <EXPLOSIVE TYPE>       e.g. - (REQUIRED) "Large", "Medium", "Small", "Smoke", "Flare", "Apers", "ApersB", "ApersT", "AT", "Slam", "WaterM", "WaterB", "WaterP", "" (string)
*  0: <OPT EXPLOSIVE ARRAY>
*     0: <EXPLOSIVE TYPE>    e.g. - (REQUIRED) see: above
*     1: <ROUND COUNT>       e.g. - total amount of rounds to be used (scalar)
*     2: <SLEEP ROUNDS>      e.g. - amount of time to sleep between rounds (scalar)
*     3: <RANDOM SLEEP>      e.g. - add random amount of time to sleep (scalar)
*
*  1: <AREA>                 e.g. - (REQUIRED) "marker1" or Trigger name, area can be ellipse or rectangle
*  1: <OPT AREA ARRAY>
*     0: <AREA>              e.g. - (REQUIRED) see: above
*     1: <RANDOM AREA>       e.g. - true will use a random place in area, false will use center position (bool)
*
*  2: <OPT DROP HEIGHT>      e.g. - height to drop artillery rounds (scalar) - default 200
*
*  3: <OPT EXPLODE IN AIR>   e.g. - true will explode artillery at selected height (bool) - default false
*
*  4: <OPT MAX TIME>         e.g. - 300 will stop barrage after 300 sec (scalar) - default 0 (off)
*
*  EXAMPLES:
*  [["medium", 10, 3, 5], ["explosionZone", true]] remoteExec ["pr_fnc_artyBarrage", 0, false];
*  [["medium", 20, 5, 10], ["area1", true]] remoteExec ["pr_fnc_artyBarrage", 0, false];
*  [["medium"], ["openWall"]] remoteExec ["pr_fnc_artyBarrage", 0, false];
*  ["Large", "exp_6"] remoteExec ["pr_fnc_artyBarrage", 0, false];
*  [["smoke", 30, 3, 7], ["smokeScreen", true]] remoteExec ["pr_fnc_artyBarrage", 0, false];
*  [["smoke", 30, 3, 7], ["smokeScreen", true],[],"",300] remoteExec ["pr_fnc_artyBarrage", 0, false];
*  [["flare", 40, 2, 6], ["flares", true]] remoteExec ["pr_fnc_artyBarrage", 0, false];
*
*/

if !(isServer) exitWith {};

_pos = [];
_rCount = 1;
_sleep = 5;
_randomSleep = 0;

_random = false;
_height = 1200;
_airExp = false;

_maxTime = 0;
_timerUp = false;

_fallSpeed = -250;
_shellSound = false;
_mine = false;

_explosive = (_this select 0) select 0;
if (count (_this select 0) > 1) then { _rCount = (_this select 0) select 1; };
if (count (_this select 0) > 2) then { _sleep = (_this select 0) select 2; };
if (count (_this select 0) > 3) then { _randomSleep = (_this select 0) select 3; };

_area = (_this select 1) select 0;
if (count (_this select 1) > 1) then { _random = (_this select 1) select 1; };

if (count _this > 2) then { _height = _this select 2; };
if (count _this > 3) then { _airExp = _this select 3; };
if (count _this > 4) then { _maxTime = _this select 4; };

if (_explosive == "Large") then {
    _explosive = "Bo_GBU12_LGB_MI10";
    _shellSound = true;
    if (_airExp) then { _explosive = "HelicopterExploSmall"; _shellSound = false; };
} else {
    if (_explosive == "Medium") then {
        _explosive = "Sh_120mm_HE";
        _shellSound = true;
        if (_airExp) then { _explosive = "HelicopterExploSmall"; _shellSound = false; };
    } else {
        if (_explosive == "Small") then {
            _explosive = "SmallSecondary";
            if !(_airExp) then { _height = 0; };
        } else {
            if (_explosive == "Smoke") then {
                _explosive = "SmokeShellArty";
                _height = 0;
            } else {
                if (_explosive == "Flare") then {
                    _explosive = "F_40mm_white";
                    _height = 160 + (random 15);
                    _fallSpeed = -3 + (random -3);
                } else {
                    if (_explosive == "Apers") then {
                        _explosive = "APERSMine";
                        _height = 0;
                        _mine = true;
                    } else {
                        if (_explosive == "ApersB") then {
                            _explosive = "APERSBoundingMine";
                            _height = 0;
                            _mine = true;
                        } else {
                            if (_explosive == "ApersT") then {
                                _explosive = "APERSTripMine";
                                _height = 0;
                                _mine = true;
                            } else {
                                if (_explosive == "AT") then {
                                    _explosive = "ATMine";
                                    _height = 0;
                                    _mine = true;
                                } else {
                                    if (_explosive == "Slam") then {
                                        _explosive = "SLAMDirectionalMine";
                                        _height = 0;
                                        _mine = true;
                                    } else {
                                        if (_explosive == "WaterM") then {
                                            _explosive = "UnderwaterMine";
                                            _height = 0;
                                            _mine = true;
                                        } else {
                                            if (_explosive == "WaterB") then {
                                                _explosive = "UnderwaterMineAB";
                                                _height = 0;
                                                _mine = true;
                                            } else {
                                                if (_explosive == "WaterP") then {
                                                    _explosive = "UnderwaterMinePDM";
                                                    _height = 0;
                                                    _mine = true;
                                                };
                                            };
                                        };
                                    };
                                };
                            };
                        };
                    };
                };
            };
        };
    };
};

if (_maxTime > 0) then {
    _serverTime = floor(serverTime);
    _totalTime = _serverTime + _maxTime;
};

_i = -1; 
while { _i = _i + 1; ((_i < _rCount) || (_timerUp)) } do {
    if (_maxTime > 0) then {
        if (floor(serverTime) > _totalTime) then {
            _timerUp = true;
        };
    };

    if (_random) then {
        _pos = [_area] call pr_fnc_getPosInArea;
    } else {
        _pos = getMarkerPos _area;
    };

    _posX =  _pos select 0;
    _posY =  _pos select 1;
    _posZ =  _pos select 2;

    if (_mine) then {
        _blast = createMine [_explosive, [_posX, _posY, _posZ], [], 0];
        _blast setDir (random 360);
        if (_explosive == "UnderwaterMine") then {
            _blast spawn {
                _blast = _this;
                sleep 0.1;
                _height = 0;
                _hAdj = 0;
                _minWater = -2.0;
                if (getTerrainHeightASL position _blast < _minWater) then {
                    _hAdj = 46.4 + random 1.6;
                    _blast setPosASL [getPosASL _blast select 0, getPosASL _blast select 1, _height - _hAdj];
                } else {
                    _hAdj = 45.4459;
                    _blast setPosATL [getPosATL _blast select 0, getPosATL _blast select 1, _height - _hAdj];
                };
            };
        } else {
            if (_explosive == "UnderwaterMineAB") then {
                _blast spawn {
                    _blast = _this;
                    sleep 3;
                    _height = getTerrainHeightASL position _blast;
                    _blast setPosASL [getPosASL _blast select 0, getPosASL _blast select 1, _height];
                };
            } else {
                if (_explosive == "UnderwaterMinePDM") then {
                    _blast spawn {
                        _blast = _this;
                        sleep 3;
                        _height = getTerrainHeightASL position _blast;
                        _blast setPosASL [getPosASL _blast select 0, getPosASL _blast select 1, _height];
                    };
                };
            };
        };
    } else {
        _blast = _explosive createVehicle [_posX, _posY, _posZ + _height]; _blast setVectorDirandUp [[0,0,-1],[0.1,0.1,1]]; _blast setVelocity [0, 0, _fallSpeed];
    };

    _blastPos = [];
    _blastPos = [_pos select 0,_pos select 1,_pos select 2];
    _closest = [_blastPos, 100] call pr_fnc_nearestPlayer;

    if ((_closest distance _blastPos < 100) && (_shellSound)) then {
        _spot = "Land_HelipadEmpty_F" createVehicle [_posX, _posY, _posZ];
        [_blast, _spot] spawn {
            params ["_blast","_spot"];
            _typeSound = ["shell1", "shell2", "shell1"] call BIS_fnc_selectRandom;
            [_spot, _typeSound] remoteExec ["say3D"];

            waitUntil { (isNull _blast) };
            deleteVehicle _spot;
        };
    };
    _newSleep = _sleep + random _randomSleep;
    sleep _newSleep;
};


