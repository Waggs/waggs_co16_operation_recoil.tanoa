

//execVM "scripts\PR\scripts\misc\fn_razorWire.sqf";

_unit = player;

_distanceBoundBox = {
    params ["_unit","_obj"];
    _uPos = _obj worldToModel (getPos _unit);
    _objBox = boundingBoxReal _obj;
    _pt = [0, 0, 0];
    {
        if (_x < (_objBox select 0 select _forEachIndex)) then {
            _pt set [_forEachIndex, (_objBox select 0 select _forEachIndex) - _x];
        } else {
            if ((_objBox select 1 select _forEachIndex) < _x) then {
                _pt set [_forEachIndex, _x - (_objBox select 1 select _forEachIndex)];
            }
        }
    } forEach _uPos;
    _pt distance [0, 0, 0]
};

while { true } do {
    _wire = nearestObject [_unit, "Land_Razorwire_F"];
    if (!(isNull _wire) && (alive _wire)) then {
        _dist = [_unit, _wire] call _distanceBoundBox;

        if (_dist <= 0.70) then {
            _getDamage = damage _unit;
            _damage = (_getDamage + 0.03) min 0.98;
            _unit setDamage _damage;
        };
    };
    sleep 1;
};