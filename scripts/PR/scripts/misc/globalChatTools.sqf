//--- globalChatTools.sqf 
//--- in your init.sqf 
//--- [] execVM "scripts\PR\scripts\misc\globalChatTools.sqf"; 
//--- example
/*  addaction.sqf
whoEverActivated = (_this select 1)
_sidechatHint = format ["%1 has Collected the rally point.", name whoEverActivated];
PAPABEAR sideChat _sidechatHint;
PAPABEAR_SideChat = _sidechatHint; publicVariable "PAPABEAR_SideChat";
*/

//ENABLE GLOBAL SIDECHAT
PAPABEAR = [west, "HQ"]; 
FIREFLY = [West, "AIRBASE"]; 

"PAPABEAR_SideChat" addPublicVariableEventHandler { 
    private ["_chat"]; 
    _chat = _this select 1; 
    PAPABEAR sideChat _chat; 
}; 

"FIREFLY_SideChat" addPublicVariableEventHandler { 
    private ["_chat"]; 
    _chat = _this select 1; 
    FIREFLY sideChat _chat; 
}; 

"logic_HQ_SideChat" addPublicVariableEventHandler { 
    private ["_chat"]; 
    _chat = _this select 1; 
    logic_HQ sideChat _chat; 
}; 

"logic_firefly_SideChat" addPublicVariableEventHandler { 
    private ["_chat"]; 
    _chat = _this select 1; 
    logic_firefly sideChat _chat; 
}; 

"logic_leader_SideChat" addPublicVariableEventHandler { 
    private ["_chat", "_unit"]; 
    _chat = _this select 1; 
    logic_Leader sideChat _chat; 
}; 

"logic_artillery_SideChat" addPublicVariableEventHandler { 
    private ["_chat", "_artillery"]; 
    _chat = _this select 1; 
    logic_artillery sideChat _chat; 
}; 

"logic_pigeon_SideChat" addPublicVariableEventHandler { 
    private ["_chat", "_artillery"]; 
    _chat = _this select 1; 
    logic_pigeon sideChat _chat; 
}; 

if (getMarkerColor "gl" == "") then { 
    _marker = createMarker ["gl",[0,0,0]]; 
}; 

if (!isNil "logic_artillery") then { 
    logic_artillery setPos getMarkerPos "gl"; 
    noHCSwap = noHCSwap + [logic_artillery]; publicVariable "noHCSwap"; 
}; 

if (!isNil "logic_leader") then { 
    logic_leader setPos getMarkerPos "gl"; 
    noHCSwap = noHCSwap + [logic_leader]; publicVariable "noHCSwap"; 
}; 

if (!isNil "logic_HQ") then { 
    logic_HQ setPos getMarkerPos "gl"; 
    noHCSwap = noHCSwap + [logic_HQ]; publicVariable "noHCSwap"; 
}; 

if (!isNil "logic_firefly") then { 
    logic_firefly setPos getMarkerPos "gl"; 
    noHCSwap = noHCSwap + [logic_firefly]; publicVariable "noHCSwap"; 
}; 

if (!isNil "logic_pigeon") then { 
    logic_pigeon setPos getMarkerPos "gl"; 
    noHCSwap = noHCSwap + [logic_pigeon]; publicVariable "noHCSwap"; 
}; 
