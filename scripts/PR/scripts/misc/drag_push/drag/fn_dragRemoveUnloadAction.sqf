/* fn_dragRemoveUnloadAction.sqf
*  Author: PapaReap
*  Function name: pr_fnc_dragRemoveUnloadAction

*  Remove unload action
*/

_vehicle = (_this select 0);
_vehUnloadId = (_this select 1);
_vehicle removeAction _vehUnloadId;
