/* fn_dragMoveDragged.sqf
*  Author: PapaReap
*  Function name: pr_fnc_dragMoveDragged

*  Keep dragged unit moving with player
*/

_unit = (_this select 0);
_player = (_this select 1);
_pos = _player modelToWorld [0,1,0];
_unit setPos _pos;
_unit setDir 180;
_unit switchMove "AinjPpneMrunSnonWnonDb";
