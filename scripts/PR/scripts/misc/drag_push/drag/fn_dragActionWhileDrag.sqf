/* fn_dragActionWhileDrag.sqf
*  Author: PapaReap
*  Function name: pr_fnc_dragActionWhileDrag

*  While dragging, add action to release or load
*/

_unit = _this select 0;
_player = _this select 1;

//player playAction "grabDrag";
//player forceWalk true;
_player playAction "grabDrag";
_player forceWalk true;

_unitID = _unit getVariable "pr_dragID";
//[_unit, vehicle _player, _unitID] remoteExec ["pr_fnc_attachWhileDragging", 0, true];
[_unit, vehicle _player, _unitID] remoteExec ["pr_fnc_dragAttachWhileDragging", 0, true];
_unit attachTo [_player, [0,1,0]];

_releaseID = _player addAction [[_unit, "Release"] call pr_fnc_dragActionText, { call pr_fnc_dragRelease }, _unit, 6];
_player setVariable ["pr_releaseID", _releaseID];

_loadCond = "(
    (_target distance cursorTarget < 6)
    && (((cursorTarget isKindOf 'LandVehicle') || (cursorTarget isKindOf 'Air') || (cursorTarget isKindOf 'Ship')) && (cursorTarget emptyPositions 'cargo' > 0))
)";
_loadID = _player addAction [[_unit, "Load"] call pr_fnc_dragActionText, { call pr_fnc_dragLoad }, _unit, 6, false, false, "", _loadCond];
_player setVariable ["pr_loadID", _loadID];

//--- prevent player from entering vehicle while dragging //*********** issues here
_player spawn { // needs testing on multiplayer
    private ["_attached","_player","_releaseID","_unit","_vehicle"];
    _player = _this;
    //_lockedVeh = [];
    while { (count (attachedObjects _player) > 0) } do { // need to add something to release if dead or unconscious
        if !(vehicle _player == _player) then {
            _attached = attachedObjects _player;
            _unit = _attached select 0;
            _vehicle = vehicle _player;
            _releaseID = _player getVariable "pr_releaseID";
            _unit = _attached select 0;
            [_player, _vehicle, _releaseID, _unit] call pr_fnc_dragRelease;
            hint "dragged detached";
        };
    };
};
