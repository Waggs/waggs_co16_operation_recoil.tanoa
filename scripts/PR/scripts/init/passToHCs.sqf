//add execVM "passToHCs.sqf"; to you init.sqf should be placed high i.e. a priority initialisation 
//name your HC's: HC,HC2,HC3  can run with two HC's if preferred. 

if (prHCs == 0) exitWith {}; 
if (!isServer) exitWith {}; 

diag_log "passToHCs: Started"; 

waitUntil { !isNil "HC" }; 
waitUntil { !isNull HC }; 
//waitUntil { sleep 10; (!(isNil Name HC) || !(isNil Name HC2)) }; 

_HC_ID = -1;  // Will become the Client ID of HC
_HC2_ID = -1; // Will become the Client ID of HC2
_HC3_ID = -1; // Will become the Client ID of HC3

sleep 50; 

rebalanceTimer = 60;  // Rebalance sleep timer in seconds
//cleanUpThreshold = 30; // Threshold of number of dead bodies + destroyed vehicles before forcing a clean up

diag_log format ["passToHCs: First pass will begin in %1 seconds", rebalanceTimer]; 

while { true } do {
    // Rebalance every rebalanceTimer seconds to avoid hammering the server
    sleep rebalanceTimer;

    // Do not enable load balancing unless more than one HC is present
    // Leave this variable false, we'll enable it automatically under the right conditions  
    _loadBalance = false; 

    // Get HC Client ID else set variables to null
    try {
        _HC_ID = owner HC;

        if (_HC_ID > 2) then {
            diag_log format ["passToHCs: Found HC with Client ID %1", _HC_ID];
        } else { 
            diag_log "passToHCs: [WARN] HC disconnected";

            HC = objNull;
            _HC_ID = -1;
        };
    } catch { diag_log format ["passToHCs: [ERROR] [HC] %1", _exception]; HC = objNull; _HC_ID = -1; };

    // Get HC2 Client ID else set variables to null
    if (!isNil "HC2") then {
        try {
            _HC2_ID = owner HC2;

            if (_HC2_ID > 2) then {
                diag_log format ["passToHCs: Found HC2 with Client ID %1", _HC2_ID];
            } else { 
                diag_log "passToHCs: [WARN] HC2 disconnected";

                HC2 = objNull;
                _HC2_ID = -1;
            };
        } catch { diag_log format ["passToHCs: [ERROR] [HC2] %1", _exception]; HC2 = objNull; _HC2_ID = -1; };
    };

    // Get HC3 Client ID else set variables to null
    if (!isNil "HC3") then {
        try {
            _HC3_ID = owner HC3;

            if (_HC3_ID > 2) then {
                diag_log format ["passToHCs: Found HC2 with Client ID %1", _HC3_ID];
            } else { 
                diag_log "passToHCs: [WARN] HC3 disconnected";

                HC3 = objNull;
                _HC3_ID = -1;
            };
        } catch { diag_log format ["passToHCs: [ERROR] [HC3] %1", _exception]; HC3 = objNull; _HC3_ID = -1; };
    };

    // If no HCs present, wait for HC to rejoin
    if ( (isNil "HC") && (isNil "HC2") && (isNil "HC3") ) then { waitUntil { (!(isNull "HC") || !(isNull "HC2") || !(isNull "HC3")) }; };  

    // Check to auto enable Round-Robin load balancing strategy
    if ( (!isNil "HC" && !isNil "HC2") || (!isNil "HC" && !isNil "HC3") || (!isNil "HC2" && !isNil "HC3") ) then { _loadBalance = true; };

    if ( _loadBalance ) then {
        diag_log "passToHCs: Starting load-balanced transfer of AI groups to HCs";
    } else {
        // No load balancing
        diag_log "passToHCs: Starting transfer of AI groups to HC";
    };

    // Determine first HC to start with
    _currentHC = 0;

    if (!isNull HC) then { _currentHC = 1; } else { 
        if (!isNull HC2) then { _currentHC = 2; } else { _currentHC = 3; };
    };

    // Gather up the dead 
    deadArray = []; 
    {
        deadArray = deadArray + [_x];
    } forEach allDead; 
    publicVariable "deadArray"; 
    _deadCount = (count deadArray); 
    diag_log format ["passToHCs: Dead count on map = %1", _deadCount]; 

    _preCount1 = 0; 
    _postCount1 = 0; 
    _preCount1 = count noHCSwap; 
    noHCSwap = noHCSwap - deadArray; 
    noHCSwap = noHCSwap - [objNull]; 
    _postCount1 = _preCount1 - (count noHCSwap); 
    diag_log format ["passToHCs: Clean-up of noHCSwap, removed objects = %1", _postCount1]; 

    _preCount2 = 0; 
    _postCount2 = 0; 
    _preCount2 = count hcUnits; 
    hcUnits = hcUnits - deadArray; 
    hcUnits = hcUnits - [objNull]; 
    _postCount2 = _preCount2 - (count hcUnits); 
    diag_log format ["passToHCs: Clean-up of HC, removed objects = %1", _postCount2]; 

    _preCount3 = 0; 
    _postCount3 = 0; 
    _preCount3 = count hc2Units; 
    hc2Units = hc2Units - deadArray; 
    hc2Units = hc2Units - [objNull]; 
    _postCount3 = _preCount3 - (count hc2Units); 
    diag_log format ["passToHCs: Clean-up of HC2, removed objects = %1", _postCount3]; 

    _preCount4 = 0; 
    _postCount4 = 0; 
    _preCount4 = count serverUnits; 
    serverUnits = serverUnits - deadArray; 
    serverUnits = serverUnits - [objNull]; 
    _postCount4 = _preCount4 - (count serverUnits); 
    diag_log format ["passToHCs: Clean-up of Server, removed objects = %1", _postCount4]; 

    // Pass the AI
    _numTransfered = 0;
    {
        _swap = true;

        // If a player is in this group, don't swap to an HC
        { if (isPlayer _x) then { _swap = false; }; } forEach (units _x); 
        { if (_x in noHCSwap) then { _swap = false; }; } forEach (units _x); 
        { if (_x in hcUnits) then { _swap = false; }; } forEach (units _x); 
        { if (_x in hc2Units) then { _swap = false; }; } forEach (units _x); 
        { if (_x in serverUnits) then { _swap = false; }; } forEach (units _x); 

        // If load balance enabled, round robin between the HCs - else pass all to HC obj
        if ( _swap ) then {
            _rc = false;

            if ( _loadBalance ) then {
                switch (_currentHC) do {
                    case 1: { _rc = _x setGroupOwner _HC_ID; if (!isNil "HC2") then { _currentHC = 2; } else { _currentHC = 3; }; };
                    case 2: { _rc = _x setGroupOwner _HC2_ID; if (!isNil "HC3") then { _currentHC = 3; } else { _currentHC = 1; }; };
                    case 3: { _rc = _x setGroupOwner _HC3_ID; if (!isNil "HC") then { _currentHC = 1; } else { _currentHC = 2; }; };
                    default { diag_log format ["passToHCs: [ERROR] No Valid HC to pass to.  _currentHC = %1", _currentHC]; };
                };
            } else {
                switch (_currentHC) do {
                    case 1: { _rc = _x setGroupOwner _HC_ID; };
                    case 2: { _rc = _x setGroupOwner _HC2_ID; };
                    case 3: { _rc = _x setGroupOwner _HC3_ID; };
                    default { diag_log format ["passToHCs: [ERROR] No Valid HC to pass to.  _currentHC = %1", _currentHC]; };
                }; 
            }; 

            // If the transfer was successful, count it for accounting and diagnostic information
            if ( _rc ) then { _numTransfered = _numTransfered + 1; };
        };
    } forEach (allGroups); 

    publicVariable "noHCSwap"; 
    publicVariable "hcUnits"; 
    publicVariable "hc2Units"; 
    publicVariable "serverUnits"; 

    if (_numTransfered > 0) then {
        // More accounting and diagnostic information

        diag_log format ["passToHCs: Transfered %1 AI groups to HC(s)", _numTransfered];

        _numHC = 0;
        _numHC2 = 0;
        _numHC3 = 0;

        {
            switch (owner ((units _x) select 0)) do {
                case _HC_ID: { _numHC = _numHC + 1; };
                case _HC2_ID: { _numHC2 = _numHC2 + 1; };
                case _HC3_ID: { _numHC3 = _numHC3+ 1; };
            };
        } forEach (allGroups);

        if (_numHC > 0) then { diag_log format ["passToHCs: %1 AI groups currently on HC", _numHC]; };
        if (_numHC2 > 0) then { diag_log format ["passToHCs: %1 AI groups currently on HC2", _numHC2]; };
        if (_numHC3 > 0) then { diag_log format ["passToHCs: %1 AI groups currently on HC3", _numHC3]; };

        diag_log format ["passToHCs: %1 AI groups total across all HC(s)", (_numHC + _numHC2 + _numHC3)];
    } else {
        diag_log "passToHCs: No rebalance or transfers required this round";
    };

/*
    // Force clean up dead bodies and destroyed vehicles
    if (count allDead > cleanUpThreshold) then {
        _numDeleted = 0;
        {
            deleteVehicle _x;
            _numDeleted = _numDeleted + 1;
        } forEach allDead;

        diag_log format ["passToHCs: Cleaned up %1 dead bodies/destroyed vehicles", _numDeleted];
    }; 
*/
};
