/*  fn_optics.sqf
    Add to your init.sqf: [] execVM "scripts\PR\scripts\init\fn_optics.sqf";
    Use with ParamsArray.ext
    1.0: 2016-02-16 initial release
*/

waitUntil { !(isNil "unitArrayCompiled") };
waitUntil { time > 5 };

private ["_optics","_unit","_sleep","_removeOptic","_optic","_addOptic","_side"];

_optics = ["missionOptics", 0] call BIS_fnc_getParamValue;
if (_optics == 0) exitWith {};  //"Optics - Default"

_sleep = 5;

// ADD SIDE OPTICS
if ((prWeapons == 5) || (prWeapons == 6) || (prWeapons == 7) || (prWeapons == 8) && (masOn)) then { // Massi - UK, UK Winter, US Marines, US Marines Winter

};

_removeOptic = {
    _unit = _this select 0;
    _optic = primaryWeaponItems _unit select 2;
    _unit removePrimaryWeaponItem _optic;
};

_addOptic = {
    _unit = _this select 0;
    _side = side _unit;
    if (_side == west) then {
        _unit addprimaryweaponitem "optic_Aco";
    } else {
        if (_side == east) then {
            _unit addprimaryweaponitem "optic_ACO_grn";
        } else {
            if (_side == independent) then {
                _unit addprimaryweaponitem "optic_Aco";
            };
        };
    };
};

if ((_optics == 1) || (_optics == 2)) then {
    if (_optics == 1) then {  // "Optics - Dot (Mission Start)"
        {
            if ((primaryWeaponItems _x select 2) in pr_opticsArray) then {
                [_x] call _removeOptic;
                [_x] call _addOptic;
            };
        } forEach (allUnits);
    } else {
        while { true } do {  // "Optics - Dot (Permanent)"
            {
                if ((primaryWeaponItems _x select 2) in pr_opticsArray) then {
                    [_x] call _removeOptic;
                    [_x] call _addOptic;
                };
            } forEach (allUnits);
            sleep _sleep;
        };
    };
} else {
    if (_optics == 3) then {  // "Optics - Open Site (Mission Start)"
        {
            if !((primaryWeaponItems _x select 2) == "") then {
                [_x] call _removeOptic;
            };
        } forEach (allUnits);
    } else {
        while { true } do {  // "Optics - Open Site (Permanent)"
            {
                if !((primaryWeaponItems _x select 2) == "") then {
                    [_x] call _removeOptic;
                };
            } forEach (allUnits);
            sleep _sleep;
         }; 
    };
};
