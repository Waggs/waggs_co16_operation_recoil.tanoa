//--- [] execVM "scripts\PR\scripts\init\areaMarkers.sqf; 

if !(isServer) exitWith {}; 

if (isNil "areaMarkers") then { 
    areaMarkers = true; publicVariable "areaMarkers"; 
    for "_x" from 1 to 200 do { 
        format ["AREA%1", _x] setMarkerAlpha 0; 
    }; 
}; 

diag_log format ["*PR* areaMarkers complete"]; 
