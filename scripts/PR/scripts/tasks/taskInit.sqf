
waitUntil { (!isnil "taskHold") }; 

if (aliveOn) then { 
    if ((isServer) && (isDedicated)) then { 
        waitUntil { (!isnil "alive_sys_player") }; 
        taskHold = true; publicVariable "taskHold"; 
    } else { 
        if ((isServer) && (hasInterface)) then { 
            taskHold = true; publicVariable "taskHold"; 
        }; 
    }; 
} else { taskHold = true; publicVariable "taskHold"; }; 

waitUntil { (taskHold) }; 

if !(isNil "pr_task1") then { 
    if !(isTask1complete == "true") then { 
        [pr_task1, (pr_task1 select 6)] call fnc_tasker; 
    } else { 
        if (isTask1complete == "true") then { 
            [pr_task1, "created"] call fnc_tasker; 
            [pr_task1, "succeeded"] call fnc_tasker; tsk1 = true; publicVariable "tsk1"; 
        }; 
    }; 
}; 

if !(isNil "pr_task2") then { 
    if (!(isTask2complete == "true") && ((tsk1) || (!isNil "tsk2assign"))) then { 
        [pr_task2, (pr_task2 select 6)] call fnc_tasker; 
    } else { 
        if ((isTask2complete == "true") && ((tsk1) || (!isNil "tsk2assign"))) then { 
            [pr_task2, "created"] call fnc_tasker; 
            [pr_task2, "succeeded"] call fnc_tasker; tsk2 = true; publicVariable "tsk2"; 
        }; 
    }; 
}; 

if !(isNil "pr_task3") then { 
    if (!(isTask3complete == "true") && ((tsk2) || (!isNil "tsk3assign"))) then { 
        [pr_task3, (pr_task3 select 6)] call fnc_tasker; 
    } else { 
        if ((isTask3complete == "true") && ((tsk2) || (!isNil "tsk3assign"))) then { 
            [pr_task3, "created"] call fnc_tasker; 
            [pr_task3, "succeeded"] call fnc_tasker; tsk3 = true; publicVariable "tsk3"; 
        }; 
    }; 
}; 

if !(isNil "pr_task4") then { 
    if (!(isTask4complete == "true") && ((tsk3) || (!isNil "tsk4assign"))) then { 
        [pr_task4, (pr_task4 select 6)] call fnc_tasker; 
    } else { 
        if ((isTask4complete == "true") && ((tsk3) || (!isNil "tsk4assign"))) then { 
            [pr_task4, "created"] call fnc_tasker; 
            [pr_task4, "succeeded"] call fnc_tasker; tsk4 = true; publicVariable "tsk4"; 
        }; 
    }; 
}; 

if !(isNil "pr_task5") then { 
    if (!(isTask5complete == "true") && ((tsk4) || (!isNil "tsk5assign"))) then { 
        [pr_task5, (pr_task5 select 6)] call fnc_tasker; 
    } else { 
        if ((isTask5complete == "true") && ((tsk4) || (!isNil "tsk5assign"))) then { 
            [pr_task5, "created"] call fnc_tasker; 
            [pr_task5, "succeeded"] call fnc_tasker; tsk5 = true; publicVariable "tsk5"; 
        }; 
    }; 
}; 

if !(isNil "pr_task6") then { 
    if (!(isTask6complete == "true") && ((tsk5) || (!isNil "tsk6assign"))) then { 
        [pr_task6, (pr_task6 select 6)] call fnc_tasker; 
    } else { 
        if ((isTask6complete == "true") && ((tsk5) || (!isNil "tsk6assign"))) then { 
            [pr_task6, "created"] call fnc_tasker; 
            [pr_task6, "succeeded"] call fnc_tasker; tsk6 = true; publicVariable "tsk6"; 
        }; 
    }; 
}; 

if !(isNil "pr_task7") then { 
    if (!(isTask7complete == "true") && ((tsk6) || (!isNil "tsk7assign"))) then { 
        [pr_task7, (pr_task7 select 6)] call fnc_tasker; 
    } else { 
        if ((isTask7complete == "true") && ((tsk6) || (!isNil "tsk7assign"))) then { 
            [pr_task7, "created"] call fnc_tasker; 
            [pr_task7, "succeeded"] call fnc_tasker; tsk7 = true; publicVariable "tsk7"; 
        }; 
    }; 
}; 

if !(isNil "pr_task8") then { 
    if (!(isTask8complete == "true") && ((tsk7) || (!isNil "tsk8assign"))) then { 
        [pr_task8, (pr_task8 select 6)] call fnc_tasker; 
    } else { 
        if ((isTask8complete == "true") && ((tsk7) || (!isNil "tsk8assign"))) then { 
            [pr_task8, "created"] call fnc_tasker; 
            [pr_task8, "succeeded"] call fnc_tasker; tsk8 = true; publicVariable "tsk8"; 
        }; 
    }; 
}; 

if !(isNil "pr_task9") then { 
    if (!(isTask9complete == "true") && ((tsk8) || (!isNil "tsk9assign"))) then { 
        [pr_task9, (pr_task9 select 6)] call fnc_tasker; 
    } else { 
        if ((isTask9complete == "true") && ((tsk8) || (!isNil "tsk9assign"))) then { 
            [pr_task9, "created"] call fnc_tasker; 
            [pr_task9, "succeeded"] call fnc_tasker; tsk9 = true; publicVariable "tsk9"; 
        }; 
    }; 
}; 

if !(isNil "pr_task10") then { 
    if (!(isTask10complete == "true") && ((tsk9) || (!isNil "tsk10assign"))) then { 
        [pr_task10, (pr_task10 select 6)] call fnc_tasker; 
    } else { 
        if ((isTask10complete == "true") && ((tsk9) || (!isNil "tsk10assign"))) then { 
            [pr_task10, "created"] call fnc_tasker; 
            [pr_task10, "succeeded"] call fnc_tasker; tsk10 = true; publicVariable "tsk10"; 
        }; 
    }; 
}; 

if !(isNil "pr_task11") then { 
    if (!(isTask11complete == "true") && ((tsk10) || (!isNil "tsk11assign"))) then { 
        [pr_task11, (pr_task11 select 6)] call fnc_tasker; 
    } else { 
        if ((isTask11complete == "true") && ((tsk10) || (!isNil "tsk11assign"))) then { 
            [pr_task11, "created"] call fnc_tasker; 
            [pr_task11, "succeeded"] call fnc_tasker; tsk11 = true; publicVariable "tsk11"; 
        }; 
    }; 
}; 

if !(isNil "pr_task12") then { 
    if (!(isTask12complete == "true") && ((tsk11) || (!isNil "tsk12assign"))) then { 
        [pr_task12, (pr_task12 select 6)] call fnc_tasker; 
    } else { 
        if ((isTask12complete == "true") && ((tsk11) || (!isNil "tsk12assign"))) then { 
            [pr_task12, "created"] call fnc_tasker; 
            [pr_task12, "succeeded"] call fnc_tasker; tsk12 = true; publicVariable "tsk12"; 
        }; 
    }; 
}; 

if !(isNil "pr_task13") then { 
    if (!(isTask13complete == "true") && ((tsk12) || (!isNil "tsk13assign"))) then { 
        [pr_task13, (pr_task13 select 6)] call fnc_tasker; 
    } else { 
        if ((isTask13complete == "true") && ((tsk12) || (!isNil "tsk13assign"))) then { 
            [pr_task13, "created"] call fnc_tasker; 
            [pr_task13, "succeeded"] call fnc_tasker; tsk13 = true; publicVariable "tsk13"; 
        }; 
    }; 
}; 

if !(isNil "pr_task14") then { 
    if (!(isTask14complete == "true") && ((tsk13) || (!isNil "tsk14assign"))) then { 
        [pr_task14, (pr_task14 select 6)] call fnc_tasker; 
    } else { 
        if ((isTask14complete == "true") && ((tsk13) || (!isNil "tsk14assign"))) then { 
            [pr_task14, "created"] call fnc_tasker; 
            [pr_task14, "succeeded"] call fnc_tasker; tsk14 = true; publicVariable "tsk14"; 
        }; 
    }; 
}; 

if !(isNil "pr_task15") then { 
    if (!(isTask15complete == "true") && ((tsk14) || (!isNil "tsk15assign"))) then { 
        [pr_task15, (pr_task15 select 6)] call fnc_tasker; 
    } else { 
        if ((isTask15complete == "true") && ((tsk14) || (!isNil "tsk15assign"))) then { 
            [pr_task15, "created"] call fnc_tasker; 
            [pr_task15, "succeeded"] call fnc_tasker; tsk15 = true; publicVariable "tsk15"; 
        }; 
    }; 
}; 

if !(isNil "pr_task16") then { 
    if (!(isTask16complete == "true") && ((tsk15) || (!isNil "tsk16assign"))) then { 
        [pr_task16, (pr_task16 select 6)] call fnc_tasker; 
    } else { 
        if ((isTask16complete == "true") && ((tsk15) || (!isNil "tsk16assign"))) then { 
            [pr_task16, "created"] call fnc_tasker; 
            [pr_task16, "succeeded"] call fnc_tasker; tsk16 = true; publicVariable "tsk16"; 
        }; 
    }; 
}; 

if !(isNil "pr_task17") then { 
    if (!(isTask17complete == "true") && ((tsk16) || (!isNil "tsk17assign"))) then { 
        [pr_task17, (pr_task17 select 6)] call fnc_tasker; 
    } else { 
        if ((isTask17complete == "true") && ((tsk16) || (!isNil "tsk17assign"))) then { 
            [pr_task17, "created"] call fnc_tasker; 
            [pr_task17, "succeeded"] call fnc_tasker; tsk17 = true; publicVariable "tsk17"; 
        }; 
    }; 
}; 

if !(isNil "pr_task18") then { 
    if (!(isTask18complete == "true") && ((tsk17) || (!isNil "tsk18assign"))) then { 
        [pr_task18, (pr_task18 select 6)] call fnc_tasker; 
    } else { 
        if ((isTask18complete == "true") && ((tsk17) || (!isNil "tsk18assign"))) then { 
            [pr_task18, "created"] call fnc_tasker; 
            [pr_task18, "succeeded"] call fnc_tasker; tsk18 = true; publicVariable "tsk18"; 
        }; 
    }; 
}; 

if !(isNil "pr_task19") then { 
    if (!(isTask19complete == "true") && ((tsk18) || (!isNil "tsk19assign"))) then { 
        [pr_task19, (pr_task19 select 6)] call fnc_tasker; 
    } else { 
        if ((isTask19complete == "true") && ((tsk18) || (!isNil "tsk19assign"))) then { 
            [pr_task19, "created"] call fnc_tasker; 
            [pr_task19, "succeeded"] call fnc_tasker; tsk19 = true; publicVariable "tsk19"; 
        }; 
    }; 
}; 

if !(isNil "pr_task20") then { 
    if (!(isTask20complete == "true") && ((tsk19) || (!isNil "tsk20assign"))) then { 
        [pr_task20, (pr_task20 select 6)] call fnc_tasker; 
    } else { 
        if ((isTask20complete == "true") && ((tsk19) || (!isNil "tsk20assign"))) then { 
            [pr_task20, "created"] call fnc_tasker; 
            [pr_task20, "succeeded"] call fnc_tasker; tsk20 = true; publicVariable "tsk20"; 
        }; 
    }; 
}; 
