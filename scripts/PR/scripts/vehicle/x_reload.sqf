// _nil = this addAction ["Resupply vehicle","custom_scripts\x_reload.sqf","",5,true,true,"","(vehicle _this != _this) && (driver (vehicle _this) == _this)"];

_object = vehicle (_this select 1);

if (isEngineOn _object) exitWith {_object vehicleChat "Engine must be off to resupply";};

_type = typeOf _object;

x_reload_time_factor = 25.00;

_object vehicleChat format ["Servicing %1... Please stand by...", _type];

_magazines = getArray(configFile >> "CfgVehicles" >> _type >> "magazines");

_ammoTrucks =   ["B_Truck_01_ammo_F"    , "O_Truck_02_Ammo_F"   , "I_Truck_02_Ammo_F"   ];  
_fuelTrucks =   ["B_Truck_01_fuel_F"    , "O_Truck_02_fuel_F"   , "I_Truck_02_fuel_F"   , "C_Van_01_fuel_F", "B_G_Van_01_fuel_F"];    
_repairTrucks = ["B_Truck_01_Repair_F"  , "O_Truck_02_box_F"    , "I_Truck_02_box_F"    ];    
_medicalTrucks= ["B_Truck_01_medical_F" , "O_Truck_02_medical_F", "I_Truck_02_medical_F"];
  
sleep x_reload_time_factor;
if (isEngineOn _object) exitWith {_object vehicleChat "Resupply stopped when engine started";};

_object vehicleChat "Repairing...";
_delay = damage _object * x_reload_time_factor;
_object setDamage 0;
sleep _delay;

if (isEngineOn _object) exitWith {_object vehicleChat "Resupply stopped when engine started";};

_object vehicleChat "Refueling...";
_fuel = fuel _object;
while {(_fuel < 1) && !(isEngineOn _object)} do {
    _fuel = _fuel + 0.01;
	_object setFuel _fuel;
	sleep (x_reload_time_factor/100);
};

if (isEngineOn _object) exitWith {_object vehicleChat "Resupply stopped when engine started";};

_type = typeOf _object;
_stocked = true;
switch true do
{
        case (_type in _ammoTrucks)   : {_object vehicleChat "Stocking ammo supplies..."; sleep ((1-(getAmmoCargo   _object))   * x_reload_time_factor); _object setAmmoCargo 1;  }; 
        case (_type in _fuelTrucks)   : {_object vehicleChat "Filling tanker...";         sleep ((1-(getFuelCargo   _object))   * x_reload_time_factor); _object setFuelCargo 1;  }; 
        case (_type in _repairTrucks) : {_object vehicleChat "Stock repair supplies...";  sleep ((1-(getRepairCargo _object)) * x_reload_time_factor); _object setRepairCargo 1;}; 
        case (_type in _medicalTrucks): 
        {                                                                                         
            // stock up on first aid kits
            _firstAidCount = 20;
            _addMedKits = 0;
            _items = getItemCargo _object;
            _loc = (_items select 0) find "FirstAidKit";
            if (_loc > -1) then
            {
                _count = ((_items select 1) select _loc);
                if ( _count < _firstAidCount) then
                {
                    _addMedKits = _firstAidCount - _count;               
                };
            }
            else
            {
                _addMedKits = _firstAidCount;
            };
            _object vehicleChat "Stocking medical supplies..."; sleep (_addMedKits/20) * x_reload_time_factor;
            if (_addMedKits > 0) then
            {
                if (_counter >= _delay) then {
                    _object addItemCargo ["FirstAidKit",_addMedKits];
                }
                else
                {
                    _increment = true;
                };
            };
        }; 
        default {_stocked = false;};
};

if (isEngineOn _object) exitWith {_object vehicleChat "Resupply stopped when engine started";};

_count = count (configFile >> "CfgVehicles" >> _type >> "Turrets");

// if (_count > 0) then {
	// for "_i" from 0 to (_count - 1) do {
		// scopeName "xx_reload2_xx";
		// _config = (configFile >> "CfgVehicles" >> _type >> "Turrets") select _i;
		// _magazines = getArray(_config >> "magazines");
		// _removed = [];
		// {
			// if (!(_x in _removed)) then {
				// _object removeMagazines _x;
				// _removed = _removed + [_x];
			// };
		// } forEach _magazines;
		// {
			// _object vehicleChat format ["Reloading %1", _x];
			// sleep x_reload_time_factor/2;
			// _object addMagazine _x;
			// sleep x_reload_time_factor/2;
		// } forEach _magazines;
		// _count_other = count (_config >> "Turrets");
		// if (_count_other > 0) then {
			// for "_i" from 0 to (_count_other - 1) do {
				// _config2 = (_config >> "Turrets") select _i;
				// _magazines = getArray(_config2 >> "magazines");
				// _removed = [];
				// {
					// if (!(_x in _removed)) then {
						// _object removeMagazines _x;
						// _removed = _removed + [_x];
					// };
				// } forEach _magazines;
				// {
					// _object vehicleChat format ["Reloading %1", _x]; 
					// sleep x_reload_time_factor/2;
					// _object addMagazine _x;
					// sleep x_reload_time_factor/2;
				// } forEach _magazines;
			// };
		// };
	// };
// };

if (_count > 0) then {
    _object vehicleChat "Restocking Ammo...";
    sleep x_reload_time_factor;
    _object setVehicleAmmo 1;	// Reload turrets / drivers magazine
};

_object vehicleChat format ["%1 is ready...", _type];

if (true) exitWith {};
