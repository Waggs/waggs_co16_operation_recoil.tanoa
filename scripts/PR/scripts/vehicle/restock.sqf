//--- restock.sqf 
//--- [[[_grp, "ammo, fuel or both", 120], "scripts\PR\scripts\vehicle\restock.sqf"], "BIS_fnc_execVM", leader _grp, false] call BIS_fnc_MP; 
//--- [[[_grp, "fuel", 120],"scripts\PR\scripts\vehicle\restock.sqf"], "BIS_fnc_execVM", leader _grp, false] call BIS_fnc_MP; 

//_leader = leader (_this select 0); 
_unit = _this select 0; 
_veh = ""; 
if (vehicle _unit != _unit) then { _veh = vehicle _unit };  

_item = toUpper (_this select 1); 
if (_item == "") exitWith {}; 
_ReloadTime = _this select 2; 

while { alive _unit } do { 
    if (_item == "AMMO") then { 
        _veh setVehicleAmmo 1; 
        sleep _ReloadTime; 
    } else { 
        if (_item == "FUEL") then { 
            if (fuel _veh <= 0.25) then { 
                _veh setFuel 1; 
            }; 
            sleep 60; 
        } else { 
            if (_item == "BOTH") then { 
                if (fuel _veh <= 0.25) then { 
                    _veh setFuel 1; 
                }; 
                _veh setVehicleAmmo 1; 
                sleep _ReloadTime; 
            }; 
        }; 
    }; 
}; 
