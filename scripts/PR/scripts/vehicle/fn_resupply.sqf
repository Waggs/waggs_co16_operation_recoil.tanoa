
//[[[_grp,"ammo,fuel or both",120],"scripts\REAP\PR_Restock.sqf"],"BIS_fnc_execVM",leader _grp,false] call BIS_fnc_MP;
//[[[_grp,120],"scripts\REAP\PR_Restock.sqf"],"BIS_fnc_execVM",leader _grp,false] call BIS_fnc_MP;
_leader = leader (_this select 0);
_item = toUpper (_this select 1);
_ReloadTime = _this select 2;

while {alive _leader} do {
	if (_item == "AMMO") then {
		if (vehicle _leader != _leader) then {
			_veh = vehicle _leader;
			_veh setVehicleAmmo 1;
		};
		sleep _ReloadTime;
	} else {
		if (_item == "FUEL") then {
			if (vehicle _leader != _leader) then {
				_veh = vehicle _leader;
				if (fuel _veh <= 0.25) then {
					_veh setFuel 1;
				};
			};
			sleep 60;
	} else {
			if (_item == "BOTH") then {
				if (vehicle _leader != _leader) then {
					_veh = vehicle _leader;
					if (fuel _veh <= 0.25) then {
						_veh setFuel 1;
					};
					_veh setVehicleAmmo 1;
					sleep _ReloadTime;
				};
			};
		};
	};
};




