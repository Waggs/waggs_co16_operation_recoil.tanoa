// Building Top function
// Finds the roof of the building, and outputs a position in a random location on this roof top (but not so random as the edge of the building)

// pr_fnc_getBuildingtop
//RUFS_BuildingTop = {
private ["_building", "_dimensions", "_center", "_position", "_x", "_y", "_z"];

_marker = _this select 0;

_building = nearestObject [getMarkerPos _marker, "house"];

//_building = _this select 0;
_dimensions = boundingBox _building;
_center = boundingCenter _building; // center of the building
_debug = true;
_oca = 1;

//_buildings = nearestObjects [getMarkerPos _marker, ["house"], (_area select 0)];
//_building = nearestObject [position p1, "house"];


if ((typeName _building) != "OBJECT") then {
    hint "NO BUILDING EXISTS";
};

// x and y extremes of bounding box
_xmin = ((_dimensions select 0) select 0);
_ymin = ((_dimensions select 0) select 1);
_xmax = ((_dimensions select 1) select 0);
_ymax = ((_dimensions select 1) select 1);

_midx = (_xmax / 2) - 1;
_midy = (_ymax / 2) - 1; 


_xC = _center select 0;
_yC = _center select 1;
_z = ((_dimensions select 1) select 2); // height

/*
_dimensions = boundingBox _i;
_WorldDimensionsMax = _i modelToWorld (_dimensions select 1);
_WorldDimensionsMin = _i modelToWorld (_dimensions select 0);

_xmin = _WorldDimensionsMin select 0;
_ymin = _WorldDimensionsMin select 1;
_zmin = _WorldDimensionsMin select 2;

_zmax = _WorldDimensionsMax select 2;
_ymax = _WorldDimensionsMax select 1;
_xmax = _WorldDimensionsMax select 0;
*/




//player sidechat format ["center: %1 isflat: %2", _center, _isFlat];
// adjust this position for any elevation changes (building on top of hill for example)
//_position = [(getposATL _building select 0) + (sin (random 360)) * (random _midx), (getposATL _building select 1) + (cos (random 360)) * (random _midy), (getposATL _building select 2) + (_z + 2)];
_position = [getposATL _building select 0, getposATL _building select 1, (getposATL _building select 2) + _z + 2];

pr_position = _position;

//_mark = createMarker ["la", _position];
//_mark setMarkerType "hd_dot";

if (_debug) then {
    // Create marker of possible weapon placement
    _dummy = createVehicle ["Sign_Sphere25cm_F", _position, [], 0, "NONE"];
    _dummy setPosATL _position;

    // Create markers that show obstruction check area
    _dummyX = createVehicle ["Sign_Sphere10cm_F", [0,0,0], [], 0, "NONE"];
    _dummyX setPosATL [(_position select 0) - _oca, (_position select 1), (_position select 2)];
    _dummyX = createVehicle ["Sign_Sphere10cm_F", [0,0,0], [], 0, "NONE"];
    _dummyX setPosATL [(_position select 0) + _oca, (_position select 1), (_position select 2)];
    _dummyY = createVehicle ["Sign_Sphere10cm_F", [0,0,0], [], 0, "NONE"];
    _dummyY setPosATL [(_position select 0), (_position select 1) - _oca, (_position select 2)];
    _dummyY = createVehicle ["Sign_Sphere10cm_F", [0,0,0], [], 0, "NONE"];
    _dummyY setPosATL [(_position select 0), (_position select 1) + _oca, (_position select 2)];

    _debugMarker = createMarker [str(_position), _position];
    _debugMarker setMarkerShape "ICON";
    _debugMarker setMarkerType "mil_dot";
    _debugMarker setMarkerColor "ColorRed";
};

// Output
_position
//};