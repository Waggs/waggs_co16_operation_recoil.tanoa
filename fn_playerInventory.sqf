
_player = _this select 0;
_playerName = format ["%1", (name _player)];
_playerInventory = _player getVariable "pr_playerInventory";


removeAllAssignedItems _player;
removeAllContainers _player;
removeAllWeapons _player;
removeBackpack _player;
removeHeadgear _player;
removeUniform _player;
removeGoggles _player;
removeVest _player;

_items = _playerInventory select 0;
_headgear = _playerInventory select 1;

_uniform = _playerInventory select 2;
_uniformItems = _playerInventory select 3;
_magazinesUniform = _playerInventory select 4;

_vest = _playerInventory select 5;
_itemsVest = _playerInventory select 6;
_magazinesVest = _playerInventory select 7;

_backpack = _playerInventory select 8;
_itemsBackpack = _playerInventory select 9;
_magazinesBackpack = _playerInventory select 10;

_primaryWeaponMagazine = _playerInventory select 11;
_primaryWeapon = _playerInventory select 12;
_primaryWeaponItems = _playerInventory select 13;

_handgunMagazine = _playerInventory select 14;
_handgunWeapon = _playerInventory select 15;
_handgunItems = _playerInventory select 16;

_secondaryWeaponMagazine = _playerInventory select 17;
_secondaryWeapon = _playerInventory select 18;
_secondaryWeaponItems = _playerInventory select 19;

_countPrimaryAmmo = _playerInventory select 20;
_countHandgunAmmo = _playerInventory select 21;

_player addHeadgear _headgear;
_player addUniform _uniform;
_player addVest _vest;
_player addBackpack _backpack;

//--- add weapons back to unit
if (count _primaryWeaponMagazine > 0) then {
    _count = count _primaryWeaponMagazine - 1;
    for "_i" from 0 to _count do {
        _player addMagazine (_primaryWeaponMagazine select _i);
    };
};
_player addWeapon _primaryWeapon;
{ _player addPrimaryWeaponItem _x } forEach _primaryWeaponItems;

if (count _handgunMagazine > 0) then {
    _count = count _handgunMagazine - 1;
    for "_i" from 0 to _count do {
        _player addMagazine (_handgunMagazine select _i);
    };
};
_player addWeapon _handgunWeapon;
{ _player addHandgunItem _x } forEach _handgunItems;

if (count _secondaryWeaponMagazine > 0) then {
    _count = count _secondaryWeaponMagazine - 1;
    for "_i" from 0 to _count do {
        _player addMagazine (_secondaryWeaponMagazine select _i);
    };
};
_player addWeapon _secondaryWeapon;
{ _player addSecondaryWeaponItem _x } forEach _secondaryWeaponItems;

_unitormContainer = uniformContainer _player;
_vestContainer = vestContainer _player;
_backPackContainer = backpackcontainer _player;

//--- add personal items back to unit
if (count _items > 0) then {
    _count = count _items - 1;
    for "_i" from 0 to _count do {
        _player addWeapon (_items select _i);
    };
};

//--- add items and magazines back to uniform
_count = count (_uniformItems select 0) - 1;
for "_i" from 0 to _count do {
    _unitormContainer addItemCargoGlobal [(_uniformItems select 0) select _i, (_uniformItems select 1) select _i];
};
_count = count (_magazinesUniform select 0) - 1;
for "_i" from 0 to _count do {
    _unitormContainer addMagazineCargoGlobal [(_magazinesUniform select 0) select _i, (_magazinesUniform select 1) select _i];
};

//--- add items and magazines back to vest
_count = count (_itemsVest select 0) - 1;
for "_i" from 0 to _count do {
    _vestContainer addItemCargoGlobal [(_itemsVest select 0) select _i, (_itemsVest select 1) select _i];
};
_count = count (_magazinesVest select 0) - 1;
for "_i" from 0 to _count do {
    _vestContainer addMagazineCargoGlobal [(_magazinesVest select 0) select _i, (_magazinesVest select 1) select _i];
};

reload _player;

//--- add items and magazines back to backpack
_count = count (_itemsBackpack select 0) - 1;
for "_i" from 0 to _count do {
    _backPackContainer addItemCargoGlobal [(_itemsBackpack select 0) select _i, (_itemsBackpack select 1) select _i];
};
_count = count (_magazinesBackpack select 0) - 1;
for "_i" from 0 to _count do {
    _backPackContainer addMagazineCargoGlobal [(_magazinesBackpack select 0) select _i, (_magazinesBackpack select 1) select _i];
};

//--- set ammo count back to weapon
_player setAmmo [primaryWeapon _player, _countPrimaryAmmo];
_player setAmmo [handgunWeapon _player, _countHandgunAmmo];

diag_log format ["*PR* - Setting inventory for player %1 complete", _playerName];
