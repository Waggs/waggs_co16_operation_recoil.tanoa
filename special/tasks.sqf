waitUntil { (!isnil "taskHold") };


/****** TASK 1 ******
TASK 1 - TRIGGER CREATE/ASSIGN
cond: Assigned in tasks.sqf // [pr_task1, "assigned"] call fnc_tasker;

TASK 1 - TRIGGER SUCCEED
to 10 / none
cond:  this && (!(isTask1complete == "true") && ({ ((getPosATL _x) select 2) < 5 } count playableUnits > 0) && ({ _x in thisList } count playableUnits > 0))
cond: (!(isTask1complete == "true") && !(alive radar1))
onAct: [pr_task1, "succeeded"] call fnc_tasker; tsk1 = true; if (isServer && aliveOn) then { ["task1complete", "true"] call ALiVE_fnc_setData; };
*/
pr_task1 = [  //PARENT
west,  // group
"taskRepell",  // Task Name
"Repell Attackers",  // Task Title
"The Pirates are putting on an offensive. Take care of them and await further orders.",  // Task Description
"",  // Waypoint Name: Caret shown in mission
"",  // Task Markername: Destination/Target shown on map
"assigned"  // Task State: Assigned, Created, Succeeded, Canceled, Cancelled, Failed
];
//tsk1 = true; publicVariable "tsk1"; // Creating Parent Here and assigning Task 2
//tsk1assign = true; publicVariable "tsk1assign"; // Remove comment to assign Task 1

/****** TASK 2 ******
TASK 2 - TRIGGER CREATE/ASSIGN
cond:  this && (!(isTask2complete == "true") && (tsk1))
onAct: [pr_task2, "assigned"] call fnc_tasker;

TASK 2 - TRIGGER SUCCEED
to 20 / seized by blufor
cond:  this && (!(isTask2complete == "true") && (tsk1))
onAct: [pr_task2, "succeeded"] call fnc_tasker;  tsk2 = true; if (isServer && aliveOn) then { ["task2complete", "true"] call ALiVE_fnc_setData; }; // end = true;
*/
pr_task2 = [  //CHILD
west,  // group
"taskSecure",  // Task Name
"Public Relations",  // Task Title
"There is a group of pirates belived to be holding civilians in some ruins at Grid 110084 go check it out. Be sure to avoid civilian causualties.",  // Task Description
"",  // Waypoint Name: Caret shown in mission
"",  // Task Markername: Destination/Target shown on map
"assigned"  // Task State: Assigned, Created, Succeeded, Canceled, Cancelled, Failed
];
//tsk2assign = true; publicVariable "tsk2assign"; // Remove comment to assign Task 2
/****** TASK 3 ******
TASK 3 - TRIGGER CREATE
cd 20
cond:  (!(isTask3complete == "true") && (tsk2))
onAct: [pr_task3, "assigned"] call fnc_tasker;

TASK 3 - TRIGGER SUCCEED
to 20 / seized by blufor
cond:  (!(isTask3complete == "true") && (wereAllHere && weKicedSomeButt))
onAct: [pr_task3, "succeeded"] call fnc_tasker;  tsk3 = true; if (isServer && aliveOn) then { ["task3complete", "true"] call ALiVE_fnc_setData; };
*/
pr_task3 = [  //CHILD
west,  // group
"",  // Task Name
"",  // Task Title
"",  // Task Description
"",  // Waypoint Name: Caret shown in mission
"",  // Task Markername: Destination/Target shown on map
""  // Task State: Assigned, Created, Succeeded, Canceled, Cancelled, Failed
];
//tsk3assign = true; publicVariable "tsk3assign"; // Remove comment to assign Task 3

/****** TASK 4 ******
TASK 4 - TRIGGER CREATE/ASSIGN
cd 8
cond:  (!(isTask4complete == "true") && (tsk3))
onAct: [pr_task4, "assigned"] call fnc_tasker;

TASK 4 - TRIGGER SUCCEED
cd 15 / seized by blufor
cond:  this && (!(isTask4complete == "true") && (tsk3))
onAct: [pr_task4, "succeeded"] call fnc_tasker; tsk4 = true; if (isServer && aliveOn) then { ["task4complete", "true"] call ALiVE_fnc_setData; };
*/
pr_task4 = [  //CHILD
west,  // group 
"",  // Task Name
"",  // Task Title
"",  // Task Description
"",  // Waypoint Name: Caret shown in mission
"",  // Task Markername: Destination/Target shown on map
""  // Task State: Assigned, Created, Succeeded, Canceled, Cancelled, Failed
];
//tsk4assign = true; publicVariable "tsk4assign"; // Remove comment to assign Task 4

/****** TASK 5 *****
TASK 5 - TRIGGER CREATE/ASSIGN
cd 8
cond:  (!(isTask5complete == "true") && (tsk4))
onAct: [pr_task5, "assigned"] call fnc_tasker;

TASK 5 - TRIGGER SUCCEED
to 20 / seized by blufor
cond:  this && (!(isTask5complete == "true") && (tsk4))
onAct: [pr_task5, "succeeded"] call fnc_tasker; tsk5 = true; if (isServer && aliveOn) then { ["task5complete", "true"] call ALiVE_fnc_setData; };
*/
pr_task5 = [  //CHILD
west,  // group
"",  // Task Name
"",  // Task Title
"",  // Task Description
"",  // Waypoint Name: Caret shown in mission
"",  // Task Markername: Destination/Target shown on map
""  // Task State: Assigned, Created, Succeeded, Canceled, Cancelled, Failed
];

/****** TASK 6 *****
TASK 6 - TRIGGER CREATE PARENT
cd 8
cond:  (!(isTask6complete == "true") && (tsk5))
onAct: [pr_task6, "Created"] call fnc_tasker; tsk6 = true;

TASK 6 - TRIGGER SUCCEED PARENT
cd 8
cond:  (!(isTask6complete == "true") && (tsk7) && (tsk8) && (tsk9))
onAct: [pr_task6, "succeeded"] call fnc_tasker; if (isServer && aliveOn) then { ["task6complete", "true"] call ALiVE_fnc_setData; };
*/
pr_task6 = [  //PARENT
west,  // group
"",  // Task Name
"",  // Task Title
"",  // Task Description
"",  // Waypoint Name: Caret shown in mission
"",  // Task Markername: Destination/Target shown on map
""  // Task State: Assigned, Created, Succeeded, Canceled, Cancelled, Failed
];

/****** TASK 7 *****
TASK 7 - TRIGGER CREATE/ASSIGN
cd 8
cond:  (!(isTask7complete == "true") && (tsk6))
onAct: [pr_task7, "assigned"] call fnc_tasker; "task7mrk" setMarkerAlpha 1;

TASK 7 - TRIGGER SUCCEED
to 20 / seized by blufor
cond:  this && (!(isTask7complete == "true") && (tsk6))
onAct: [pr_task7, "succeeded"] call fnc_tasker; tsk7 = true; if (isServer && aliveOn) then { ["task7complete", "true"] call ALiVE_fnc_setData; };
*/
pr_task7 = [  //CHILD
west,  // group
"",  // Task Name
"",  // Task Title
"",  // Task Description
"",  // Waypoint Name: Caret shown in mission
"",  // Task Markername: Destination/Target shown on map
""  // Task State: Assigned, Created, Succeeded, Canceled, Cancelled, Failed
];

/****** TASK 8 *****
TASK 8 - TRIGGER CREATE/ASSIGN
cd 8
cond:  (!(isTask8complete == "true") && (tsk7))
onAct: [pr_task8, "assigned"] call fnc_tasker; "task8mrk" setMarkerAlpha 1;

TASK 8 - TRIGGER SUCCEED
to 20 / seized by blufor
cond:  this && (!(isTask8complete == "true") && (tsk7))
onAct: [pr_task8, "succeeded"] call fnc_tasker; tsk8 = true; if (isServer && aliveOn) then { ["task8complete", "true"] call ALiVE_fnc_setData; };
*/
pr_task8 = [  //CHILD
west,  // group
"",  // Task Name
"",  // Task Title
"",  // Task Description
"",  // Waypoint Name: Caret shown in mission
"",  // Task Markername: Destination/Target shown on map
""  // Task State: Assigned, Created, Succeeded, Canceled, Cancelled, Failed
];

/****** TASK 9 *****
TASK 9 - TRIGGER CREATE/ASSIGN
cd 8
cond:  (!(isTask9complete == "true") && (tsk8))
onAct: [pr_task9, "assigned"] call fnc_tasker; "task9mrk" setMarkerAlpha 1; 

TASK 9 - TRIGGER SUCCEED 
to 20 / seized by blufor
cond:  this && (!(isTask9complete == "true") && (tsk8))
onAct: [pr_task9, "succeeded"] call fnc_tasker; tsk9 = true; if (isServer && aliveOn) then { ["task9complete", "true"] call ALiVE_fnc_setData; }; 
*/
pr_task9 = [  //CHILD
west,  // group 
"",  // Task Name 
"",  // Task Title 
"",  // Task Description 
"",  // Waypoint Name: Caret shown in mission 
"",  // Task Markername: Destination/Target shown on map 
""  // Task State: Assigned, Created, Succeeded, Canceled, Cancelled, Failed
]; 

/****** TASK 10 *****
TASK 10 - TRIGGER CREATE/ASSIGN
cd 8
cond:  (!(isTask10complete == "true") && (tsk9))
onAct: [pr_task10, "assigned"] call fnc_tasker; 

TASK 10 - TRIGGER SUCCEED 
to 20 / seized by blufor
cond:  this && (!(isTask10complete == "true") && (tsk9) && (!alive nuke1))
onAct: [pr_task10, "succeeded"] call fnc_tasker; tsk10 = true; if (isServer && aliveOn) then { ["task10complete", "true"] call ALiVE_fnc_setData; }; 
*/
pr_task10 = [ 
west,  // group 
"",  // Task Name 
"",  // Task Title 
"",  // Task Description 
"",  // Waypoint Name: Caret shown in mission 
"",  // Task Markername: Destination/Target shown on map 
""  // Task State: Assigned, Created, Succeeded, Canceled, Cancelled, Failed
]; 

/****** TASK 11 *****
*/
pr_task11 = [ 
west,  // group 
"",  // Task Name 
"",  // Task Title 
"",  // Task Description 
"",  // Waypoint Name: Caret shown in mission 
"",  // Task Markername: Destination/Target shown on map 
""  // Task State: Assigned, Created, Succeeded, Canceled, Cancelled, Failed
]; 

/****** TASK 12 *****
*/
pr_task12 = [ 
west,  // group 
"",  // Task Name 
"",  // Task Title 
"",  // Task Description 
"",  // Waypoint Name: Caret shown in mission 
"",  // Task Markername: Destination/Target shown on map 
""  // Task State: Assigned, Created, Succeeded, Canceled, Cancelled, Failed
]; 