/*
*  Author: PapaReap
*
*  DOWNLOAD DATA DEVICES
*  UPLOAD DATA DEVICES
*  CODE HOLDERS
*  DEFUSE DEVICES
*  NUKE DEVICES
*  ver 1.0 2016-05-15
*/

/*
*  DOWNLOAD DATA DEVICES
*  Usage: Separate by commas i.e. - downloadData = [laptop1, laptop2, laptop3, laptop4];
*  Arguments:
*  0: <NAME OF DEVICE>  - Name of object to download data from    - (variable name)
*/
downloadData = [laptop1, laptop2];


/*
*  UPLOAD DATA DEVICES
*  Usage: Separate by commas i.e. - uploadData = [laptop3, satphone, satphone2];
*  Arguments:
*  0: <NAME OF DEVICE>          - Name of object to upload data from    - (variable name)
*/
uploadData = [];


/*
*  CODE HOLDERS
*  Code Holders: Laptop or other item to get codes and wire information from. Can use as many as you like.
*  Usage: Separate by commas i.e. - codeHolder = [[laptop1, 2, 1, "DATA"] ,[laptop2, 2, 1, "DATA"], [satPhone, 2, 1]];
*  Arguments:
*  0: <NAME OF CODE HOLDER>    - Name of object to hold the codes    - (variable name)
*  1: <NUMBER OF CODES>        - How many code numbers to give, 0- Any amount can be used.    - (scalar)
*  2: <NUMBER OF WIRES>        - How many wires to cut, 0-4. Maximum of 4 wires combined with all codeholders can be used.    - (scalar)
*  3: <DATA>                   - <OPTIONAL> Used for data download script    - (string)
*/
codeHolder = [
[laptop1, 2, 1, "DATA"],
[laptop2, 2, 1, "DATA"]
];


/*
*  DEFUSE DEVICES
*  Defuse device: Laptop, satPhone, bomb, etc...
*  Usage: Separate by commas i.e. - [[defuseDevice_1, true], [satPhone, false]];
*  Arguments:
*  0: <NAME OF DEFUSE DEVICE>   - Defusing device name, can also be the bomb.    - (variable name)
*  1: <REQUIRE SPECILIST>       - Require specialist - soldier type = engineer or explosive specialist.   - (bool)
*/
defuseDevice = [[defuseDevice_1, true]];


/*
*  NUKE DEVICES
*  Arguments:
*  0: <NAME OF BOMB>            - Name of object to center nuclear explosion    - (variable name)
*  1: <CONDITION NAME>          - Condition name to create for detonation timer, use quotes. In a trigger or by other means, to activate the bomb timer use for example,  setbomb1 = true;    - (string)
*  2: <DETONATION TIME>         - Time before bomb detonation    - (scalar)
*  3: <METER DISTANCE>          - Distance from bomb to hear the radiation meter    - (scalar)
*  4: <REQUIRE MINE DETECTOR>   - Player must have a mine detector to hear geiger    - (bool)
*  5: <AFFECTED RADIUS>         - Area of destruction, immediate %100 destruction is 1/2 this distance    - (scalar)
*  6: <DO DAMAGE>               - Do damage from explosion    - (bool)
*  7: <DO FIRE>                 - Set random buildings on fire, could have frame impact.    - (bool)
*  8: <FIRE SCALE>              - Scale of buildings to burn. Higher number means more fires. Value of 0 will only do small amount of fires at immediate blast area, roughly 1/2 of the affected radius    - (scalar)
*  9: <IRRADIATION TIME>        - Time until radiation dissipates    - (scalar)
* 10: <REQUIRE GPS>             - Player must have a gps to hear radiation geiger    - (bool)
* 11: <BLOW SPEED>              - Velocity of blown units caught in shockwave    - (scalar)
* 12: <RADIATION COEFFICIENT>   - nuclear radiation damage coefficient    - (scalar)
* 13: <VEHICLE ARMOR>           - nuclear car armour    - (scalar) // NEED WORK
* 14: <BLACKLIST>               - Add unit or object names to this list, separate by commas i.e. [p1, p2, car1], you can instead, put into the unit or objects init: noNukeDestroy = noNukeDestroy + [this];    - (array)
* 15: <RADIATION SAFE AREAS>    - Radiation free zones, can be house, vehicle    - (array)
*/ 
useNuclear = true; //--- allow the use of nuclear bombs

nukes = [
        [
/*  <0> */  main_nuclear,
/*  <1> */  "setbomb1",
/*  <2> */  90,
/*  <3> */  50,
/*  <4> */  true,
/*  <5> */  940,
/*  <6> */  true,
/*  <7> */  true,
/*  <8> */  3,
/*  <9> */  360,
/* <10> */  true,
/* <11> */  35,
/* <12> */  0.02,
/* <13> */  0.5,
/* <14> */  [],
/* <15> */  [radCar,radCar2,radHouse,radHouse2]
        ]
/* un-comment below to add more nukes */
       ,[
/*  <0> */  main_nuclear2,
/*  <1> */  "setbomb2",
/*  <2> */  15,
/*  <3> */  50,
/*  <4> */  true,
/*  <5> */  940,
/*  <6> */  true,
/*  <7> */  true,
/*  <8> */  2,
/*  <9> */  360,
/* <10> */  true,
/* <11> */  35,
/* <12> */  0.02,
/* <13> */  0.5,
/* <14> */  [],
/* <15> */  []
       ]
];






/*************************************** DO NOT EDIT BELOW ***************************************/

if !(isServer) exitWith {};

//sleep 1;
waitUntil { time > 0 }; 

//--- DOWNLOAD & UPLOAD DATA ---//
{ [[_x], "T8_fnc_addActionLaptop", true, true] spawn BIS_fnc_MP; } forEach downloadData;
{ [[_x], "T9_fnc_addActionLaptop", true, true] spawn BIS_fnc_MP; } forEach uploadData;
compiledDownload = true;

waitUntil { defusedInitCompiled };

//--- CODE HOLDERS ---//
if (isNil "buildCodes") then { buildCodes = true; };
for [{ _loop = 0 }, { _loop < count codeHolder }, { _loop = _loop + 1 }] do {
    waitUntil { buildCodes };
    buildCodes = false;
    _codeHolderOrder = _loop + 1;
    _codeHolder = (codeHolder select _loop) select 0;
    _codeCnt = (codeHolder select _loop) select 1;
    _wireCnt = (codeHolder select _loop) select 2;
    _type = (codeHolder select _loop) select 3;
    [[_codeHolder, _codeHolderOrder], ["code", _codeCnt], ["wire", _wireCnt], _type] spawn pr_fnc_code;
};

//--- DEFUSING DEVICES ---//
if (isNil "buildDefuse") then { buildDefuse = true; };
for [{ _loop = 0 }, { _loop < count defuseDevice }, { _loop = _loop + 1 }] do {
    waitUntil { buildDefuse };
    buildDefuse = false;
    _device = (defuseDevice select _loop) select 0;
    _condition = (defuseDevice select _loop) select 1;
    [[_device, _condition], "pr_fnc_defuseAction", true, false] call BIS_fnc_MP;
};

//--- NUKE DEVICES ---//
if (useNuclear) then {
    if (isNil "buildNukes") then { buildNukes = true; };
    waitUntil { buildNukes };
    buildNukes = false;
    for [{ _loop = 0 }, { _loop < count nukes }, { _loop = _loop + 1 }] do {
        _nuke          = (nukes select _loop) select 0;
        _condition     = (nukes select _loop) select 1;
        _time          = (nukes select _loop) select 2;
        _geigerDst     = (nukes select _loop) select 3;
        _reqMineDt     = (nukes select _loop) select 4;
        _radius        = (nukes select _loop) select 5;
        _doDamage      = (nukes select _loop) select 6;
        _doFire        = (nukes select _loop) select 7;
        _fireScale     = (nukes select _loop) select 8;
        _irradTime     = (nukes select _loop) select 9;
        _reqGPS        = (nukes select _loop) select 10;
        _blowSpeed     = (nukes select _loop) select 11;
        _radCoEff      = (nukes select _loop) select 12;
        _carArmor      = (nukes select _loop) select 13;
        _blacklist     = (nukes select _loop) select 14;
        _radFree       = (nukes select _loop) select 15;
        _blacklist = _blacklist + noNukeDestroy;
        _radFree = _radFree + noRadiation; pr_radFree=_radFree;
        [_nuke, _condition, _time, _geigerDst, _reqMineDt, _radius, _doDamage, _doFire, _fireScale, _irradTime, _reqGPS, _blowSpeed, _radCoEff, _carArmor, _blacklist, _radFree] spawn pr_fnc_nukeInit;
    };
};
